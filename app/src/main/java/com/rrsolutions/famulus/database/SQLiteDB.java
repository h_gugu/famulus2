package com.rrsolutions.famulus.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.rrsolutions.famulus.datawrapper.PrinterCategoryPair;
import com.rrsolutions.famulus.modeljson.Category;
import com.rrsolutions.famulus.modeljson.Device;
import com.rrsolutions.famulus.modeljson.EventInfo;
import com.rrsolutions.famulus.modeljson.HistoryItem;
import com.rrsolutions.famulus.modeljson.Login;
import com.rrsolutions.famulus.modeljson.Option;
import com.rrsolutions.famulus.modeljson.Printer;
import com.rrsolutions.famulus.modeljson.Product;
import com.rrsolutions.famulus.enumeration.OrderStatus;
import com.rrsolutions.famulus.printer.PrintCategory;
import com.rrsolutions.famulus.printer.PrintOption;
import com.rrsolutions.famulus.printer.PrintProduct;
import com.rrsolutions.famulus.printer.PrintedCategory;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;


/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class SQLiteDB extends SQLiteOpenHelper {

    private static SQLiteDB sInstance;

    private static String DATABASE_NAME = "famulus.db";
    private static final int DATABASE_VERSION = 1;

    // region Database Tables Structures

    private String sTable_Login = "CREATE TABLE login (id integer PRIMARY KEY AUTOINCREMENT,username text,password text)";

    private String sTable_Device = "CREATE TABLE device (id integer PRIMARY KEY AUTOINCREMENT, waiter_name text," +
            "operation_mode integer, allow_oh_viewing integer, enable_payment_screen integer, " +
            "oh_update_interval integer, level_1_pin text, level_2_pin text)";

    private String sTable_Printer = "CREATE TABLE printers (id integer PRIMARY KEY AUTOINCREMENT,name text," +
            "manufacturer text,mac text,serial text,model text)";

    private String sTable_EventInfo = "CREATE TABLE event_info (id integer PRIMARY KEY AUTOINCREMENT,name text, " +
            "start_date text, end_date text)";

    private String sTable_Category = "CREATE TABLE category (id integer PRIMARY KEY,name text, extra_info text,enabled integer," +
            "deleted integer)";

    private String sTable_Product = "CREATE TABLE products (id integer PRIMARY KEY,category_id integer " +
            ",name text, extra_info text,price double,enabled integer,deleted integer)";

    private String sTable_Option = "CREATE TABLE options (id integer PRIMARY KEY, product_id integer " +
            ",orderIndex integer,name text, extra_info text,enabled integer," +
            "deleted integer)";

    private String sTable_Printers_Category = "CREATE TABLE printers_category (printer_id integer,category_id integer)";

    private String sTable_Order = "CREATE TABLE orders (id integer NOT NULL PRIMARY KEY,waiter_name text,printing_mode integer," +
            "status integer,created text, table_id text, operation_mode int, sync int)";

    private String sTable_Order_Category = "CREATE TABLE order_category (order_id integer,category_id integer," +
            "status integer,print_option integer,print_message text, duplicate int)";

    private String sTable_Order_Category_Product = "CREATE TABLE order_category_product (category_id integer,product_id integer, price double, quantity integer,description text, order_id integer)";

    private String sTable_Order_Category_Product_Option = "CREATE TABLE order_category_product_option (product_id integer,option_id integer, order_id integer)";

    private String sTable_Printed_Category = "CREATE TABLE printed_category (order_id integer, category_id integer,mac text)";

    // endregion

    // region Tables Name

    private String sTable_Login_Name = "login";
    private String sTable_Device_Name = "device";
    private String sTable_Printer_Name = "printers";
    private String sTable_EventInfo_Name = "event_info";
    private String sTable_Category_Name = "category";
    private String sTable_Product_Name = "products";
    private String sTable_Option_Name = "options";
    private String sTable_Printed_Category_Name = "printed_category";
    private String sTable_Printers_Category_Name = "printers_category";
    private String sTable_Order_Name = "orders";
    private String sTable_Order_Category_Name = "order_category";
    private String sTable_Order_Category_Product_Name = "order_category_product";
    private String sTable_Order_Category_Product_Option_Name = "order_category_product_option";


    // endregion

    // region Login Columns

    private String sTable_Login_Username = "username";
    private String sTable_Login_Password = "password";

    // endregion

    // region Device Columns

    private String sTable_Device_Waiter_Name = "waiter_name";
    private String sTable_Device_Operation_Mode = "operation_mode";
    private String sTable_Device_Allow_Oh_Viewing = "allow_oh_viewing";
    private String sTable_Device_Enable_Payment_Screen = "enable_payment_screen";
    private String sTable_Device_Oh_Update_Interval = "oh_update_interval";
    private String sTable_Device_Level_1_Pin = "level_1_pin";
    private String sTable_Device_Level_2_Pin = "level_2_pin";

    // endregion

    // region Printer Columns

    private String sTable_Printer_Id = "id";
    private String sTable_Printer_Name_Title = "name";
    private String sTable_Printer_Manufacturer = "manufacturer";
    private String sTable_Printer_Mac = "mac";
    private String sTable_Printer_Serial = "serial";
    private String sTable_Printer_Model = "model";

    // endregion

    // region EventInfo Columns

    private String sTable_EventInfo_Name_Title = "name";
    private String sTable_EventInfo_Start_Date = "start_date";
    private String sTable_EventInfo_End_Date = "end_date";

    // endregion

    // region Category Columns

    private String sTable_Category_Id = "id";
    private String sTable_Category_Name_Title = "name";
    private String sTable_Category_Extra_Info = "extra_info";
    private String sTable_Category_Enabled = "enabled";
    private String sTable_Category_Deleted = "deleted";

    // endregion Columns

    // region Product Columns

    private String sTable_Product_Id = "id";
    private String sTable_Product_CatId = "category_id";
    private String sTable_Product_Name_Title = "name";
    private String sTable_Product_ExtraInfo = "extra_info";
    private String sTable_Product_Price = "price";
    private String sTable_Product_Enabled = "enabled";
    private String sTable_Product_Deleted = "deleted";

    // endregion

    // region Options Columns

    private String sTable_Option_Id = "id";
    private String sTable_Option_ProId = "product_id";
    private String sTable_Option_OrderIndex = "orderIndex";
    private String sTable_Option_Name_Title = "name";
    private String sTable_Option_ExtraInfo = "extra_info";
    private String sTable_Option_Enabled = "enabled";
    private String sTable_Option_Deleted = "deleted";

    // endregion

    // region Printers Category Columns

    private String sTable_Printers_Category_PrinterId = "printer_id";
    private String sTable_Printers_Category_CategoryId = "category_id";

    // endregion

    // region Order Columns

    private String sTable_Order_Id = "id";
    private String sTable_Order_WaiterName = "waiter_name";
    private String sTable_Order_Printing_Mode = "printing_mode";
    // status = 0; all categories have not printed yet.
    // status = 2; all categories have printed.
    private String sTable_Order_Status = "status";
    // datetime with format year-month-day hour:minues:secs 24 hours format
    private String sTable_Order_Created = "created";
    private String sTable_Order_TableId = "table_id";
    // 0 = waiter
    // 1 = cashier
    private String sTable_Order_Operation_Mode = "operation_mode";
    // 0 = order is not sync
    // 2 = order is sync
    private String sTable_Order_Sync = "sync";


    // endregion

    // region Order Category Columns

    private String sTable_Order_Category_OrderId = "order_id";
    private String sTable_Order_Category_CategoryId = "category_id";

    // print_option = 0; category will print in one receipt/ticket.
    // print_option = 1 category products will be printed in each receipt/ticket.

    private String sTable_Order_Category_Print_Option = "print_option";

    // status = 0; category is not printed yet.
    // status = 1; category in in queue for printing.
    // status = 2; category has printed.

    private String sTable_Order_Category_Status = "status";
    private String sTable_Order_Category_Duplicated = "duplicate";
    private String sTable_Order_Category_Message = "print_message";

    // endregion

    // region Order Category Product Columns

    private String sTable_Order_Category_Product_CategoryId = "category_id";
    private String sTable_Order_Category_Product_ProductId = "product_id";
    private String sTable_Order_Category_Product_Price = "price";
    private String sTable_Order_Category_Product_Quantity = "quantity";
    private String sTable_Order_Category_Product_Description = "description";
    private String sTable_Order_Category_Product_OrderId = "order_id";

    // endregion

    // region Order Category Product Option Columns

    private String sTable_Order_Category_Product_Option_OptionId = "option_id";
    private String sTable_Order_Category_Product_Option_ProductId = "product_id";
    private String sTable_Order_Category_Product_Option_OrderId = "order_id";

    // endregion

    // region Printed Category Columns

    private String sTable_Printed_Category_OrderId = "order_id";
    private String sTable_Printed_Category_CategoryId = "category_id";
    private String sTable_Printed_Category_Mac = "mac";

    // endregion

    public static synchronized SQLiteDB getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        //String folder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
        //DATABASE_NAME = folder + "/" + "famulus.db";
        if (sInstance == null) {
            sInstance = new SQLiteDB(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    public SQLiteDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sTable_Login);
        db.execSQL(sTable_Device);
        db.execSQL(sTable_Printer);
        db.execSQL(sTable_EventInfo);
        db.execSQL(sTable_Category);
        db.execSQL(sTable_Product);
        db.execSQL(sTable_Option);
        db.execSQL(sTable_Printers_Category);
        db.execSQL(sTable_Order);
        db.execSQL(sTable_Order_Category);
        db.execSQL(sTable_Order_Category_Product);
        db.execSQL(sTable_Order_Category_Product_Option);
        db.execSQL(sTable_Printed_Category);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // region Login CRUD

    public void insert(String username, String password) {
        SQLiteDatabase db = getWritableDatabase();
        //String path =  db.getPath();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Login_Username, username);
            values.put(sTable_Login_Password, password);
            db.insertOrThrow(sTable_Login_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public Login getLogin() {
        Login login = null;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Login_Username, sTable_Login_Password };
        Cursor cursor = db.query(sTable_Login_Name, columns, null, null,null, null, null, "1" );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                login = new Login();
                login.setUsername(cursor.getString(cursor.getColumnIndex(sTable_Login_Username)));
                login.setPassword(cursor.getString(cursor.getColumnIndex(sTable_Login_Password)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return login;
    }

    public void deleteLogin() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Login_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    // endregion

    // region JSON data from Server

    public void insert(Device device) {
        SQLiteDatabase db = getWritableDatabase();
        //String path =  db.getPath();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Device_Waiter_Name, device.getWaiter_name());
            values.put(sTable_Device_Operation_Mode, device.getOperation_mode());
            values.put(sTable_Device_Allow_Oh_Viewing, device.isAllow_oh_viewing() ? 1 : 0);
            values.put(sTable_Device_Enable_Payment_Screen, device.isEnable_payment_screen() ? 1 : 0);
            values.put(sTable_Device_Oh_Update_Interval, device.getOh_update_interval());
            values.put(sTable_Device_Level_1_Pin, device.getLevel_1_pin());
            values.put(sTable_Device_Level_2_Pin, device.getLevel_2_pin());
            db.insertOrThrow(sTable_Device_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void insert(List<Printer> printers) {
        SQLiteDatabase db = getWritableDatabase();
        //String path =  db.getPath();
        db.beginTransaction();
        try {
            for(Printer printer : printers) {
                ContentValues values = new ContentValues();
                values.put(sTable_Printer_Name_Title, printer.getName());
                values.put(sTable_Printer_Manufacturer, printer.getManufacturer());
                values.put(sTable_Printer_Mac, printer.getMac());
                values.put(sTable_Printer_Serial, printer.getSerial());
                values.put(sTable_Printer_Model, printer.getModel());
                db.insertOrThrow(sTable_Printer_Name, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void insertCats(List<Category> categories) {
        SQLiteDatabase db = getWritableDatabase();
        //String path =  db.getPath();
        db.beginTransaction();
        ContentValues values = new ContentValues();
        try {
            for(Category category : categories) {
                values.clear();
                values.put(sTable_Category_Id, category.getId());
                values.put(sTable_Category_Name_Title, category.getName());
                values.put(sTable_Category_Extra_Info, category.getExtra_info());
                values.put(sTable_Category_Enabled, category.isEnabled() ? 1 : 0);
                values.put(sTable_Category_Deleted, category.isDeleted() ? 1 : 0);
                db.insertOrThrow(sTable_Category_Name, null, values);
                if(category.getProducts().size() > 0) {
                    for(Product product : category.getProducts()) {
                        values.clear();
                        values.put(sTable_Product_Id, product.getId());
                        values.put(sTable_Product_CatId, product.getCat_id());
                        values.put(sTable_Product_Name_Title, product.getName());
                        values.put(sTable_Product_ExtraInfo, product.getExtra_info());
                        values.put(sTable_Product_Price, product.getPrice());
                        values.put(sTable_Product_Enabled, product.isEnabled() ? 1 : 0);
                        values.put(sTable_Product_Deleted, product.isDeleted() ? 1 : 0);
                        db.insertOrThrow(sTable_Product_Name, null, values);
                        if(product.getOptions().size() > 0) {
                            for(Option option : product.getOptions()) {
                                values.clear();
                                values.put(sTable_Option_Id, option.getId());
                                values.put(sTable_Option_ProId, option.getProd_id());
                                values.put(sTable_Option_OrderIndex, option.getOrderIndex());
                                values.put(sTable_Option_Name_Title, option.getName());
                                values.put(sTable_Option_ExtraInfo, option.getExtra_info());
                                values.put(sTable_Option_Enabled, option.isEnabled() ? 1 : 0);
                                values.put(sTable_Option_Deleted, option.isDeleted() ? 1 : 0);
                                db.insertOrThrow(sTable_Option_Name, null, values);
                            }
                        }
                    }
                }
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    public String getWaiterName() {
        String waiter = "";
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Device_Waiter_Name };
        Cursor cursor = db.query(sTable_Device_Name, columns, null, null,null, null, null, "1" );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                waiter = cursor.getString(cursor.getColumnIndex(sTable_Device_Waiter_Name));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return waiter;
    }

    public int getUserType() {
        int type = 0;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Device_Operation_Mode };
        Cursor cursor = db.query(sTable_Device_Name, columns, null, null,null, null, null, "1" );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                type = cursor.getInt(cursor.getColumnIndex(sTable_Device_Operation_Mode));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return type;
    }

    public String getPrinterName(String mac) {
        String name = "";
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Printer_Name_Title };
        String where = sTable_Printer_Mac + " = ? ";
        Cursor cursor = db.query(sTable_Printer_Name, columns, where, new String[] { mac },null, null, null, "1" );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                name = cursor.getString(cursor.getColumnIndex(columns[0]));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return name;
    }

    public Printer getPrinter(String mac) {
        Printer printer = new Printer();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Printer_Id, sTable_Printer_Name_Title, sTable_Printer_Mac, sTable_Printer_Manufacturer, sTable_Printer_Serial, sTable_Printer_Model };
        String where = sTable_Printer_Mac + " = ? ";
        Cursor cursor = db.query(sTable_Printer_Name, columns, where, new String[] { mac },null, null, null, "1" );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                printer.setId(cursor.getInt(cursor.getColumnIndex(sTable_Printer_Id)));
                printer.setName(cursor.getString(cursor.getColumnIndex(sTable_Printer_Name_Title)));
                printer.setModel(cursor.getString(cursor.getColumnIndex(sTable_Printer_Model)));
                printer.setSerial(cursor.getString(cursor.getColumnIndex(sTable_Printer_Serial)));
                printer.setManufacturer(cursor.getString(cursor.getColumnIndex(sTable_Printer_Manufacturer)));
                printer.setMac(cursor.getString(cursor.getColumnIndex(sTable_Printer_Mac)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return printer;
    }

    public String[] getPrinterMac(int catid) {
            String[] macs = null;
            SQLiteDatabase db = getReadableDatabase();
            String query = "select p.mac from printers p, printers_category pc where p.id = pc.printer_id and pc.category_id = ?";
            Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(catid) });
            try {
                if(cursor.getCount() > 0) {
                    int i = 0;
                    macs = new String[cursor.getCount()];
                    for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        macs[i] = cursor.getString(0);
                        i++;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if(cursor != null && !cursor.isClosed()) cursor.close();
            }
            return macs;
    }

    public String[] getPrinterMacOther(int catid) {
        String[] macs = null;
        SQLiteDatabase db = getReadableDatabase();
        String query = "select p.mac from printers p, printers_category pc where p.id = pc.printer_id and pc.category_id = ?";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(catid) });
        try {
            if(cursor.getCount() > 0) {
                int i = 0;
                macs = new String[cursor.getCount()];
                for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    macs[i] = cursor.getString(0);
                    i++;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return macs;
    }

    public boolean isCategoryAssigned(int catid, String mac) {
        boolean hasCat = false;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT pc.category_id FROM printers p, printers_category pc WHERE p.id = pc.printer_id " +
                "AND pc.category_id = ? " +
                "AND pc.printer_id = (SELECT id FROM printers WHERE mac = ?)";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(catid), mac });
        try {
            hasCat = cursor.getCount() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return hasCat;
    }

    public boolean isCategoryAssigned(int catid) {
        boolean hasCat = false;
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT printer_id FROM printers_category WHERE category_id = ? ";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(catid) });
        try {
            hasCat = cursor.getCount() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return hasCat;
    }

    public boolean hasOtherCategory(int printerid) {
        boolean hasOrder = false;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Printers_Category_CategoryId };
        String where = sTable_Printers_Category_PrinterId + " = ? AND " + sTable_Printers_Category_CategoryId + " = -1";
        Cursor cursor = db.query(sTable_Printers_Category_Name, columns, where, new String[] { String.valueOf(printerid) },null, null, null );
        try {
            hasOrder = cursor.getCount() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return hasOrder;
    }

    public List<Category> selectAllCategories() {
        List<Category> categories = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Category_Id, sTable_Category_Name_Title, sTable_Category_Extra_Info };
        String selection = sTable_Category_Enabled + " = 1 AND " + sTable_Category_Deleted + " = 0";
        String orderBy = sTable_Category_Id;
        Cursor cursor = db.query(sTable_Category_Name, columns, selection, null,null, null, orderBy );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Category category = new Category();
                category.setId(cursor.getInt(cursor.getColumnIndex(sTable_Category_Id)));
                category.setName(cursor.getString(cursor.getColumnIndex(sTable_Category_Name_Title)));
                category.setExtra_info(cursor.getString(cursor.getColumnIndex(sTable_Category_Extra_Info)));
                categories.add(category);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return categories;
    }

    public Category selectCategory(int catid) {
        Category category = new Category();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Category_Name_Title, sTable_Category_Extra_Info };
        String selection = sTable_Category_Id + " = " + catid + " AND " + sTable_Category_Enabled + " = 1 AND " + sTable_Category_Deleted + " = 0";
        Cursor cursor = db.query(sTable_Category_Name, columns, selection, null,null, null, null );
        try {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                category.setId(catid);
                category.setName(cursor.getString(cursor.getColumnIndex(sTable_Category_Name_Title)));
                category.setExtra_info(cursor.getString(cursor.getColumnIndex(sTable_Category_Extra_Info)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return category;
    }

    public int getCategoryId(int proid) {
        int catid = 0;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Product_CatId };
        String selection = sTable_Product_Id + " = " + proid;
        Cursor cursor = db.query(sTable_Product_Name, columns, selection, null,null, null, null );
        try {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                catid = cursor.getInt(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return catid;
    }

    public List<Product> selectAllProducts(int catid) {
        List<Product> products = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Product_Id, sTable_Product_Name_Title, sTable_Product_ExtraInfo, sTable_Product_Price  };
        String selection = sTable_Product_CatId + " = " + catid + " AND " + sTable_Product_Enabled + " = 1 AND " + sTable_Product_Deleted + " = 0";
        String orderBy = sTable_Product_Id;
        Cursor cursor = db.query(sTable_Product_Name, columns, selection, null,null, null, orderBy );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Product product = new Product();
                product.setId(cursor.getInt(cursor.getColumnIndex(sTable_Product_Id)));
                product.setCat_id(catid);
                product.setName(cursor.getString(cursor.getColumnIndex(sTable_Product_Name_Title)));
                product.setExtra_info(cursor.getString(cursor.getColumnIndex(sTable_Product_ExtraInfo)));
                product.setPrice(cursor.getDouble(cursor.getColumnIndex(sTable_Product_Price)));
                products.add(product);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return products;
    }

    public List<Printer> selectAllPrinters() {
        List<Printer> printers = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Printer_Id, sTable_Printer_Name_Title, sTable_Printer_Manufacturer, sTable_Printer_Mac, sTable_Printer_Serial  };
        String orderBy = sTable_Printer_Id;
        Cursor cursor = db.query(sTable_Printer_Name, columns, null, null,null, null, orderBy );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Printer printer = new Printer();
                printer.setId(cursor.getInt(cursor.getColumnIndex(sTable_Printer_Id)));
                printer.setName(cursor.getString(cursor.getColumnIndex(sTable_Printer_Name_Title)));
                printer.setManufacturer(cursor.getString(cursor.getColumnIndex(sTable_Printer_Manufacturer)));
                printer.setMac(cursor.getString(cursor.getColumnIndex(sTable_Printer_Mac)));
                printer.setSerial(cursor.getString(cursor.getColumnIndex(sTable_Printer_Serial)));
                printers.add(printer);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return printers;
    }

    public Product selectProduct(int proid) {
        Product product = new Product();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Product_Name_Title, sTable_Product_CatId, sTable_Product_ExtraInfo, sTable_Product_Price };
        String selection = sTable_Product_Id + " = " + proid + " AND " + sTable_Product_Enabled + " = 1 AND " + sTable_Product_Deleted + " = 0";
        String orderBy = sTable_Product_Id;
        Cursor cursor = db.query(sTable_Product_Name, columns, selection, null,null, null, orderBy );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                product.setId(proid);
                product.setCat_id(cursor.getInt(cursor.getColumnIndex(sTable_Product_CatId)));
                product.setName(cursor.getString(cursor.getColumnIndex(sTable_Product_Name_Title)));
                product.setExtra_info(cursor.getString(cursor.getColumnIndex(sTable_Product_ExtraInfo)));
                product.setPrice(cursor.getDouble(cursor.getColumnIndex(sTable_Product_Price)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return product;
    }

    public List<Option> selectAllOptions(int proid) {
        List<Option> options = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Option_Id, sTable_Option_Name_Title, sTable_Option_ExtraInfo, sTable_Option_OrderIndex };
        String selection = sTable_Option_ProId + " = " + proid + " AND " + sTable_Option_Enabled + " = 1 AND " + sTable_Option_Deleted + " = 0";
        String orderBy = sTable_Option_OrderIndex;
        Cursor cursor = db.query(sTable_Option_Name, columns, selection, null,null, null, orderBy );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                Option option = new Option();
                option.setId(cursor.getInt(cursor.getColumnIndex(sTable_Option_Id)));
                option.setProd_id(proid);
                option.setName(cursor.getString(cursor.getColumnIndex(sTable_Option_Name_Title)));
                option.setExtra_info(cursor.getString(cursor.getColumnIndex(sTable_Option_ExtraInfo)));
                option.setOrderIndex(cursor.getInt(cursor.getColumnIndex(sTable_Option_OrderIndex)));
                options.add(option);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return options;
    }

    public String getOptionName(int optid) {
        String name = "";
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Option_Name_Title };
        String selection = sTable_Option_Id + " = " + optid;
        Cursor cursor = db.query(sTable_Option_Name, columns, selection, null,null, null, null );
        try {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                name = cursor.getString(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return name;
    }

    public boolean hasOptions(int proid) {
        SQLiteDatabase db = getReadableDatabase();
        long l = DatabaseUtils.queryNumEntries(db, sTable_Option_Name, sTable_Option_ProId + " = " + proid);
        return l > 0;
    }

    public void insert(EventInfo eventInfo) {
        SQLiteDatabase db = getWritableDatabase();
        //String path =  db.getPath();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_EventInfo_Name_Title, eventInfo.getName());
            values.put(sTable_EventInfo_Start_Date, eventInfo.getStart_date());
            values.put(sTable_EventInfo_End_Date, eventInfo.getEnd_date());
            db.insertOrThrow(sTable_EventInfo_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deleteDevice() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Device_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deletePrinter() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Printer_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deleteEventInfo() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_EventInfo_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deleteCategory() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Category_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deleteProduct() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Product_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deleteOption() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Option_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }
    // endregion

    // region Discover Printer View CRUD

    public void insertPrinterCategory(int printerid, int categoryid) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Printers_Category_PrinterId, printerid);
            values.put(sTable_Printers_Category_CategoryId, categoryid);
            db.insertOrThrow(sTable_Printers_Category_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deletePrinterCategory(int printerid, int categoryid) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            String where = sTable_Printers_Category_PrinterId + " = " + printerid + " AND " +
                    sTable_Printers_Category_CategoryId + " = " + categoryid;
            db.delete(sTable_Printers_Category_Name, where, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deletePrinterCategory() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Printers_Category_Name, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void bulkInsertPrinterCategory(List<PrinterCategoryPair> map) {
        SQLiteDatabase db = getWritableDatabase();
        String sql = "INSERT INTO "+ sTable_Printers_Category_Name +" VALUES (?,?);";
        SQLiteStatement statement = db.compileStatement(sql);
        db.beginTransaction();
        try {
            for(PrinterCategoryPair printerCategoryPair : map) {
                statement.clearBindings();
                statement.bindLong(1, printerCategoryPair.getPrintid());
                statement.bindLong(2, printerCategoryPair.getCategoryid());
                statement.execute();
            }
            db.setTransactionSuccessful();
        } catch (Exception ex) {

        } finally {
            db.endTransaction();
        }
    }

    public List<Integer> selectPrinterCategories(String printername) {
        List<Integer> categories = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT pc.category_id FROM printers_category pc, " +
                "printers p WHERE p.id = pc.printer_id AND p.id = " +
                "(SELECT id FROM printers WHERE name = '"+ printername +"') order by pc.category_id", null, null);
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                categories.add(cursor.getInt(0));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return categories;
    }

    public List<String> selectPrinterCategoriesName(String printername) {
        List<String> categories = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT c.name FROM printers_category pc, printers p, Category c " +
                "WHERE c.id = pc.category_id AND p.id = pc.printer_id AND p.id = " +
                "(SELECT id FROM printers WHERE name = '"+ printername +"') order by pc.category_id", null, null);
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                categories.add(cursor.getString(0));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return categories;
    }

    // endregion

    // region Order CRUD

    public void insertOrder(long orderid, String waiter, int printingmode, int status, String date_create, String table_id, int operation_mode, int sync) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Id, orderid);
            values.put(sTable_Order_WaiterName, waiter);
            values.put(sTable_Order_Printing_Mode, printingmode);
            values.put(sTable_Order_Status, status);
            values.put(sTable_Order_Created, date_create);
            values.put(sTable_Order_TableId, table_id);
            values.put(sTable_Order_Operation_Mode, operation_mode);
            values.put(sTable_Order_Sync, sync);
            db.insertOrThrow(sTable_Order_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void insertOrderCategory(long orderid, int categoryid, int status, String msg, int printing_option) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Category_OrderId, orderid);
            values.put(sTable_Order_Category_CategoryId, categoryid);
            values.put(sTable_Order_Category_Status, status);
            values.put(sTable_Order_Category_Message, msg);
            values.put(sTable_Order_Category_Print_Option, printing_option);
            values.put(sTable_Order_Category_Duplicated, 0);
            db.insertOrThrow(sTable_Order_Category_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void insertOrderCategoryProduct(long orderid, int categoryid, int productid, double price, int quantity, String desc) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Category_Product_ProductId, productid);
            values.put(sTable_Order_Category_Product_CategoryId, categoryid);
            values.put(sTable_Order_Category_Product_Price, price);
            values.put(sTable_Order_Category_Product_Quantity, quantity);
            values.put(sTable_Order_Category_Product_Description, desc);
            values.put(sTable_Order_Category_Product_OrderId, orderid);
            db.insertOrThrow(sTable_Order_Category_Product_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void insertOrderCategoryProductOption(long orderid, int productid, int optionid) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Category_Product_Option_ProductId, productid);
            values.put(sTable_Order_Category_Product_Option_OptionId, optionid);
            values.put(sTable_Order_Category_Product_Option_OrderId, orderid);
            db.insertOrThrow(sTable_Order_Category_Product_Option_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public boolean hasPendingOrder() {
        boolean hasOrder = false;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Order_Id };
        String where = sTable_Order_Status + " = 0";
        Cursor cursor = db.query(sTable_Order_Name, columns, where, null,null, null, null );
        try {
            hasOrder = cursor.getCount() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return hasOrder;
    }

    public List<Long> getPendingOrder() {
        List<Long> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Order_Id };
        String where = sTable_Order_Status + " = 0";
        Cursor cursor = db.query(sTable_Order_Name, columns, where, null,null, null, null );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                list.add(cursor.getLong(0));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return list;
    }

    public List<HistoryItem> getOrders(String username, int usertype, String tableid) {
        List<HistoryItem> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String where2 = " AND " + sTable_Order_TableId + "='" + tableid + "'";
        String[] columns = { sTable_Order_Id, sTable_Order_WaiterName, sTable_Order_Printing_Mode, sTable_Order_Status, sTable_Order_Created, sTable_Order_TableId };
        String where = sTable_Order_WaiterName + " = ? AND " + sTable_Order_Operation_Mode + " = ?";
        if(!tableid.equalsIgnoreCase("")) {
            where += where2;
        }
        Cursor cursor = db.query(sTable_Order_Name, columns, where, new String[] { username, String.valueOf(usertype) } ,null, null, sTable_Order_Created + " DESC" );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                HistoryItem historyItem = new HistoryItem();
                historyItem.setOrderid(cursor.getLong(cursor.getColumnIndex(sTable_Order_Id)));
                historyItem.setWaiter(cursor.getString(cursor.getColumnIndex(sTable_Order_WaiterName)));
                historyItem.setPrintingmode(cursor.getInt(cursor.getColumnIndex(sTable_Order_Printing_Mode)));
                historyItem.setStatus(cursor.getInt(cursor.getColumnIndex(sTable_Order_Status)));
                historyItem.setDate(cursor.getString(cursor.getColumnIndex(sTable_Order_Created)));
                historyItem.setTable(cursor.getString (cursor.getColumnIndex(sTable_Order_TableId)));
                historyItem.setSearchtable(cursor.getString(cursor.getColumnIndex(sTable_Order_TableId)));
                historyItem.setCategoryList(getCategories(historyItem.getOrderid()));
                list.add(historyItem);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return list;
    }

    public List<PrintCategory> getCategoriesHistory(long orderid) {
        SQLiteDatabase db = getReadableDatabase();
        List<PrintCategory> listPrintCategory = new ArrayList<>();
        String[] columns = { sTable_Order_Category_CategoryId, sTable_Order_Category_Status };
        String where = sTable_Order_Category_OrderId + " = ? ";
        Cursor cursor = db.query(sTable_Order_Category_Name, columns, where, new String[] { String.valueOf(orderid) } ,null, null, sTable_Order_Category_CategoryId );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                PrintCategory printCategory = new PrintCategory();
                printCategory.setCatid(cursor.getInt(cursor.getColumnIndex(sTable_Order_Category_CategoryId)));
                printCategory.setStatus(cursor.getInt(cursor.getColumnIndex(sTable_Order_Category_Status)));
                listPrintCategory.add(printCategory);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return listPrintCategory;
    }

    public List<PrintCategory> getCategories(long orderid) {
        SQLiteDatabase db = getReadableDatabase();
        List<PrintCategory> listPrintCategory = new ArrayList<>();
        List<Integer> list = new ArrayList<>();
        Hashtable<Integer, PrintCategory> hashtable = new Hashtable<>();
        String query = "SELECT ocp.category_id, c.name, ocp.product_id, p.name, ocp.price, ocp.quantity, ocp.description, p.extra_info " +
                "FROM order_category_product ocp, category c, products p " +
                "WHERE ocp.category_id = c.id AND ocp.product_id = p.id AND ocp.order_id = ? order by ocp.category_id";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(orderid) });
        try {
            if(cursor.getCount() > 0) {
                PrintCategory printCategory = null;
                List<PrintProduct> listProducts = null;
                for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    if(hashtable.containsKey(cursor.getInt(0))) {
                        printCategory = hashtable.get(cursor.getInt(0));
                    } else {
                        printCategory = new PrintCategory();
                        printCategory.setCatid(cursor.getInt(0));
                        printCategory.setCatname(cursor.getString(1));
                        printCategory.setStatus(isCategoryPrinted(orderid, cursor.getInt(0)) ? 2 : 0);
                    }
                    listProducts = printCategory.getListProduct();
                    PrintProduct printProduct = new PrintProduct();
                    printProduct.setId(cursor.getInt(2));
                    printProduct.setName(cursor.getString(3));
                    printProduct.setPrice(cursor.getDouble(4));
                    printProduct.setQuantity(cursor.getInt(5));
                    if(cursor.getString(6) != null) printProduct.setDescription(cursor.getString(6));
                    printProduct.setExtrainfo(cursor.getString(7));
                    listProducts.add(printProduct);
                    printProduct.setListOption(getPendingProductOption(orderid, printProduct.getId()));
                    printCategory.setListProduct(listProducts);
                    hashtable.put(cursor.getInt(0), printCategory);
                    if(!list.contains(cursor.getInt(0))) {
                        list.add(cursor.getInt(0));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        if(hashtable.size() > 0) {
            for(int id : list) {
                listPrintCategory.add(hashtable.get(id));
            }
        }
        return listPrintCategory;
    }

    public String getOrderTable(long orderid) {
        String table = "";
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Order_TableId };
        String where = sTable_Order_Id + " = ?";
        Cursor cursor = db.query(sTable_Order_Name, columns, where, new String[] { String.valueOf(orderid) },null, null, null );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                table =  cursor.getString(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return table;
    }

    public boolean isCategoryPrinted(long orderid, int catid) {
        boolean status = false;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Order_Category_Status };
        String where = sTable_Order_Category_OrderId + " = ? AND " + sTable_Order_Category_CategoryId + " = ?";
        Cursor cursor = db.query(sTable_Order_Category_Name, columns, where, new String[] { String.valueOf(orderid), String.valueOf(catid) },null, null, null );
        try {
            if(cursor.getCount() > 0) {
                cursor.moveToFirst();
                status = cursor.getInt(0) == OrderStatus.PRINTED.ordinal();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return status;
    }

    public void updateOrder(long orderid) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            String where = sTable_Order_Id + " = ?";
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Status, 2);
            db.update(sTable_Order_Name, values, where, new String[] { String.valueOf(orderid) });
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void updateOrder(int status) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            String where = sTable_Order_Id + " = ?";
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Status, status);
            db.update(sTable_Order_Name, values, null, null );
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public List<Integer> getPendingCategory(long orderid) {
        List<Integer> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Order_Category_CategoryId };
        String where = sTable_Order_Category_Status + " = 0 AND " + sTable_Order_Category_OrderId + " = ?";
        Cursor cursor = db.query(sTable_Order_Category_Name, columns, where, new String[] { String.valueOf(orderid) },null, null, null );
        try {
            for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                list.add(cursor.getInt(0));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return list;
    }

    public PrintCategory getPendingCategory(long orderid, int catid) {
        SQLiteDatabase db = getReadableDatabase();
        PrintCategory printCategory = null;
        String query = "SELECT ocp.category_id, c.name, ocp.product_id, p.name, ocp.price, ocp.quantity, ocp.description " +
                "FROM order_category_product ocp, category c, products p " +
                "WHERE ocp.category_id = c.id AND ocp.product_id = p.id AND ocp.category_id = ? AND ocp.order_id = ?";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(catid), String.valueOf(orderid) });
        try {
            if(cursor.getCount() > 0) {
                printCategory = new PrintCategory();
                List<PrintProduct> listProducts = new ArrayList<>();
                for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    printCategory.setCatid(catid);
                    printCategory.setCatname(cursor.getString(1));
                    PrintProduct printProduct = new PrintProduct();
                    printProduct.setId(cursor.getInt(2));
                    printProduct.setName(cursor.getString(3));
                    printProduct.setPrice(cursor.getDouble(4));
                    printProduct.setQuantity(cursor.getInt(5));
                    if(cursor.getString(6) != null) printProduct.setDescription(cursor.getString(6));
                    listProducts.add(printProduct);
                    printProduct.setListOption(getPendingProductOption(orderid, printProduct.getId()));
                }
                printCategory.setListProduct(listProducts);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return printCategory;
    }

    private List<PrintOption> getPendingProductOption(long orderid, int proid) {
        List<PrintOption> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT ocpo.option_id, op.name " +
                "FROM order_category_product_option ocpo, options op " +
                "WHERE ocpo.option_id = op.id AND ocpo.order_id = ? AND ocpo.product_id = ?";
        Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(orderid), String.valueOf(proid) });
        try {
            if(cursor.getCount() > 0) {
                for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    PrintOption printOption = new PrintOption();
                    printOption.setId(cursor.getInt(0));
                    printOption.setName(cursor.getString(1));
                    list.add(printOption);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return list;
    }

    public void insertPrintedCategory(long orderid, int catid, String mac) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(sTable_Printed_Category_OrderId, orderid);
            values.put(sTable_Printed_Category_CategoryId, catid);
            values.put(sTable_Printed_Category_Mac, mac);
            db.insertOrThrow(sTable_Printed_Category_Name, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deletePrintedCategory(long orderid) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(sTable_Printed_Category_Name, sTable_Printed_Category_OrderId + " = ?", new String[] { String.valueOf(orderid) });
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public List<PrintedCategory> getPrintedCategories() {
        List<PrintedCategory> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT order_id, category_id, mac FROM printed_category";
        Cursor cursor = db.rawQuery(query, null);
        try {
            if(cursor.getCount() > 0) {
                for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    PrintedCategory printedCategory = new PrintedCategory();
                    printedCategory.setOrderid(cursor.getInt(0));
                    printedCategory.setCatid(cursor.getInt(1));
                    printedCategory.setMac(cursor.getString(2));
                    list.add(printedCategory);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return list;
    }

    public void updatePrintedCategory(long orderid, int catid) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            String where = sTable_Order_Category_CategoryId + " = ? AND "
                    + sTable_Order_Category_OrderId + " = ?";
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Category_Status, 2);
            db.update(sTable_Order_Category_Name, values, where, new String[] { String.valueOf(catid), String.valueOf(orderid) });
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void updatePrintedCategory() {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            String where = sTable_Order_Category_CategoryId + " = ? AND "
                    + sTable_Order_Category_OrderId + " = ?";
            ContentValues values = new ContentValues();
            values.put(sTable_Order_Category_Status, 0);
            db.update(sTable_Order_Category_Name, values, null, null );
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public void deleteOrders(String waiter) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            String where = sTable_Order_WaiterName + " = ? ";
            db.delete(sTable_Order_Name, where, new String[] { waiter });
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //Log.d(TAG, "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    // endregion

    // region Device CRUD

    public boolean isPINValid(String pin, int level) {
        boolean isValid = false;
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = { sTable_Device_Waiter_Name };
        String where = "";
        if(level == 1) {
            where = sTable_Device_Level_1_Pin + " = ?";
        } else if(level == 2) {
            where = sTable_Device_Level_2_Pin + " = ?";
        }
        Cursor cursor = db.query(sTable_Device_Name, columns, where, new String[] { pin }, null, null, null);
        try {
            isValid = cursor.getCount() > 0;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(cursor != null && !cursor.isClosed()) cursor.close();
        }
        return isValid;
    }

    // endregion
}
