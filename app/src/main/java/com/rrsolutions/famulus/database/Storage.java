package com.rrsolutions.famulus.database;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.test.PerformanceTestCase;

import com.rrsolutions.famulus.common.Shared;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by hassanmushtaq on 08/12/2017.
 */

public class Storage {

    private static SharedPreferences pref;

    public Storage() {

    }

    public static void init(Context context) {
        if(pref == null)
            pref = context.getApplicationContext().getSharedPreferences(Shared.sStorageName, Activity.MODE_PRIVATE);
    }


    public static void save(String key, int value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void save(String key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void save(String key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void save(String key, Set<String> activityData) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putStringSet(key, activityData);
        editor.apply();
    }

    // type = 0 it is category and key starts with "cat" + id.
    // type = 1 it is product and key starts with "pro" + id.
    // type = 2 it is option and key starts with "opt" + id.
    // type = 3 it is option and key starts with "proopt" + id.

    public static void save(int id, String value, int type) {
        String key = "";
        SharedPreferences.Editor editor = pref.edit();
        if(type == 0) key = "cat";
        if(type == 1) key = "pro";
        if(type == 2) key = "opt";
        if(type == 3) key = "proopt";
        key += id;
        editor.putString(key, value);
        editor.apply();
    }

    public static void save(int id, Set<String> value, int type) {
        String key = "";
        SharedPreferences.Editor editor = pref.edit();
        if(type == 0) key = "cat";
        if(type == 1) key = "pro";
        if(type == 2) key = "opt";
        if(type == 3) key = "proopt";
        key += id;
        editor.putStringSet(key, value);
        editor.apply();
    }

    public static boolean exist(int id, int type){
        String key = "";
        if(type == 0) key = "cat";
        if(type == 1) key = "pro";
        if(type == 2) key = "opt";
        if(type == 3) key = "proopt";
        key += id;
        return pref.contains(key);
    }

    public static Map<String, Set<String>> getAllProducts() {
        Map<String, ?> map = pref.getAll();
        Set<String> keys = map.keySet();
        Map<String, Set<String>> result = new HashMap<>();
        String key = "pro";
        Iterator<String> iterator = keys.iterator();
        while(iterator.hasNext()) {
            String k = iterator.next();
            String kk = k.replace(key, "");
            if(Shared.isInteger(kk)) {
                Set<String> set = new HashSet<>();
                set = (Set<String>) map.get(k);
                result.put(k, set);
            }
        }
        return result;
    }

    public static List<String> getAllPrintedCategories() {
        Map<String, ?> map = pref.getAll();
        List<String> list = new ArrayList<>();
        String key = "order";
        for(String k : map.keySet()) {
            if(k.startsWith(key)) {
                list.add((String) map.get(k));
            }
        }
        return list;
    }

    public static Map<String, String> getAllPrintOrder() {
        Map<String, ?> map = pref.getAll();
        Set<String> keys = map.keySet();
        Map<String, String> result = new HashMap<>();
        String key = "printorder";
        Iterator<String> iterator = keys.iterator();
        while(iterator.hasNext()) {
            String k = iterator.next();
            String kk = k.replace(key, "");
            if(Shared.isInteger(kk)) {
                String string = "";
                string = (String) map.get(k);
                result.put(k, string);
            }
        }
        return result;
    }

    public static List<String> getAll() {
        Map<String, ?> map = pref.getAll();
        Set<String> keys = map.keySet();
        List<String> result = new ArrayList<>();
        Iterator<String> iterator = keys.iterator();
        while(iterator.hasNext()) {
            result.add(iterator.next());
        }
        return result;
    }

    public static int get(String key, int defaultValue) {
        return pref.getInt(key, defaultValue);
    }

    public static String get(String key, String defaultValue) {
        return pref.getString(key, defaultValue);
    }

    public static boolean get(String key, boolean defaultValue) {
        return pref.getBoolean(key, defaultValue);
    }

    public static Set<String> get(String key, Set<String> defaultValue) {
        return pref.getStringSet(key, defaultValue);
    }

    public static void remove(String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key);
        editor.apply();
    }

}
