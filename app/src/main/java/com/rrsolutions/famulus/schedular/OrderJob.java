package com.rrsolutions.famulus.schedular;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.printer.MyPrinter;
import com.rrsolutions.famulus.printer.PrintManagement;

import java.util.ArrayList;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class OrderJob extends Job {

    public static final String TAG = "order";

    @Override
    protected void onReschedule(int newJobId) {
        super.onReschedule(newJobId);
    }

    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {
        runPendingOrders();
        return Result.SUCCESS;
    }

    private class PendingOrders extends AsyncTask<String, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            PrintManagement printManagement = new PrintManagement(getContext());
            printManagement.updatePendingOrders();

            return true;
        }

        @Override
        protected void onPostExecute(Boolean aVoid) {
            SchedulerManagement schedulerManagement = new SchedulerManagement(getContext());
            schedulerManagement.createJob();


//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(3000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    boolean running = true;
//                    while (true) {
//                        if (!running) break;
//                        ArrayList<MyPrinter> arrayList = new ArrayList<>();
//                        for (MyPrinter myPrinter : Shared.hashtablePrinting.values()) {
//                            arrayList.add(myPrinter);
//                        }
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        if (arrayList.size() > 0) {
//                            for (MyPrinter myPrinter : arrayList) {
//                                if (myPrinter != null) {
//                                    if (myPrinter.isRunning()) {
//                                        running = true;
//                                        break;
//                                    } else {
//                                        running = false;
//                                    }
//                                } else {
//                                    running = false;
//                                }
//                            }
//                        } else {
//                            running = false;
//                        }
//                    }
//                    if (!running) {
//                        Shared.hashtablePrinting.clear();
//                        SchedulerManagement schedulerManagement = new SchedulerManagement(getContext());
//                        schedulerManagement.createJob();
//                    }
//
//                }
//            }).start();


//                int jobId = new JobRequest.Builder(OrderJob.TAG)
//                        .setExact(5000)
//                        .build()
//                        .schedule();
        }
    }

    private void runPendingOrders() {
        new PendingOrders().execute("");
//
//        PersistableBundleCompat extras = new PersistableBundleCompat();
//        extras.putString("key", "Hello world");
//
//        int jobId = new JobRequest.Builder(TAG)
//                .setExecutionWindow(30_000L, 40_000L)
//                .setBackoffCriteria(5_000L, JobRequest.BackoffPolicy.EXPONENTIAL)
//                .setRequiresCharging(true)
//                .setRequiresDeviceIdle(false)
//                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
//                .setExtras(extras)
//                .setRequirementsEnforced(true)
//                .setUpdateCurrent(true)
//                .build()
//                .schedule();
    }

}
