package com.rrsolutions.famulus.schedular;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class MyJobCreator implements JobCreator {

    @Override
    @Nullable
    public Job create(@NonNull String tag) {
        switch (tag) {
            case OrderJob.TAG:
                return new OrderJob();
            default:
                return null;
        }
    }
}