package com.rrsolutions.famulus.schedular;

import android.content.Context;
import android.widget.Toast;

import com.epson.epos2.Epos2Exception;
import com.evernote.android.job.JobRequest;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;

import java.util.Date;
import java.util.Random;

/**
 * Created by hassanmushtaq on 01/01/2018.
 */

public class SchedulerManagement {

    private Context mContext = null;

    public SchedulerManagement(Context context) {
        mContext = context;
    }

    public void createJob() {
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Storage.init(mContext);
        if(sqLiteDB.hasPendingOrder()) {
            Random r = new Random();
            int Low = 40;
            int High = 60;
            int Result = r.nextInt(High - Low) + Low;
            int jobId = createJob(Result * 1000);
            Storage.init(mContext);
            Storage.save(Shared.sJobIDKey, jobId);
            Toast.makeText(mContext, "Job started", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Job finished", Toast.LENGTH_SHORT).show();
        }
    }

    public void createJobNow() {
        Storage.save(Shared.sBackgroundWorkerRunatKey, Shared.sdfUTCDateTime.format(new Date()));
        int jobId = new JobRequest.Builder(OrderJob.TAG)
                .startNow()
                .build()
                .schedule();
        Storage.init(mContext);
        Storage.save(Shared.sJobIDKey, jobId);
    }

    private int createJob(int delay) {
        Storage.save(Shared.sBackgroundWorkerRunatKey, Shared.sdfUTCDateTime.format(new Date()));
        return new JobRequest.Builder(OrderJob.TAG)
                .setExact(delay)
                .build()
                .schedule();
    }

}
