package com.rrsolutions.famulus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.epson.epos2.printer.Printer;
import com.rrsolutions.famulus.businesslogic.MainMenu;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.businesslogic.OrderController;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.PrinterOption;
import com.rrsolutions.famulus.enumeration.UserType;

import java.util.HashSet;
import java.util.Set;

public class OrderActivity extends AppCompatActivity {

    private TextView txtTable = null;
    private TextView txtPrice = null;
    private Button btnPrint = null;
    private Button btnClose = null;
    private Button btnBack = null;
    private ExpandableListView elstOrder = null;

    private Context mContext = null;

    private boolean sessionexpired = false;
    private boolean activityChanged = false;
    private boolean dataChanged = false;
    private MainMenu mainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        mContext = this;
        txtTable = findViewById(R.id.txtTable);
        txtPrice = findViewById(R.id.txtPrice);
        btnPrint = findViewById(R.id.btnPrint);
        btnClose = findViewById(R.id.btnClose);
        btnBack = findViewById(R.id.btnBack);
        elstOrder = findViewById(R.id.elstOrder);

        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mContext.getString(R.string.order_preview_title));
        }

        mainMenu = new MainMenu(mContext);

        OrderController orderController = new OrderController();
        orderController.setListener(mContext);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!isSessionExpired()) {
            Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName());
            if(!isActivityChanged()) {
                saveSession();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(mContext, CategoryActivity.class);
        mContext.startActivity(intent);
    }

    public void saveSession() {
        if(((OrderActivity) mContext).isDataChanged()) {
            for (int i = 0; i < elstOrder.getCount(); i++) {
                View view = elstOrder.getChildAt(i);
                ElegantNumberButton ebtn = view.findViewById(R.id.ebtnQuantity);
                if (ebtn != null) {
                    if (ebtn.getVisibility() == View.VISIBLE) {
                        Storage.init(mContext);
                        OrderProduct orderProduct = (OrderProduct) ebtn.getTag();
                        Set<String> set = new HashSet<>();
                        set = Storage.get("pro" + orderProduct.getId(), set);
                        String descopt = "";
                        if(set.size() > 0) {
                            for (String string : set) {
                                String[] data = string.substring(0, string.lastIndexOf("|")).split("\\|");
                                if(data.length > 7) {
                                    descopt += "|" + data[7];
                                }
                                if(data.length > 8) {
                                    descopt += "|" + data[8];
                                }
                            }
                        }
                        Storage.remove("pro" + orderProduct.getId());
                        if( Integer.parseInt(ebtn.getNumber()) > 0) {
                            SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                            String catName = sqLiteDB.selectCategory(orderProduct.getCat_id()).getName();
                            String product_data = orderProduct.getCat_id() + "|" + catName + "|" + orderProduct.getId()
                                    + "|" + orderProduct.getName() + "|" + orderProduct.getExtra_info() + "|" + orderProduct.getPrice()
                                    + "|" + ebtn.getNumber() + descopt;
                            set.clear();
                            set.add(product_data + "|1");
                            Storage.save("pro" + orderProduct.getId(), set);
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        boolean isPrintingMode = Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal();
        if(isPrintingMode) {
            menu.findItem(R.id.action_printing_mode).setVisible(true);
            menu.findItem(R.id.action_normal_printing).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.CATEGORY.ordinal());
            menu.findItem(R.id.action_one_product_per_ticket).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.PRODUCT.ordinal());
        }
        mainMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }

    public TextView getTable() {
        return txtTable;
    }

    public TextView getPrice() {
        return txtPrice;
    }

    public Button getPrint() {
        return btnPrint;
    }

    public Button getClose() {
        return btnClose;
    }

    public Button getBack() {
        return btnBack;
    }

    public ExpandableListView getOrderList() {
        return elstOrder;
    }

    public boolean isActivityChanged() {
        return this.activityChanged;
    }

    public void setActivityChanged(boolean activityChanged) {
        this.activityChanged = activityChanged;
    }

    public boolean isSessionExpired() {
        return sessionexpired;
    }

    public void setSessionExpired(boolean sessionexpired) {
        this.sessionexpired = sessionexpired;
    }

    public boolean isDataChanged() {
        return dataChanged;
    }

    public void setDataChanged(boolean dataChanged) {
        this.dataChanged = dataChanged;
    }



}
