package com.rrsolutions.famulus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rrsolutions.famulus.businesslogic.CategoryController;
import com.rrsolutions.famulus.businesslogic.MainMenu;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.PrinterOption;
import com.rrsolutions.famulus.enumeration.UserType;

public class CategoryActivity extends AppCompatActivity {

    private RelativeLayout rlBottomBar = null;
    private View bottomLayout = null;
    private TextView txtWaiter = null;
    private GridView gvCategory = null;
    private Button btnBack = null;
    private Button btnForward = null;
    private MainMenu mainMenu = null;
    private Context mContext = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        mContext = this;

        txtWaiter = findViewById(R.id.txtWaiterName);
        gvCategory = findViewById(R.id.gvCategories);
        btnBack = findViewById(R.id.btnBack);
        btnForward = findViewById(R.id.btnForward);
        bottomLayout = findViewById(R.id.bottomBar);
        rlBottomBar = bottomLayout.findViewById(R.id.rlBottomBar);

        mainMenu = new MainMenu(mContext);
        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mContext.getString(R.string.category_title));
        }

        CategoryController controller = new CategoryController();
        controller.setListeners(mContext);
    }

    public TextView getWaiter() {
        return txtWaiter;
    }

    public GridView getCategory() {
        return gvCategory;
    }

    public Button getBack() {
        return btnBack;
    }

    public Button getForward() {
        return btnForward;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName());
    }

    public View getBottomBar() {
        return bottomLayout;
    }

    @Override
    public void onBackPressed() {
        Storage.init(mContext);
        if(Storage.get(Shared.sUserTypeKey, 0) == UserType.WAITER.ordinal()) {
            Intent intent = new Intent(mContext, TableActivity.class);
            mContext.startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        boolean isPrintingMode = Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal();
        if(isPrintingMode) {
            menu.findItem(R.id.action_printing_mode).setVisible(true);
            menu.findItem(R.id.action_normal_printing).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.CATEGORY.ordinal());
            menu.findItem(R.id.action_one_product_per_ticket).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.PRODUCT.ordinal());
        }
        mainMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }
}
