package com.rrsolutions.famulus;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.evernote.android.job.JobManager;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;

/**
 * Created by hassanmushtaq on 16/01/2018.
 */

public class AppClosedService extends Service {
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(), "Service started", Toast.LENGTH_LONG).show();
        //Log.d("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("Service", "Service Destroyed");
        //Log.d("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.i("Service", "onTaskRemoved()");
        //Log.e("ClearFromRecentService", "END");
        //Code here
        Storage.init(getApplicationContext());
        int jobId = Storage.get(Shared.sJobIDKey, 0);
        if(JobManager.instance().cancel(jobId)) {
            Toast.makeText(getApplicationContext(), "Job stopped", Toast.LENGTH_SHORT).show();
        } else {
            int jobId2 = Storage.get(Shared.sJobIDKey, 0);
            if(JobManager.instance().cancel(jobId2))
            Toast.makeText(getApplicationContext(), "Job stopped 2nd time", Toast.LENGTH_SHORT).show();
        }
        stopSelf();
    }
}
