package com.rrsolutions.famulus.businesslogic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.DiscoverPrintersActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.TableActivity;
import com.rrsolutions.famulus.adapter.PrinterAdapter;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.datawrapper.PrinterCategoryPair;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.modeljson.Category;
import com.rrsolutions.famulus.modeljson.Printer;
import com.rrsolutions.famulus.printer.MyNetwork;
import com.rrsolutions.famulus.printer.MyPrinter;
import com.rrsolutions.famulus.enumeration.OrderType;
import com.rrsolutions.famulus.printer.PrintOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 16/12/2017.
 */

public class DiscoverPrinterController {

    private PrinterAdapter listAdapter;
    private ExpandableListView expListView;
    private Button btnPrint = null;
    private Button btnSave = null;
    private List<String> listDataHeader;
    private HashMap<String, List<Category>> listHeaderChildren;
    private HashMap<String, Boolean> listHeaderStatus; // Printer is online or offline
    private HashMap<String, Printer> listPrinters;
    private Context mContext = null;
    private HashMap<String, boolean[]> mChildCheckStates;
    private DiscoverPrinters discoverPrinters = null;
    private List<Printer> printers = null;
    private List<Category> categories = null;
    private FilterOption filterOption = null;
    private boolean isFirstTime = true;
    private MyNetwork myNetwork = null;
    private String Printers = "";
    private String PrePrinters = "";
    private TSnackbar snackbar = null;

    public DiscoverPrinterController() {
        filterOption = new FilterOption();
        filterOption.setPortType(Discovery.PORTTYPE_TCP);
        filterOption.setBroadcast("255.255.255.255");
        filterOption.setDeviceModel(Discovery.MODEL_ALL);
        filterOption.setEpsonFilter(Discovery.FILTER_NAME);
        filterOption.setDeviceType(Discovery.TYPE_ALL);
    }

    public void startDiscovery() {
        try {
            snackbar = TSnackbar.make( ((Activity) mContext).findViewById(android.R.id.content), mContext.getString(R.string.discover_printer_searching), 5000);
            snackbar.show();
            isDiscoveryStarted = true;
            Shared.connectedPrinters.clear();
            if(myNetwork.isWifiConnected()) {
                Discovery.start(mContext, filterOption, mDiscoveryListener);
            } else {
                snackbar.dismiss();
                TSnackbar snackbar = TSnackbar.make( ((DiscoverPrintersActivity) mContext).findViewById(android.R.id.content),
                        mContext.getString(R.string.discover_printer_no_network), 3000);
                snackbar.show();
                if(expListView.getCount() > 0) {
                    ((DiscoverPrintersActivity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setOnlinePrinter(Shared.connectedPrinters);
                        }
                    });
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private DiscoveryListener mDiscoveryListener = new DiscoveryListener() {
        @Override
        public void onDiscovery(final DeviceInfo deviceInfo) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    String name = deviceInfo.getDeviceName();
                    String target = deviceInfo.getMacAddress();
                    String ip = deviceInfo.getIpAddress();
                    Shared.connectedPrinters.add(target);
                    Printers += "|" + target;
                    if(isFirstTime) {
                        setOnlinePrinter(Shared.connectedPrinters);
                    }
                }
            });
        }
    };

    public void setListener(Context context) {
        mContext = context;
        myNetwork = new MyNetwork(mContext);
        expListView = ((DiscoverPrintersActivity) mContext).getListView();
        btnPrint = ((DiscoverPrintersActivity) mContext).getBack();
        btnSave = ((DiscoverPrintersActivity) mContext).getForward();
        btnPrint.setOnClickListener(btnPrint_onClick);
        btnSave.setOnClickListener(btnSave_onClick);
        generateList();
    }

    private void print() {
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        if(Shared.connectedPrinters.size() > 0) {
            for(String mac : Shared.connectedPrinters) {
                if(Shared.isPrinterAvailable(mac)) {
                    MyPrinter myPrinter = new MyPrinter(mContext, mac);
                    PrintOrder printOrder = new PrintOrder(mContext);
                    printOrder.setPrintType(OrderType.TEST_PRINT.ordinal());
                    myPrinter.addinQueue(printOrder);
                    myPrinter.setPrinterData(sqLiteDB.getPrinter(mac));
                    new Thread(myPrinter).start();
                } else {
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(mContext.getString(R.string.discover_printer_offline_printer))
                            .setContentText(mContext.getString(R.string.discover_printer_offline_printer_msg))
                            .show();
                }
            }
        } else {
            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(mContext.getString(R.string.discover_printer_offline_printer))
                    .setContentText(mContext.getString(R.string.discover_printer_no_printers))
                    .show();
        }
    }

    private View.OnClickListener btnPrint_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!isDiscoveryStarted) {
                print();
                btnPrint.setVisibility(View.INVISIBLE);
                btnPrint.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btnPrint.setVisibility(View.VISIBLE);
                    }
                }, 3000);
            } else {
                new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(mContext.getString(R.string.discover_printer_searching))
                        .show();
            }
        }
    };

    private View.OnClickListener btnSave_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            HashMap<String, boolean[]> selectedItems = listAdapter.getmChildCheckStates();
            List<PrinterCategoryPair> pairs = new ArrayList<>();
            for(String key : selectedItems.keySet()) {
                boolean[] child = selectedItems.get(key);
                int k = 0;
                for(boolean c : child) {
                    if(c) {
                        Printer printer = listPrinters.get(key);
                        List<Category> categories =  listHeaderChildren.get(key);
                        pairs.add(new PrinterCategoryPair(printer.getId(), categories.get(k).getId()));
                    }
                    k++;
                }
            }
            if(pairs.size() > 0) {
                SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                sqLiteDB.deletePrinterCategory();
                sqLiteDB.bulkInsertPrinterCategory(pairs);
            }
            stop();
            Storage.init(mContext);
            if(Storage.get(Shared.sUserTypeKey, 0) == UserType.WAITER.ordinal()) {
                Intent intent = new Intent(mContext, TableActivity.class);
                mContext.startActivity(intent);
            } else {
                Intent intent = new Intent(mContext, CategoryActivity.class);
                mContext.startActivity(intent);
            }

        }
    };

    public void generateList() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SQLiteDB sqLiteDB = new SQLiteDB(mContext);
                printers = sqLiteDB.selectAllPrinters();
                categories = sqLiteDB.selectAllCategories();
                prepareListData();
                listAdapter = new PrinterAdapter(mContext, listDataHeader, listHeaderChildren, listHeaderStatus, mChildCheckStates);
                listAdapter.setExpandableListView(expListView);
                // setting list adapter
                expListView.setAdapter(listAdapter);
                refresh();
            }
        }, 100);
    }

    private void setOnlinePrinter(List<String> macs) {
        ((DiscoverPrintersActivity) mContext).updateActionBar(mContext.getString(R.string.discover_printer_title),
                mContext.getString(R.string.discover_printer_online).replace("[1]", String.valueOf(macs.size())));
        for(Printer printer : printers) {
            if(macs.contains(printer.getMac())) {
                listHeaderStatus.put(printer.getName(), true);
                listDataHeader.remove(printer.getName());
                listDataHeader.add(0, printer.getName());
            } else {
                listHeaderStatus.put(printer.getName(), false);
            }
        }
        if(!Printers.equalsIgnoreCase(PrePrinters)) {
            listAdapter.notifyDataSetChanged();
            for (int i = 0; i < listHeaderStatus.size(); i++) {
                if(listHeaderStatus.get(listDataHeader.get(i))) {
                    if(isFirstTime) expListView.expandGroup(i);
                } else {
                    expListView.collapseGroup(i);
                }
            }
        }

    }

    private void prepareListData() {
        listDataHeader = new ArrayList<>();
        listHeaderStatus = new HashMap<>();
        listPrinters = new HashMap<>();
        listHeaderChildren = new HashMap<>();
        mChildCheckStates = new HashMap<>();

        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        List<Category> child = new ArrayList<>();
        Category categoryOther = new Category();
        categoryOther.setId(-1);
        categoryOther.setName(mContext.getString(R.string.cat_others));
        categoryOther.setEnabled(true);
        categoryOther.setDeleted(false);

        for(Category category : categories) {
            child.add(category);
        }
        child.add(categoryOther);
        for(Printer printer : printers) {
            listHeaderStatus.put(printer.getName(), false);
            listDataHeader.add(printer.getName());
            listPrinters.put(printer.getName(), printer);
        }
        for(Printer printer : printers) {
            if(!listDataHeader.contains(printer.getName())) {
                listDataHeader.add(printer.getName());
            }
            if(!listPrinters.containsKey(printer.getName())) {
                listPrinters.put(printer.getName(), printer);
            }
        }
        for(int i = 0; i < listDataHeader.size(); i++) {
            listHeaderChildren.put(listDataHeader.get(i), child);
        }
        for(int i = 0; i < listDataHeader.size(); i++) {
            List<Integer> list = sqLiteDB.selectPrinterCategories(listDataHeader.get(i));
            List<Category> categories2 = sqLiteDB.selectAllCategories();
            categoryOther.setSelected(false);
            categories2.add(categoryOther);
            boolean[] selected = new boolean[categories2.size()];
            if(list.size() > 0) {
                for(int l : list) {
                    for(int j = 0; j < categories2.size(); j++) {
                        if(categories2.get(j).getId() == l) {
                            categories2.get(j).setSelected(true);
                            break;
                        }
                    }
                }
                for(int k = 0; k < categories2.size(); k++) {
                    selected[k] = categories2.get(k).isSelected();
                }
                mChildCheckStates.put(listDataHeader.get(i), selected);
            }
        }
    }

    public void stop() {
        try {
            isDiscoveryStarted = false;
            Discovery.stop();
        } catch (Epos2Exception e) {
            e.printStackTrace();
        }
    }

    public void refresh() {
        if(!isDiscoveryStarted) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startDiscovery();
                }
            }, 500);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    stop();
                    if(!isFirstTime) setOnlinePrinter(Shared.connectedPrinters);
                    isFirstTime = false;
                    if(Shared.connectedPrinters.size() == 0) {
                        TSnackbar snackbar = TSnackbar.make( ((DiscoverPrintersActivity) mContext).findViewById(android.R.id.content),
                                mContext.getString(R.string.discover_printer_no_printers), 3000);
                        snackbar.show();
                    }
                }
            }, 5000);
        }

    }

    private boolean isDiscoveryStarted = false;

//    @Override
//    public synchronized void run() {
//        while(isActivityRunning) {
//            try {
//                startDiscovery();
//                if(!isActivityRunning) break;
//                wait(4000);
//                stop();
//                if(!isFirstTime) {
//                    ((DiscoverPrintersActivity) mContext).runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            setOnlinePrinter(Shared.connectedPrinters);
//                        }
//                    });
//                }
//                PrePrinters = Printers;
//                if(!isActivityRunning) break;
//                wait(2000);
//                isFirstTime = false;
//                isFirstTime = Shared.connectedPrinters.size() == 0;
//            } catch (Exception ex) {
//
//            }
//        }
//    }
}
