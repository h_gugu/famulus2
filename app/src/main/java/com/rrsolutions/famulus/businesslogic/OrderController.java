package com.rrsolutions.famulus.businesslogic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.OrderActivity;
import com.rrsolutions.famulus.PrintOrderActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.TableActivity;
import com.rrsolutions.famulus.adapter.OrderAdapter;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.datawrapper.ProductData;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.UserType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 11/12/2017.
 */

public class OrderController {

    private Context mContext = null;
    private TextView txtTable = null;
    private TextView txtPrice = null;
    private Button btnPrint = null;
    private Button btnClose = null;
    private Button btnBack = null;
    private ExpandableListView elstOrder = null;
    private List<String> listDataHeader;
    private ArrayList<Integer> temp = new ArrayList<>();
    private HashMap<String, List<OrderProduct>> listDataChild2;
    private String sTable = "";
    private double totalPrice = 0.0;
    private Locale locale = null;

    public OrderController() {

    }

    public void setListener(Context context) {
        mContext = context;
        locale = Shared.getLocale(mContext);
        txtTable = ((OrderActivity) mContext).getTable();
        txtPrice = ((OrderActivity) mContext).getPrice();
        btnPrint = ((OrderActivity) mContext).getPrint();
        btnClose = ((OrderActivity) mContext).getClose();
        btnBack = ((OrderActivity) mContext).getBack();
        elstOrder = ((OrderActivity) mContext).getOrderList();
        Storage.init(mContext);
        sTable = Storage.get(Shared.sTableKey, "");
        if(sTable.equalsIgnoreCase("")) {
            txtTable.setText(mContext.getString(R.string.cashier));
        } else {
            txtTable.setText(mContext.getString(R.string.order_table).replace("[no]", sTable));
        }

        listDataHeader = new ArrayList<>();
        listDataChild2 = new HashMap<>();
        Map<String, Set<String>> map = Storage.getAllProducts();
        for(String key : map.keySet()) {
            Set<String> value = map.get(key);
            addProduct(value);
        }
        OrderAdapter orderAdapter = new OrderAdapter(mContext, listDataHeader, listDataChild2);
        orderAdapter.setPrice(txtPrice);
        elstOrder.setAdapter(orderAdapter);
        txtPrice.setText(mContext.getString(R.string.order_total).replace("[amount]", String.format(locale, "%.2f", totalPrice)));
        for(int i = 0; i < listDataHeader.size(); i++) {
            elstOrder.expandGroup(i);
        }
        btnClose.setOnClickListener(btnClose_onClick);
        btnPrint.setOnClickListener(btnPrint_onClick);
        btnBack.setOnClickListener(btnBack_onClick);
    }

    private void addProduct(Set<String> product) {
        int index = 0;
        int quantity = 0;
        OrderProduct orderProduct = new OrderProduct();
        List<OrderProduct> child2 = new ArrayList<>();
        String catName = "";
        for(String pro : product) {
            String[] data = pro.split("\\|");
            if(index == 0) {
                if (!listDataHeader.contains(data[1].trim())) {
                    listDataHeader.add(data[1].trim());
                }
                if(listDataChild2.containsKey(data[1].trim())) {
                    child2 = listDataChild2.get(data[1].trim());
                }
                orderProduct.setCat_id(Integer.parseInt(data[0].trim()));
                orderProduct.setId(Integer.parseInt(data[2].trim()));
                orderProduct.setName(data[3].trim());
                catName = data[1].trim();
                orderProduct.setExtra_info(data[4].trim());
                orderProduct.setPrice(Double.parseDouble(data[5].trim()));
                orderProduct.setType(1);
            }
            quantity += Integer.parseInt(data[6].trim());
            index++;
        }
        orderProduct.setQuantity(quantity);
        orderProduct.setShowquantity(index == 1);
        child2.add(orderProduct);
        totalPrice += orderProduct.getQuantity() * orderProduct.getPrice();
        listDataChild2.put(catName, child2);
    }

    private View.OnClickListener btnBack_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((OrderActivity) mContext).setActivityChanged(true);
            ((OrderActivity) mContext).saveSession();
            Intent intent = new Intent(mContext, CategoryActivity.class);
            mContext.startActivity(intent);
//            Storage.init(mContext);
//            String activityName = Storage.get(Shared.sLastOpenedActivityKey, "");
//            Class c = null;
//            Bundle bundle = new Bundle();
            try {
//                if(activityName.length() > 1) {
//                    if (activityName.contains("MultiProductsActivity")) {
//                        c = Class.forName(activityName);
//                        String data = Storage.get(Shared.sLastOpenedActivityDataKey, "");
//                        if (data.trim().length() > 0) {
//                            String[] activityData = data.split("\\|");
//                            if (activityData.length > 0) {
//                                bundle.putString("catname", activityData[0]);
//                                ProductData productData = new Gson().fromJson(activityData[1], ProductData.class);
//                                bundle.putParcelable("product", productData);
//                            }
//                        }
//                    } else if (activityName.contains("ProductsActivity")) {
//                        String data = Storage.get(Shared.sLastOpenedActivityDataKey, "");
//                        if(data.trim().length() > 0) {
//                            String[] activityData = data.split("\\|");
//                            if(activityData.length > 0) {
//                                bundle.putInt("catid", Integer.parseInt(activityData[0]));
//                                bundle.putString("catname", activityData[1]);
//                            }
//                        }
//                    }
//                    Intent intent = new Intent(mContext, c);
//                    intent.putExtras(bundle);
//                    mContext.startActivity(intent);
//                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    };

    private View.OnClickListener btnPrint_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((OrderActivity) mContext).saveSession();
            Storage.init(mContext);
            Map<String, Set<String>> products = Storage.getAllProducts();
            if(products.size() > 0) {
                ((OrderActivity) mContext).setActivityChanged(true);
                saveOrderPrint();
                Intent intent = new Intent(mContext, PrintOrderActivity.class);
                mContext.startActivity(intent);
            } else {
                new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(mContext.getString(R.string.order_preview_err_empty))
                        .setContentText(mContext.getString(R.string.order_preview_err_empty_detail))
                        .show();
            }
        }
    };

    private View.OnClickListener btnClose_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((OrderActivity) mContext).setDataChanged(false);
            new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(mContext.getString(R.string.delete_confirmation))
                    .setContentText(mContext.getString(R.string.order_delete_session))
                    .setConfirmText(mContext.getString(R.string.ok))
                    .setCancelText(mContext.getString(R.string.cancel))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            ((OrderActivity) mContext).setSessionExpired(true);
                            Shared.clearSession(mContext);
                            sDialog.dismissWithAnimation();
                            Storage.init(mContext);
                            if(Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal()) {
                                Intent intent = new Intent(mContext, CategoryActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Intent intent = new Intent(mContext, TableActivity.class);
                                mContext.startActivity(intent);
                            }

                        }
                    })
                    .show();
        }
    };

    private void saveOrderPrint() {
        Storage.init(mContext);
        Map<String, Set<String>> map = Storage.getAllProducts();
        for(String key : map.keySet()) {
            Set<String> set = map.get(key);
            String k = key.replace("pro", "");
            int quantity = 0;
            if(set.size() > 0) {
                String[] data = null;
                for (String string : set) {
                    data = string.split("\\|");
                    quantity += Integer.parseInt(data[6]);
                }
                data[6] = String.valueOf(quantity);
                String str = "";
                for(int i = 0; i < data.length; i++) {
                    str += "|" + data[i];
                }
                Storage.save("printorder" + k, str.substring(1));
            }
        }
    }

}
