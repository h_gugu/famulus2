package com.rrsolutions.famulus.businesslogic;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.rrsolutions.famulus.LoginActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.modeljson.Login;
import com.rrsolutions.famulus.network.WebManagement;

import java.util.Calendar;
import java.util.GregorianCalendar;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * Created by hassanmushtaq on 05/12/2017.
 */

public class LoginController {

    private Context mContext = null;
    private EditText edtUsername = null;
    private EditText edtPassword = null;
    private Button btnLogin = null;
    private TextView txtMarker = null;
    private SwitchCompat swhSaveCredentials = null;

    private String sessionKey = "";
    private SweetAlertDialog pDialog = null;

    public LoginController() {

    }

    public void setListeners(Context context) {
        mContext = context;

        edtUsername = ((LoginActivity) mContext).getUsername();
        edtPassword = ((LoginActivity) mContext).getPassword();
        btnLogin = ((LoginActivity) mContext).getLogin();
        txtMarker = ((LoginActivity) mContext).getMarker();
        swhSaveCredentials = ((LoginActivity) mContext).getSaveCredentials();

        Calendar calendar = new GregorianCalendar();
        int year = calendar.get(Calendar.YEAR);
        String marker = mContext.getString(R.string.login_txt_marker);
        marker = marker.replace("[app_name]", mContext.getString(R.string.app_name));
        marker = marker.replace("[year]", year + "");
        //edtPassword.setInputType(129);
        edtPassword.setTag("0");
        txtMarker.setText(Shared.fromHtml(marker));
        btnLogin.setOnClickListener(btnLogin_onClick);
        edtPassword.setOnTouchListener(edtPassword_onTouch);
        edtPassword.setOnEditorActionListener(edtPassword_onEditorKeyPress);

        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Login login = sqLiteDB.getLogin();
        if(login != null) {
            edtUsername.setText(login.getUsername());
            edtPassword.setText(login.getPassword());
        } else {
            edtUsername.requestFocus();
        }
    }

    private boolean isValid() {
        if(edtUsername.getText().toString().trim().equalsIgnoreCase("")) {
            return false;
        }
        if(edtPassword.getText().toString().trim().equalsIgnoreCase("")) {
            return false;
        }
        return true;
    }

    private View.OnClickListener btnLogin_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isValid()) {
                WebManagement webManagement = new WebManagement(mContext);
                SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                webManagement.login(edtUsername.getText().toString(), edtPassword.getText().toString());
                if(swhSaveCredentials.isChecked()) {
                    sqLiteDB.insert(edtUsername.getText().toString().toLowerCase(), edtPassword.getText().toString());
                } else {
                    sqLiteDB.deleteLogin();
                }
            } else {
                ((LoginActivity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.dismissWithAnimation();
                    }
                });
                Shared.showAlert(mContext, mContext.getString(R.string.login_title), mContext.getString(R.string.login_err_empty_user_pass), SweetAlertDialog.ERROR_TYPE);
            }
        }
    };

    private View.OnTouchListener edtPassword_onTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (edtPassword.getRight() - edtPassword.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    if(edtPassword.getTag().toString().equalsIgnoreCase("0")) {
                        edtPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye_outline, 0);
                        Shared.TogglePasswordField(edtPassword, true);
                        edtPassword.setTag("1");
                    } else {
                        edtPassword.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.eye, 0);
                        Shared.TogglePasswordField(edtPassword, false);
                        edtPassword.setTag("0");
                    }
                    return true;
                }
            }
            return false;
        }
    };

    private TextView.OnEditorActionListener edtPassword_onEditorKeyPress = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_GO
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || (event.getAction() == KeyEvent.KEYCODE_ENTER)) {
                if(isValid()) {
                    WebManagement webManagement = new WebManagement(mContext);
                    SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                    webManagement.login(edtUsername.getText().toString(), edtPassword.getText().toString());
                    if(swhSaveCredentials.isChecked()) {
                        sqLiteDB.insert(edtUsername.getText().toString().toLowerCase(), edtPassword.getText().toString());
                    }
                } else {
                    ((LoginActivity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pDialog.cancel();
                        }
                    });
                    Shared.showAlert(mContext, mContext.getString(R.string.login_title), mContext.getString(R.string.login_err_empty_user_pass), SweetAlertDialog.ERROR_TYPE);
                }
                return true;
            }
            return false;
        }
    };


}
