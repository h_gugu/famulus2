package com.rrsolutions.famulus.businesslogic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.PrintOrderActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.TableActivity;
import com.rrsolutions.famulus.adapter.PrintOrderAdapter;
import com.rrsolutions.famulus.common.DecimalFilter;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.enumeration.OrderStatus;
import com.rrsolutions.famulus.enumeration.SyncStatus;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.printer.PrintManagement;
import com.rrsolutions.famulus.schedular.SchedulerManagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 24/12/2017.
 */

public class PrintOrderController {

    private Context mContext = null;
    private ExpandableListView elstOrder = null;
    private Button btnBack = null;
    private Button btnForward = null;
    private TextView txtSplit = null;
    private TextView txtRest = null;
    private List<String> listDataHeader;
    private HashMap<String, List<OrderProduct>> listDataChild2;
    private double totalAmount = 0;
    private long orderId = 0;
    private PrintOrderAdapter printOrderAdapter = null;
    private Locale locale = null;

    public PrintOrderController() {

    }

    public void setListener(Context context) {
        mContext = context;
        locale = Shared.getLocale(mContext);
        elstOrder = ((PrintOrderActivity) mContext).getOrder();
        btnBack = ((PrintOrderActivity) mContext).getBack();
        btnForward = ((PrintOrderActivity) mContext).getForward();
        txtSplit = ((PrintOrderActivity) mContext).getSplit();
        txtRest = ((PrintOrderActivity) mContext).getRest();

        txtSplit.setText(mContext.getString(R.string.print_order_split) + " " + String.format(locale, "%.2f", 0.0));
        txtRest.setText(mContext.getString(R.string.print_order_rest) + " " + String.format(locale, "%.2f", 0.0));

        btnBack.setOnClickListener(btnSplit_onClick);
        btnForward.setOnClickListener(btnOneTime_onClick);

        orderId = Shared.makeOrderId();

        generate();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                saveOrder();
                SchedulerManagement schedulerManagement = new SchedulerManagement(mContext);
                schedulerManagement.createJobNow();
            }
        }, 500);
    }

    private void generate() {
        listDataHeader = new ArrayList<>();
        listDataChild2 = new HashMap<>();
        Storage.init(mContext);
        Map<String, String> map = Storage.getAllPrintOrder();
        for (String key : map.keySet()) {
            String value = map.get(key);
            String[] data = value.split("\\|");
            if(!listDataHeader.contains(data[1])) {
                listDataHeader.add(data[1]);
            }
            List<OrderProduct> child2 = new ArrayList<>();
            if(listDataChild2.containsKey(data[1].trim())) {
                child2 = listDataChild2.get(data[1].trim());
            }
            OrderProduct orderProduct = new OrderProduct();
            orderProduct.setCat_id(Integer.parseInt(data[0].trim()));
            orderProduct.setId(Integer.parseInt(data[2].trim()));
            orderProduct.setName(data[3].trim());
            orderProduct.setExtra_info(data[4].trim());
            orderProduct.setPrice(Double.parseDouble(data[5].trim()));
            orderProduct.setType(1);
            orderProduct.setQuantity(Integer.parseInt(data[6]));
            child2.add(orderProduct);
            totalAmount += Double.parseDouble(data[5].trim()) * Integer.parseInt(data[6]);
            listDataChild2.put(data[1], child2);
        }
        printOrderAdapter = new PrintOrderAdapter(mContext, listDataHeader, listDataChild2);
        printOrderAdapter.setTotalAmount(totalAmount);
        printOrderAdapter.setRest(txtRest);
        printOrderAdapter.setSplit(txtSplit);
        elstOrder.setAdapter(printOrderAdapter);
        txtRest.setText(mContext.getString(R.string.print_order_rest) + " " + String.format(locale, "%.2f", totalAmount));
        for(int i = 0; i < listDataHeader.size(); i++) {
            elstOrder.expandGroup(i);
        }
    }

    private View.OnClickListener btnSplit_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            paymentDialog();
        }
    };

    private View.OnClickListener btnOneTime_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fullPaymentDialog();
        }
    };

    private void fullPaymentDialog() {
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_payment, null);
        final TextView txtAmount = grid.findViewById(R.id.txtAmount);
        final EditText edtGiven = grid.findViewById(R.id.edtGiven);
        txtAmount.setText(String.format(locale, "%.2f", totalAmount));
        edtGiven.addTextChangedListener(new DecimalFilter(edtGiven, (Activity) mContext));
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText(mContext.getString(R.string.payment_title))
                .setConfirmText(mContext.getString(R.string.payment_confirm))
                .setCancelText(mContext.getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog dialog) {
                        dialog.dismissWithAnimation();
                        Shared.clearSession(mContext);
                        Storage.init(mContext);
                        if(Storage.get(Shared.sUserTypeKey, 0) == UserType.WAITER.ordinal()) {
                            Intent intent = new Intent(mContext, TableActivity.class);
                            mContext.startActivity(intent);
                        } else {
                            Intent intent = new Intent(mContext, CategoryActivity.class);
                            mContext.startActivity(intent);
                        }

                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog dialog) {
                        dialog.dismissWithAnimation();
                    }
                }).show();
        TextView txt = sweetAlertDialog.findViewById(cn.pedant.SweetAlert.R.id.title_text);
        if (txt != null) {
            txt.setWidth(Shared.getScreenDimension(mContext, true));
        }
    }

    double dRest = 0.0;
    private void paymentDialog() {
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_payment, null);
        final TextView txtAmount = grid.findViewById(R.id.txtAmount);
        final EditText edtGiven = grid.findViewById(R.id.edtGiven);
        String sp = this.txtSplit.getText().toString().substring(mContext.getString(R.string.print_order_split).length()).trim();
        String rs = this.txtRest.getText().toString().substring(mContext.getString(R.string.print_order_rest).length()).trim();
        double split = Double.parseDouble(sp);
        txtAmount.setText(String.format(locale, "%.2f", totalAmount));
        txtAmount.setText(String.format(locale, "%.2f", split));
        dRest = split;
        edtGiven.addTextChangedListener(new DecimalFilter(edtGiven, (Activity) mContext));
        if(split > 0) {
            SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE);
            sweetAlertDialog.setCancelable(false);
            sweetAlertDialog.setTitleText(mContext.getString(R.string.payment_title))
                    .setConfirmText(mContext.getString(R.string.payment_confirm))
                    .setCancelText(mContext.getString(R.string.cancel))
                    .setCustomView(grid)
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog dialog) {
                            if (edtGiven.getText().toString().length() > 0) {
                                double given = Double.parseDouble(edtGiven.getText().toString());
                                if (given >= dRest) {
                                    printOrderAdapter.notifyDataSetChanged();
                                    txtSplit.setText(mContext.getString(R.string.print_order_split) + " " + String.format(locale, "%.2f", 0));
                                    totalAmount = Double.parseDouble(PrintOrderController.this.txtRest.getText().toString().substring(mContext.getString(R.string.print_order_rest).length()).trim());
                                    dRest = 0;
                                    printOrderAdapter.setTotalAmount(totalAmount);
                                    dialog.dismissWithAnimation();
                                    if (totalAmount == 0) {
                                        dialog.dismissWithAnimation();
                                        Shared.clearSession(mContext);
                                        Storage.init(mContext);
                                        if(Storage.get(Shared.sUserTypeKey, 0) == UserType.WAITER.ordinal()) {
                                            Intent intent = new Intent(mContext, TableActivity.class);
                                            mContext.startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(mContext, CategoryActivity.class);
                                            mContext.startActivity(intent);
                                        }
                                    }
                                } else {
                                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                            .setTitleText(mContext.getString(R.string.payment_title))
                                            .setContentText(mContext.getString(R.string.payment_err_enter_correct_amount))
                                            .setConfirmText(mContext.getString(R.string.ok))
                                            .setConfirmClickListener(null)
                                            .show();
                                }
                            } else {
                                new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                                        .setTitleText(mContext.getString(R.string.payment_title))
                                        .setContentText(mContext.getString(R.string.payment_err_enter_amount))
                                        .setConfirmText(mContext.getString(R.string.ok))
                                        .setConfirmClickListener(null)
                                        .show();
                            }
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog dialog) {
                            dialog.dismissWithAnimation();
                        }
                    }).show();
            TextView txt = sweetAlertDialog.findViewById(cn.pedant.SweetAlert.R.id.title_text);
            if (txt != null) {
                txt.setWidth(Shared.getScreenDimension(mContext, true));
            }
        } else {
            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(mContext.getString(R.string.payment_title))
                    .setContentText(mContext.getString(R.string.payment_err_select_product))
                    .setConfirmText(mContext.getString(R.string.ok))
                    .setConfirmClickListener(null)
                    .show();
        }
    }

    private boolean isCategoryPrinted(List<String> list, int catId) {
        boolean status = false;
        for(String string : list) {
            String[] data = string.split("\\|");
            if(data.length > 0) {
                for(String str : data) {
                    String[] d = str.split(":");
                    if( Integer.parseInt(d[1]) == catId) {
                        status = true;
                        break;
                    }
                }
            }
            if(status) break;
        }
        return status;
    }

    private boolean isAllCategoriesPrinted(List<String> list, List<Integer> catIds) {
        boolean status = false;
        for(int catId : catIds) {
            status = isCategoryPrinted(list, catId);
            if(!status) break;
        }
        return status;
    }

    private void saveOrder() {
        List<Integer> catIds = new ArrayList<>();
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Storage.init(mContext);
        String waiter = sqLiteDB.getWaiterName();
        String created = Shared.sdfUTCDateTime.format(new Date());
        String table = Storage.get(Shared.sTableKey, "");
        Map<String, Set<String>> map = Storage.getAllProducts();
        //List<String> listPrintedCategories = Storage.getAllPrintedCategories();
        for(String key : map.keySet()) {
            Set<String> values = map.get(key);
            for(String value : values) {
                String desc = "";
                String opts = "";
                String[] data = value.substring(0, value.lastIndexOf("|")).split("\\|");
                if(data.length > 7) {
                    if(data[7].startsWith("d")) {
                        desc = data[7].substring(1);
                    } else {
                        opts = data[7].substring(1);
                    }
                }
                if(data.length > 8) {
                    opts = data[8].substring(1);
                }
                if(!catIds.contains(Integer.parseInt(data[0]))) {
                    catIds.add(Integer.parseInt(data[0]));
                    //boolean b = isCategoryPrinted(listPrintedCategories, Integer.parseInt(data[0]));
                    sqLiteDB.insertOrderCategory(orderId, Integer.parseInt(data[0]), OrderStatus.NON_PRINTED.ordinal(),
                            "", Storage.get(Shared.sPrintingOptionKey, 0));
                }
                sqLiteDB.insertOrderCategoryProduct(orderId, Integer.parseInt(data[0]), Integer.parseInt(data[2]),
                        Double.parseDouble(data[5]), Integer.parseInt(data[6]), desc);
                if(opts.length() > 0) {
                    String[] opt = opts.split(",");
                    for(String o : opt) {
                        sqLiteDB.insertOrderCategoryProductOption(orderId, Integer.parseInt(data[2]), Integer.parseInt(o));
                    }
                }
            }
        }
        //boolean st = isAllCategoriesPrinted(listPrintedCategories, catIds);
        sqLiteDB.insertOrder(orderId, waiter, Storage.get(Shared.sPrintingOptionKey, 0),
                OrderStatus.NON_PRINTED.ordinal(), created, table, Storage.get(Shared.sUserTypeKey, 0), SyncStatus.NOT_SYNC.ordinal());
    }

   /*
    private void saveOrder() {
        List<Integer> catIds = new ArrayList<>();
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Storage.init(mContext);
        String waiter = sqLiteDB.getWaiterName();
        String created = Shared.sdfUTCDateTime.format(new Date());
        String table = Storage.get(Shared.sTableKey, "");
        Map<String, Set<String>> map = Storage.getAllProducts();
        List<String> listPrintedCategories = Storage.getAllPrintedCategories();
        for(String key : map.keySet()) {
            Set<String> values = map.get(key);
            for(String value : values) {
                String desc = "";
                String opts = "";
                String[] data = value.substring(0, value.lastIndexOf("|")).split("\\|");
                if(data.length > 7) {
                    if(data[7].startsWith("d")) {
                        desc = data[7].substring(1);
                    } else {
                        opts = data[7].substring(1);
                    }
                }
                if(data.length > 8) {
                    opts = data[8].substring(1);
                }
                if(!catIds.contains(Integer.parseInt(data[0]))) {
                    catIds.add(Integer.parseInt(data[0]));
                    boolean b = isCategoryPrinted(listPrintedCategories, Integer.parseInt(data[0]));
                    sqLiteDB.insertOrderCategory(orderId, Integer.parseInt(data[0]), b ? 1 : 0, "");
                }
                sqLiteDB.insertOrderCategoryProduct(orderId, Integer.parseInt(data[0]), Integer.parseInt(data[2]),
                        Double.parseDouble(data[5]), Integer.parseInt(data[6]), desc);
                if(opts.length() > 0) {
                    String[] opt = opts.split(",");
                    for(String o : opt) {
                        sqLiteDB.insertOrderCategoryProductOption(orderId, Integer.parseInt(data[2]), Integer.parseInt(o));
                    }
                }
            }
        }
        boolean st = isAllCategoriesPrinted(listPrintedCategories, catIds);
        sqLiteDB.insertOrder(orderId, waiter, 0, st ? 1 : 0, created, table);
    }*/
}
