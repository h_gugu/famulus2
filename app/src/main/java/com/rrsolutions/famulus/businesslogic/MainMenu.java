package com.rrsolutions.famulus.businesslogic;

import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.DiscoverPrintersActivity;
import com.rrsolutions.famulus.ExitActivity;
import com.rrsolutions.famulus.HistoryActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.TableActivity;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.PrinterOption;
import com.rrsolutions.famulus.modeljson.Category;
import com.rrsolutions.famulus.modeljson.Product;
import com.rrsolutions.famulus.printer.MyPrinter;
import com.rrsolutions.famulus.enumeration.OrderType;
import com.rrsolutions.famulus.printer.PrintOrder;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 29/12/2017.
 */

public class MainMenu {

    private Context mContext = null;
    private Menu menu = null;

    public MainMenu(Context context) {
        mContext = context;
    }

    public void handleMainMenu(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_printer_setting:
                Shared.showPINDialog(mContext, mContext.getString(R.string.app_name), DiscoverPrintersActivity.class, false);
                break;
            case R.id.action_about:
                break;
            case R.id.action_order_history:
                Intent intent1 = new Intent(mContext, HistoryActivity.class);
                mContext.startActivity(intent1);
                break;
            case R.id.action_exit:
                ExitActivity.exitApplication(mContext);
                break;
            case R.id.action_reset:
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(mContext.getString(R.string.delete_confirmation))
                        .setContentText(mContext.getString(R.string.clear_session))
                        .setConfirmText(mContext.getString(R.string.yes))
                        .setCancelText(mContext.getString(R.string.cancel))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                Shared.clearSession(mContext);
                                sDialog.dismissWithAnimation();
                                Intent intent = new Intent(mContext, TableActivity.class);
                                mContext.startActivity(intent);
                            }
                        })
                        .show();
                break;
            case R.id.action_refresh:
                ((DiscoverPrintersActivity) mContext).refresh();
                break;
            case R.id.action_search:
                ((HistoryActivity) mContext).showSearch();
                break;
            case R.id.action_print_products:
                SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                String[] macs = sqLiteDB.getPrinterMacOther(-1);
                for(String mac : macs) {
                    if(Shared.isPrinterAvailable(mac)) {
                        MyPrinter myPrinter = new MyPrinter(mContext, mac);
                        PrintOrder printOrder = new PrintOrder(mContext);
                        printOrder.setPrintType(OrderType.PRODUCT_PRICES.ordinal());
                        myPrinter.addinQueue(printOrder);
                        //myPrinter.setPrinterData(sqLiteDB.getPrinter(mac));
                        new Thread(myPrinter).start();
                    }
                }
                break;
            case R.id.action_logout:
                Shared.showPINDialog(mContext, mContext.getString(R.string.app_name), null, true);
                break;
            case R.id.action_select_all:
                ((HistoryActivity) mContext).showSelectCheckBox();
                break;
            case R.id.action_test:
                Storage.init(mContext);
                SQLiteDB sqLiteDB1 = SQLiteDB.getInstance(mContext);
                List<Category> categoryList = sqLiteDB1.selectAllCategories();
                for(Category category : categoryList) {
                    List<Product> productList = sqLiteDB1.selectAllProducts(category.getId());
                    for(Product product : productList) {
                        String product_data = "";
                        product_data = category.getId() + "|" + category.getName() + "|" + product.getId() + "|" + product.getName() + "|"
                                + product.getExtra_info() + "|" + product.getPrice() + "|" + "1" + "|1";
                        Set<String> proopt = new HashSet<>();
                        proopt.add(product_data);
                        Storage.save(product.getId(), proopt, 1);
                    }
                }
                Intent intent = new Intent(mContext, CategoryActivity.class);
                mContext.startActivity(intent);
                break;
            case R.id.action_normal_printing:
                menu.findItem(R.id.action_printing_mode).getSubMenu().findItem(R.id.action_normal_printing).setChecked(true);
                menu.findItem(R.id.action_printing_mode).getSubMenu().findItem(R.id.action_one_product_per_ticket).setChecked(false);
                Storage.init(mContext);
                Storage.save(Shared.sPrintingOptionKey, PrinterOption.CATEGORY.ordinal());
                break;
            case R.id.action_one_product_per_ticket:
                menu.findItem(R.id.action_printing_mode).getSubMenu().findItem(R.id.action_normal_printing).setChecked(false);
                menu.findItem(R.id.action_printing_mode).getSubMenu().findItem(R.id.action_one_product_per_ticket).setChecked(true);
                Storage.init(mContext);
                Storage.save(Shared.sPrintingOptionKey, PrinterOption.PRODUCT.ordinal());
                break;
        }
    }

    public void hide(MenuItem item) {
        item.setVisible(false);
    }

    public void show(MenuItem item) {
        item.setVisible(true);
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
