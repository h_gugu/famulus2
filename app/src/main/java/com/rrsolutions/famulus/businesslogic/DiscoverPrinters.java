package com.rrsolutions.famulus.businesslogic;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.epson.epos2.Epos2Exception;
import com.epson.epos2.discovery.DeviceInfo;
import com.epson.epos2.discovery.Discovery;
import com.epson.epos2.discovery.DiscoveryListener;
import com.epson.epos2.discovery.FilterOption;
import com.rrsolutions.famulus.DiscoverPrintersActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.printer.ConnectedPrinter;
import com.rrsolutions.famulus.printer.MyNetwork;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 10/12/2017.
 */

public class DiscoverPrinters {

    private Context mContext = null;
    private Calendar calendarStart = null;
    private boolean isRunning = true;
    private boolean isDiscovered = true;
    private boolean isStop = false;

    private int iFoundPrinter = 0;
    private int iWaitNormal = 500;
    private int iWaitAfterFound = 3000;
    private int iWaitNoFound = 5000;

    private MyNetwork myNetwork = null;
    private List<String> connectedPrinter = new ArrayList<>();

    public DiscoverPrinters(Context context) {
        mContext = context;
        myNetwork = new MyNetwork(mContext);
        myNetwork.fetchNetworkInfo();
    }

    public void start() {
        isRunning = true;
        if(myNetwork.isWifiConnected()) {
//            Shared.showSnackBar((DiscoverPrintersActivity) mContext, mContext.getString(R.string.discover_printer_searching),
//                    5000, mContext.getString(R.string.dismiss), null);
            connectedPrinter.clear();
            calendarStart = new GregorianCalendar();
            FilterOption filterOption = null;
            filterOption = new FilterOption();
            filterOption.setPortType(Discovery.PORTTYPE_TCP);
            filterOption.setBroadcast("255.255.255.255");
            filterOption.setDeviceModel(Discovery.MODEL_ALL);
            filterOption.setEpsonFilter(Discovery.FILTER_NAME);
            filterOption.setDeviceType(Discovery.TYPE_ALL);
            try {
                Discovery.start(mContext, filterOption, mDiscoveryListener);
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            while (!isStop) {
//                                Thread.sleep(iWaitNormal);
//                                Calendar now = new GregorianCalendar();
//                                if (isDiscovered) {
//                                    long seconds = (now.getTimeInMillis() - calendarStart.getTimeInMillis());
//                                    if (seconds >= iWaitAfterFound) {
//                                        stopThread();
//                                        break;
//                                    }
//                                } else {
//                                    long seconds = (now.getTimeInMillis() - calendarStart.getTimeInMillis());
//                                    if (seconds >= iWaitNoFound) {
//                                        stopThread();
//                                        break;
//                                    }
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            isRunning = false;
//                            isStop = true;
//                        }
//                    }
//                }).start();
            } catch (Epos2Exception e) {
                e.printStackTrace();
                int status = e.getErrorStatus();
                //int k = 0;
                //showException(e, "start", mContext);
            }
        } else {
//            isRunning = false;
            new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(mContext.getString(R.string.discover_printer_netwrok_error))
                    .setContentText(mContext.getString(R.string.discover_printer_no_network))
                    .show();
        }
    }

    private DiscoveryListener mDiscoveryListener = new DiscoveryListener() {
        @Override
        public void onDiscovery(final DeviceInfo deviceInfo) {
            ((Activity) mContext).runOnUiThread(new Runnable() {
                @Override
                public synchronized void run() {
                    String name = deviceInfo.getDeviceName();
                    String target = deviceInfo.getMacAddress();
                    connectedPrinter.add(target);
                    isDiscovered = true;
                    calendarStart = new GregorianCalendar();
                }
            });
        }
    };

    public boolean isRunning() {
        return isRunning;
    }

    public int getConnectedPrinter() {
        return iFoundPrinter;
    }

    public void stopThread() throws Epos2Exception {
        Discovery.stop();
//        isRunning = false;
//        isStop = true;
//        synchronized (Shared.connectedPrinters) {
//            Shared.connectedPrinters.clear();
//            for (String target : connectedPrinter) {
//                Shared.connectedPrinters.add(target);
//                SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
//                ConnectedPrinter connectedPrinter = new ConnectedPrinter(target, sqLiteDB.getPrinterName(target), "", "",
//                        myNetwork.getName(), myNetwork.getBandwidth());
//                Shared.connectedPrintersDetails.put(target, connectedPrinter);
//            }
//            iFoundPrinter = Shared.connectedPrinters.size();
//            if(iFoundPrinter == 0) {
//                ((DiscoverPrintersActivity) mContext).runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
//                                .setTitleText(mContext.getString(R.string.discover_printer_netwrok_error))
//                                .setContentText(mContext.getString(R.string.discover_printer_no_printers))
//                                .show();
//                    }
//                });
//
//            }
//        }
    }

    public void stop() {
        try {
            isRunning = false;
            isStop = true;
            Discovery.stop();
        } catch (Epos2Exception e) {
            e.printStackTrace();
        }
    }

    public static void showException(Exception e, String method, Context context) {
        String msg = "";
        if (e instanceof Epos2Exception) {
            msg = String.format(
                    "%s\n\t%s\n%s\n\t%s",
                    "Error",
                    getEposExceptionText(((Epos2Exception) e).getErrorStatus()),
                    "Method",
                    method);
        }
        else {
            msg = e.toString();
        }
        show(msg, context);
    }

    private static String getEposExceptionText(int state) {
        String return_text = "";
        switch (state) {
            case    Epos2Exception.ERR_PARAM:
                return_text = "ERR_PARAM";
                break;
            case    Epos2Exception.ERR_CONNECT:
                return_text = "ERR_CONNECT";
                break;
            case    Epos2Exception.ERR_TIMEOUT:
                return_text = "ERR_TIMEOUT";
                break;
            case    Epos2Exception.ERR_MEMORY:
                return_text = "ERR_MEMORY";
                break;
            case    Epos2Exception.ERR_ILLEGAL:
                return_text = "ERR_ILLEGAL";
                break;
            case    Epos2Exception.ERR_PROCESSING:
                return_text = "ERR_PROCESSING";
                break;
            case    Epos2Exception.ERR_NOT_FOUND:
                return_text = "ERR_NOT_FOUND";
                break;
            case    Epos2Exception.ERR_IN_USE:
                return_text = "ERR_IN_USE";
                break;
            case    Epos2Exception.ERR_TYPE_INVALID:
                return_text = "ERR_TYPE_INVALID";
                break;
            case    Epos2Exception.ERR_DISCONNECT:
                return_text = "ERR_DISCONNECT";
                break;
            case    Epos2Exception.ERR_ALREADY_OPENED:
                return_text = "ERR_ALREADY_OPENED";
                break;
            case    Epos2Exception.ERR_ALREADY_USED:
                return_text = "ERR_ALREADY_USED";
                break;
            case    Epos2Exception.ERR_BOX_COUNT_OVER:
                return_text = "ERR_BOX_COUNT_OVER";
                break;
            case    Epos2Exception.ERR_BOX_CLIENT_OVER:
                return_text = "ERR_BOX_CLIENT_OVER";
                break;
            case    Epos2Exception.ERR_UNSUPPORTED:
                return_text = "ERR_UNSUPPORTED";
                break;
            case    Epos2Exception.ERR_FAILURE:
                return_text = "ERR_FAILURE";
                break;
            default:
                return_text = String.format("%d", state);
                break;
        }
        return return_text;
    }

    public static void showMsg(String msg, Context context) {
        show(msg, context);
    }

    private static void show(String msg, Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                return ;
            }
        });
        alertDialog.create();
        alertDialog.show();
    }
}
