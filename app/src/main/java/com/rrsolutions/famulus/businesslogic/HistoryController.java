package com.rrsolutions.famulus.businesslogic;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.TextView;

import com.rrsolutions.famulus.HistoryActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.adapter.HistoryAdapter;
import com.rrsolutions.famulus.adapter.HistoryDetailAdapter;
import com.rrsolutions.famulus.adapter.SearchAdapter;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.modeljson.HistoryItem;
import com.rrsolutions.famulus.printer.MyPrinter;
import com.rrsolutions.famulus.enumeration.OrderStatus;
import com.rrsolutions.famulus.enumeration.OrderType;
import com.rrsolutions.famulus.printer.PrintCategory;
import com.rrsolutions.famulus.printer.PrintOrder;
import com.rrsolutions.famulus.printer.PrintProduct;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 03/01/2018.
 */

public class HistoryController implements Runnable {

    private GridView gvHistory = null;
    private ExpandableListView elstProducts = null;
    private TextView txtWaiter = null;
    private TextView txtTotalPrice = null;
    private TextView txtPrice = null;
    private Context mContext = null;
    private Button btnBack = null;
    private Button btnForward = null;
    private EditText edtSearch = null;
    private SearchAdapter historyAdapter = null;
    private HistoryDetailAdapter historyDetailAdapter = null;
    private List<HistoryItem> historyItemList = null;
    private List<String> listDataHeader; // header titles
    private List<Boolean> listDataHeaderPrinted; // header titles
    private HashMap<String, List<PrintProduct>> listDataChild;
    private double total = 0.0;
    private String sWaiter = "";
    private Locale locale = null;
    private boolean isRunning = true;

    public HistoryController() {

    }

    public void setListener(final Context context) {
        mContext = context;
        locale = Shared.getLocale(mContext);
        final SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Storage.init(mContext);
        gvHistory = ((HistoryActivity) mContext).getHistory();
        elstProducts = ((HistoryActivity) mContext).getProducts();
        txtTotalPrice = ((HistoryActivity) mContext).getTotalPrice();
        txtPrice = ((HistoryActivity) mContext).getPrice();
        txtWaiter = ((HistoryActivity) mContext).getWaiter();
        btnBack = ((HistoryActivity) mContext).getBack();
        btnForward = ((HistoryActivity) mContext).getForward();
        edtSearch = ((HistoryActivity) mContext).getSearch();

        btnForward.setBackgroundResource(R.drawable.printer);
        btnBack.setBackgroundResource(R.drawable.delete_forever);

        btnBack.setOnClickListener(btnBack_onClick);
        btnForward.setOnClickListener(btnForward_onClick);
        edtSearch.setOnTouchListener(edtSearch_onTouch);

        sWaiter = sqLiteDB.getWaiterName();
        txtWaiter.setText(sWaiter);

        edtSearch.setInputType(InputType.TYPE_CLASS_NUMBER);
        edtSearch.setFilters(new InputFilter[] {new InputFilter.LengthFilter(Shared.iMaxTablesDigits)});

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                historyItemList = sqLiteDB.getOrders(sWaiter, Storage.get(Shared.sUserTypeKey, 0), "");
                calculateOrderTotal();
                historyAdapter = new HistoryAdapter(historyItemList, mContext).registerFilter(HistoryItem.class, "searchtable");
                gvHistory.setAdapter(historyAdapter);
            }
        }, 300);

        gvHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView txtOrderId = view.findViewById(R.id.txtOrderId);
                long orderid = Long.parseLong(txtOrderId.getText().toString());
                HistoryItem historyItem = getHistoryItem(orderid);
                gvHistory.setVisibility(View.GONE);
                elstProducts.setVisibility(View.VISIBLE);
                btnBack.setVisibility(View.GONE);
                btnForward.setVisibility(View.GONE);
                ((HistoryActivity) mContext).setOrderGridVisible(false);
                listDataHeader = new ArrayList<>();
                listDataHeaderPrinted = new ArrayList<>();
                listDataChild = new HashMap<>();
                for(PrintCategory printCategory : historyItem.getCategoryList()) {
                    if(!listDataHeader.contains(printCategory.getCatname())) {
                        listDataHeader.add(printCategory.getCatname());
                        listDataHeaderPrinted.add(sqLiteDB.isCategoryPrinted(orderid, printCategory.getCatid()));
                    }
                    listDataChild.put(printCategory.getCatname(), printCategory.getListProduct());
                }
                if(listDataChild.size() > 0) {
                    historyDetailAdapter = new HistoryDetailAdapter(mContext, listDataHeader, listDataChild, listDataHeaderPrinted);
                    txtTotalPrice.setVisibility(View.GONE);
                    txtPrice.setVisibility(View.VISIBLE);
                    historyDetailAdapter.setPrice(txtPrice);
                    elstProducts.setAdapter(historyDetailAdapter);
                    for(int i = 0; i < listDataHeader.size(); i++) elstProducts.expandGroup(i, true);
                }
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                historyAdapter.filter(s.toString());
                if(s.length() > 0) {
                    calculateOrderTotal(s.toString());
                } else {
                    calculateOrderTotal();
                }
            }
        });

    }

    private HistoryItem getHistoryItem(long orderid) {
        for(HistoryItem historyItem : historyItemList) {
            if(historyItem.getOrderid() == orderid) {
                return historyItem;
            }
        }
        return null;
    }

    private View.OnClickListener btnBack_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(historyAdapter.getCount() > 0) {
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(mContext.getString(R.string.delete_confirmation))
                        .setContentText(mContext.getString(R.string.order_history_delete_history))
                        .setConfirmText(mContext.getString(R.string.yes))
                        .setCancelText(mContext.getString(R.string.cancel))
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                showPINDialog(mContext, mContext.getString(R.string.app_name));
                                sDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        }
    };

    private View.OnClickListener btnForward_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            btnForward.setVisibility(View.INVISIBLE);
            List<Long> listOrderIds = new ArrayList<>();
            boolean showall = ((HistoryActivity) mContext).isShowAll();
            if(showall) {
                for(int i = 0; i < gvHistory.getCount(); i++) {
                    View view = gvHistory.getChildAt(i);
                    CheckBox checkBox = view.findViewById(R.id.chkSelect);
                    if(checkBox.isChecked()) {
                        TextView txt = view.findViewById(R.id.txtOrderId);
                        long orderid = Long.parseLong(txt.getText().toString());
                        listOrderIds.add(orderid);
                    }
                }
            }
            SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
            String[] macs = sqLiteDB.getPrinterMacOther(-1);
            String table = "";
            if(!showall) {
                if(edtSearch.getVisibility() == View.VISIBLE) {
                    if(edtSearch.getText().toString().length() > 0) {
                        if(historyAdapter.getCount() > 0) {
                            table = edtSearch.getText().toString();
                        }
                    }
                }
            }

            for(String mac : macs) {
                if(Shared.isPrinterAvailable(mac)) {
                    MyPrinter myPrinter = new MyPrinter(mContext, mac);
                    PrintOrder printOrder = new PrintOrder(mContext);
                    printOrder.setPrintType(OrderType.HISTORY.ordinal());
                    if(!showall) printOrder.setTableid(table);
                    else printOrder.setOrderIdList(listOrderIds);
                    myPrinter.addinQueue(printOrder);
                    new Thread(myPrinter).start();
                }
            }

            btnForward.postDelayed(new Runnable() {
                @Override
                public void run() {
                    btnForward.setVisibility(View.VISIBLE);
                }
            }, 10000);

        }
    };

    private View.OnTouchListener edtSearch_onTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (edtSearch.getRight() - edtSearch.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    edtSearch.setVisibility(View.GONE);
                    edtSearch.setText("");
                    return true;
                }
            }
            return false;
        }
    };

    public void setTotal() {
        txtTotalPrice.setText(String.format(locale, "%.2f", total) + "€");
    }

    private void calculateOrderTotal() {
        total = 0;
        if(historyItemList.size() > 0) {
            for(HistoryItem historyItem : historyItemList) {
                List<PrintCategory> list = historyItem.getCategoryList();
                if(list.size() > 0) {
                    for(PrintCategory printCategory : list) {
                        List<PrintProduct> printProductList = printCategory.getListProduct();
                        if(printProductList.size() > 0) {
                            for(PrintProduct printProduct : printProductList) {
                                total += printProduct.getQuantity() * printProduct.getPrice();
                            }
                        }
                    }
                }
            }
            txtTotalPrice.setText(String.format(locale, "%.2f", total) + "€");
        }
    }

    private void calculateOrderTotal(String table) {
        total = 0;
        if(historyItemList.size() > 0) {
            for(HistoryItem historyItem : historyItemList) {
                if(table.equalsIgnoreCase(historyItem.getTable())) {
                    List<PrintCategory> list = historyItem.getCategoryList();
                    if(list.size() > 0) {
                        for(PrintCategory printCategory : list) {
                            List<PrintProduct> printProductList = printCategory.getListProduct();
                            if(printProductList.size() > 0) {
                                for(PrintProduct printProduct : printProductList) {
                                    total += printProduct.getQuantity() * printProduct.getPrice();
                                }
                            }
                        }
                    }
                }
            }
            txtTotalPrice.setText(String.format(locale, "%.2f", total) + "€");
        }
    }

    public static void showPINDialog(final Context context, String title) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_quantity, null);
        final TextView txtPIN = grid.findViewById(R.id.txtQuantity);
        final EditText edtPIN = grid.findViewById(R.id.edtQuantity);
        final GridView gridView = ((HistoryActivity) context).getHistory();
        final ExpandableListView expandableListView = ((HistoryActivity) context).getProducts();

        edtPIN.setHint("");
        txtPIN.setText(context.getString(R.string.pin_level1_dialog_enter));
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText(title)
                .setConfirmText(context.getString(R.string.ok))
                .setCancelText(context.getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        if(edtPIN.getText().toString().trim().length() > 0) {
                            SQLiteDB sqLiteDB = SQLiteDB.getInstance(context);
                            sqLiteDB.deleteOrders(sqLiteDB.getWaiterName());
                            gridView.setAdapter(null);
                            expandableListView.setAdapter((BaseExpandableListAdapter) null);
                            expandableListView.setVisibility(View.GONE);
                            gridView.setVisibility(View.VISIBLE);
                            sweetAlertDialog.dismissWithAnimation();
                        } else {
                            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Enter PIN")
                                    .show();
                        }
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();

    }

    public void showCheckBox() {
        historyAdapter.notifyDataSetChanged();
    }

    public void stop() {
        isRunning = false;
    }

    @Override
    public void run() {
        final SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Storage.init(mContext);
        while (isRunning) {
            try {
                boolean isOrderChanged = false;
                final List<HistoryItem> historyItems = sqLiteDB.getOrders(sqLiteDB.getWaiterName(), Storage.get(Shared.sUserTypeKey, 0), "");
                if(historyItems.size() > historyItemList.size()) {
                    historyItemList = historyItems;
                    ((HistoryActivity)  mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            calculateOrderTotal();
                            historyAdapter.notifyDataSetChanged();
                            if(elstProducts.getVisibility() == View.VISIBLE) {
                                elstProducts.deferNotifyDataSetChanged();
                            }
                        }
                    });
                } else {
                    for(int i = 0; i < historyItems.size(); i++) {
                        HistoryItem historyItemsDB = historyItems.get(i);
                        HistoryItem historyItemGrid = historyItemList.get(i);
                        if(historyItemsDB.getOrderid() == historyItemGrid.getOrderid()) {
                            List<PrintCategory> listDB = historyItemsDB.getCategoryList();
                            List<PrintCategory> listGrid = historyItemGrid.getCategoryList();
                            if(historyItemGrid.getStatus() != OrderStatus.PRINTED.ordinal()) {
                                int totalPrintedCats = 0;
                                for(int j = 0; j < listDB.size(); j++) {
                                    PrintCategory printCategoryDB = listDB.get(j);
                                    PrintCategory printCategoryGrid = listGrid.get(j);
                                    if(printCategoryDB.getCatid() == printCategoryGrid.getCatid()) {
                                        if (printCategoryGrid.getStatus() != OrderStatus.PRINTED.ordinal()) {
                                            if (printCategoryDB.getStatus() == OrderStatus.PRINTED.ordinal()) {
                                                printCategoryGrid.setStatus(OrderStatus.PRINTED.ordinal());
                                                totalPrintedCats++;
                                            }
                                        } else {
                                            totalPrintedCats++;
                                        }
                                    }
                                }
                                if(totalPrintedCats == listGrid.size()) {
                                    sqLiteDB.updateOrder(historyItemsDB.getOrderid());
                                    historyItemGrid.setStatus(OrderStatus.PRINTED.ordinal());
                                    isOrderChanged = true;
                                }
                            }
                        }
                    }
                    if(isOrderChanged) {
                        ((HistoryActivity)  mContext).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                calculateOrderTotal();
                                historyAdapter.notifyDataSetChanged();
                                if(elstProducts.getVisibility() == View.VISIBLE) {
                                    elstProducts.deferNotifyDataSetChanged();
                                }
                            }
                        });
                    }
                }
                Thread.sleep(3000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
