package com.rrsolutions.famulus.businesslogic;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.OrderActivity;
import com.rrsolutions.famulus.ProductsActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.adapter.ProductAdapter;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.modeljson.Product;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 11/12/2017.
 */

public class ProductController {

    private Context mContext = null;

    private int iCateId = 0;
    private String sCatName = "";

    private GridView gvProducts = null;
    private Button btnBack = null;
    private Button btnForward = null;

    private List<Product> products = null;

    // catid | catname | productid | productname | extrainfo | price | quantity

    public ProductController() {

    }

    public void setListeners(Context context) {
        mContext = context;
        gvProducts = ((ProductsActivity) mContext).getProductGrid();
        btnBack = ((ProductsActivity) mContext).getBack();
        btnForward = ((ProductsActivity) mContext).getForward();
        products = ((ProductsActivity) mContext).getProducts();
        iCateId = ((ProductsActivity) mContext).getCatId();
        sCatName = ((ProductsActivity) mContext).getCatName();

        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        products = sqLiteDB.selectAllProducts(iCateId);
        restoreLastSession();
        if(products.size() > 0) {
            ProductAdapter productAdapter = new ProductAdapter(mContext, products);
            gvProducts.setAdapter(productAdapter);
        }
        btnBack.setOnClickListener(btnBack_onClick);
        btnForward.setOnClickListener(btnForward_onClick);
    }

    public void saveSession() {
        String product_data = "";
        Storage.init(mContext);
        Product product = null;
        for (int i = 0; i < products.size(); i++) {
            product = products.get(i);
            int quantity = product.getQuantity();
            if (quantity > 0) {
                Set<String> proopt = new HashSet<>();
                proopt = Storage.get("pro" + product.getId(), proopt);
                String descopt = "";
                if (proopt.size() > 0) {
                    for (String string : proopt) {
                        String[] data = string.substring(0, string.lastIndexOf("|")).split("\\|");
                        if (data.length > 7) {
                            descopt += "|" + data[7];
                        }
                        if (data.length > 8) {
                            descopt += "|" + data[8];
                        }
                    }
                }
                product_data = iCateId + "|" + sCatName + "|" + product.getId() + "|" + product.getName() + "|"
                        + product.getExtra_info() + "|" + product.getPrice() + "|" + quantity + descopt;
                if (proopt.size() > 0) {
                    proopt.clear();
                }
                proopt.add(product_data + "|1");
                Storage.save(product.getId(), proopt, 1);
            }
        }
    }

    private void restoreLastSession() {
        try {
            Storage.init(mContext);
            int quantity = 0;
            for (Product product : products) {
                quantity = 0;
                Set<String> proopt = new HashSet<>();
                proopt = Storage.get("pro" + product.getId(), proopt);
                if (proopt.size() > 0) {
                    for (String po : proopt) {
                        String[] data = po.split("\\|");
                        if (data.length > 0) {
                            quantity += Integer.parseInt(data[6].trim());
                        }
                    }
                    product.setQuantity(quantity);
                    product.setSelected(proopt.size() > 0);
                    if(proopt.size() > 1) product.setShowQuantity(false);
                    else product.setShowQuantity(true);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private View.OnClickListener btnBack_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((ProductsActivity) mContext).setActivityChanged(true);
            saveSession();
            Intent intent = new Intent(mContext, CategoryActivity.class);
            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener btnForward_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            saveSession();
            if(Shared.hasProduct(mContext)) {
                ((ProductsActivity) mContext).setActivityChanged(true);
                Intent intent = new Intent(mContext, OrderActivity.class);
                mContext.startActivity(intent);
            } else {
                SweetAlertDialog alertDialog = new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE);
                alertDialog.setTitleText(mContext.getString(R.string.product_err_no_product));
                alertDialog.show();
            }

        }
    };

}
