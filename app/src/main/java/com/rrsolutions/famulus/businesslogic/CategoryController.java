package com.rrsolutions.famulus.businesslogic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.DiscoverPrintersActivity;
import com.rrsolutions.famulus.OrderActivity;
import com.rrsolutions.famulus.ProductsActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.TableActivity;
import com.rrsolutions.famulus.adapter.CategoryAdapter;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.modeljson.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by hassanmushtaq on 07/12/2017.
 */

public class CategoryController {

    private Context mContext = null;
    private TextView txtWaiter = null;
    private GridView gvCategory = null;
    private Button btnBack = null;
    private Button btnForward = null;
    private View rlBottomBar = null;

    private String sWaiter = "";

    private List<Category> categories = new ArrayList<>();

    public CategoryController() {

    }

    public void setListeners(Context context) {
        mContext = context;
        txtWaiter = ((CategoryActivity) mContext).getWaiter();
        gvCategory = ((CategoryActivity) mContext).getCategory();
        btnBack = ((CategoryActivity) mContext).getBack();
        btnForward = ((CategoryActivity) mContext).getForward();
        rlBottomBar = ((CategoryActivity) mContext).getBottomBar();
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        categories = sqLiteDB.selectAllCategories();
        sWaiter = sqLiteDB.getWaiterName();
        btnBack.setOnClickListener(btnBack_onClick);
        btnForward.setOnClickListener(btnForward_onClick);
        restoreLastSession();
        CategoryAdapter categoryAdapter = new CategoryAdapter(mContext, categories);
        gvCategory.setAdapter(categoryAdapter);
        txtWaiter.setText(sWaiter);
        gvCategory.setOnItemClickListener(gvCategory_onItemClickListener);
        if(Shared.hasProduct(mContext)) {
            rlBottomBar.setVisibility(View.VISIBLE);
        } else {
            rlBottomBar.setVisibility(View.GONE);
        }
    }

    private AdapterView.OnItemClickListener gvCategory_onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(mContext, ProductsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("catid", categories.get(position).getId());
            bundle.putString("catname", categories.get(position).getName());
            intent.putExtras(bundle);
            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener btnBack_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, TableActivity.class);
            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener btnForward_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, OrderActivity.class);
            mContext.startActivity(intent);
        }
    };

    private void restoreLastSession() {
        try {
            ArrayList<Integer> temp = new ArrayList<>();
            Storage.init(mContext);
            Map<String, Set<String>> savedProducts = Storage.getAllProducts();
            if (savedProducts.size() > 0) {
                Collection<String> keys = savedProducts.keySet();
                for (String key : keys) {
                    Set<String> set = savedProducts.get(key);
                    for(String s : set) {
                        String[] data = s.split("\\|");
                        int id = Integer.parseInt(data[0].trim());
                        if (!temp.contains(id)) {
                            setCategorySelected(id);
                            temp.add(id);
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setCategorySelected(int id) {
        for(Category category : categories) {
            if(category.getId() == id) {
                category.setSelected(true);
                break;
            }
        }
    }

}
