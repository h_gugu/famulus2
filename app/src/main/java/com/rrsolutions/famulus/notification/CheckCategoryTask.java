package com.rrsolutions.famulus.notification;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.rrsolutions.famulus.DiscoverPrintersActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.modeljson.Category;

import java.util.List;

/**
 * Created by hassanmushtaq on 31/12/2017.
 */

public class CheckCategoryTask extends AsyncTask<String, Void, Boolean> {

    private Context mContext;
    private String message = "";
    private int count = 0;

    public CheckCategoryTask(Context context) {
        mContext = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        List<Category> categories = sqLiteDB.selectAllCategories();

        String cats = "";
        for (Category category : categories) {
            if (!sqLiteDB.isCategoryAssigned(category.getId())) {
                cats += ", " + category.getName();
                count++;
            }
        }
        if(cats.length() > 1) {
            if (count == 1) {
                message = mContext.getString(R.string.print_category_error).replace("[1]", cats.substring(1).trim());
            } else {
                message = mContext.getString(R.string.print_categories_error).replace("[1]", cats.substring(1).trim());
            }
        }

        return true;
    }

    @Override
    protected void onPostExecute(Boolean aVoid) {
        if (count > 0) {
            Shared.showSnackBar((Activity) mContext, message, 10000, mContext.getString(R.string.dismiss), DiscoverPrintersActivity.class);
        }
    }

}