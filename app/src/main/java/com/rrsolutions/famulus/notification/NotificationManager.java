package com.rrsolutions.famulus.notification;

import android.content.Context;

/**
 * Created by hassanmushtaq on 31/12/2017.
 */

public class NotificationManager {

    private Context mContext = null;

    public NotificationManager(Context context) {
        mContext = context;
    }

    public void checkCategoriesPrinters() {
        CheckCategoryTask snackBarTask = new CheckCategoryTask(mContext);
        snackBarTask.execute("");
    }
}
