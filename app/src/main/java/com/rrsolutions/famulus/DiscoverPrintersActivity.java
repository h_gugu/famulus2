package com.rrsolutions.famulus;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.rrsolutions.famulus.businesslogic.DiscoverPrinterController;
import com.rrsolutions.famulus.businesslogic.MainMenu;

public class DiscoverPrintersActivity extends AppCompatActivity {

    private ExpandableListView expListView;
    private Context mContext = null;
    private Button btnBack = null;
    private Button btnForward = null;

    private DiscoverPrinterController discoverPrinterController = null;
    private MainMenu mainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_printer);
        mContext = this;
        expListView = findViewById(R.id.elstPrinters);
        btnBack = findViewById(R.id.btnBack);
        btnForward = findViewById(R.id.btnForward);

        btnBack.setBackgroundResource(R.drawable.printer);
        btnForward.setBackgroundResource(R.drawable.check);

        updateActionBar(getString(R.string.discover_printer_title), getString(R.string.discover_printer_online).replace("[1]", "0"));
        mainMenu = new MainMenu(mContext);

        discoverPrinterController = new DiscoverPrinterController();
        discoverPrinterController.setListener(mContext);
    }

    public void updateActionBar(String title, String subtitle) {
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setSubtitle(subtitle);
        }
    }

    public ExpandableListView getListView() {
        return expListView;
    }

    public Button getBack() {
        return btnBack;
    }

    public Button getForward() {
        return btnForward;
    }

    public void refresh() {
        discoverPrinterController.refresh();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            discoverPrinterController.stop();
        } catch (Exception ex) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(true);
        menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_printing_mode).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        try {
            discoverPrinterController.stop();
        } catch (Exception ex) {

        }
        super.onBackPressed();
    }
}
