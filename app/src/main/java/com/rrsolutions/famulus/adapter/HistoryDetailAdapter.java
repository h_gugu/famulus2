package com.rrsolutions.famulus.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.printer.PrintProduct;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by hassanmushtaq on 11/12/2017.
 */

public class HistoryDetailAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    private List<Boolean> _listDataHeaderPrinted; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<PrintProduct>> _listDataChild;
    private TextView txtPrice = null;
    private double total = 0.0;
    private Locale locale = null;

    public HistoryDetailAdapter(Context context,
                                List<String> listDataHeader, HashMap<String,
                                List<PrintProduct>> listChildData, List<Boolean> listDataHeaderPrinted) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._listDataHeaderPrinted = listDataHeaderPrinted;
        locale = Shared.getLocale(context);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText =  ((PrintProduct) getChild(groupPosition, childPosition)).getName();
        final String childExtraText =  ((PrintProduct) getChild(groupPosition, childPosition)).getExtrainfo();
        final int quantity = ((PrintProduct) getChild(groupPosition, childPosition)).getQuantity();
        final double price = ((PrintProduct) getChild(groupPosition, childPosition)).getPrice() * quantity;
        final PrintProduct printProduct = (PrintProduct) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.history_product_item, null);
        }
        final TextView txtProduct = convertView.findViewById(R.id.txtProduct);
        TextView txtProductExtraInfo = convertView.findViewById(R.id.txtProductExtraInfo);
        TextView txtQuantity = convertView.findViewById(R.id.txtQuantity);
        TextView txtPrice = convertView.findViewById(R.id.txtPrice);
        TextView txtOptions = convertView.findViewById(R.id.txtOptions);

        txtProduct.setText(childText);
        txtProductExtraInfo.setText(childExtraText);
        txtQuantity.setText(String.valueOf(quantity));
        txtPrice.setText(String.format(locale, "%.2f", price) + "€");

        if(!_listDataHeaderPrinted.get(groupPosition)) {
            convertView.setBackgroundColor(ContextCompat.getColor(_context, R.color.category_pending));
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.order_header_item, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        ImageView imgIndicator = convertView.findViewById(R.id.imgIndicator);

        lblListHeader.setText(headerTitle);

        if(!_listDataHeaderPrinted.get(groupPosition)) {
            convertView.setBackgroundColor(ContextCompat.getColor(_context, R.color.category_pending));
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        if(isExpanded) {
            imgIndicator.setBackgroundResource(R.drawable.arrow_up);
        } else {
            imgIndicator.setBackgroundResource(R.drawable.arrow_down);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private void calculateTotal() {
        if(_listDataChild.size() > 0) {
            for(String category : _listDataChild.keySet()) {
                List<PrintProduct> printProductList = _listDataChild.get(category);
                if(printProductList.size() > 0) {
                    for(PrintProduct printProduct : printProductList) {
                        total += printProduct.getQuantity() * printProduct.getPrice();
                    }
                }
            }
        }
    }

    public void setPrice(TextView txtPrice) {
        this.txtPrice = txtPrice;
        calculateTotal();
        this.txtPrice.setText(String.format(locale, "%.2f", total) + "€");
    }
}
