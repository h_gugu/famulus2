package com.rrsolutions.famulus.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.modeljson.Category;

import java.util.List;

/**
 * Created by hassanmushtaq on 12/12/2017.
 */

public class CategoryAdapter extends BaseAdapter {

    private Context mContext = null;
    private List<Category> categories = null;

    public CategoryAdapter(Context context, List<Category> c) {
        mContext = context;
        categories = c;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        if(categories.size() > 0) return categories.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        if(categories.size() > 0) return categories.get(position).getId();
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid = null;
        if (convertView == null) {
            grid = new View(mContext);
            LayoutInflater inflater = ((CategoryActivity) mContext).getLayoutInflater();
            grid = inflater.inflate(R.layout.category_item, parent, false);
        } else {
            grid = (View) convertView;
        }
        RelativeLayout rlElement = grid.findViewById(R.id.rlElement);
        TextView txtCategory = grid.findViewById(R.id.txtCategory);
        txtCategory.setText(categories.get(position).getName());
        txtCategory.setTag(categories.get(position).getId());

        if(categories.get(position).isSelected()) {
            rlElement.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected));
        } else {
            rlElement.setBackgroundColor(Color.TRANSPARENT);
        }

        return grid;
    }
}
