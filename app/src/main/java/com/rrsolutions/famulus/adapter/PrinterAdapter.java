package com.rrsolutions.famulus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.modeljson.Category;
import com.rrsolutions.famulus.modeljson.Printer;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hassanmushtaq on 16/12/2017.
 */

public class PrinterAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<Category>> _listDataChild;
    private HashMap<String, List<Boolean>> _listDataChecked;
    private HashMap<String, Boolean> _listDataChildStatus;
    private HashMap<String, Printer> _listDataPrinter;

    private HashMap<String, boolean[]> mChildCheckStates;
    private ExpandableListView expandableListView = null;
    private ChildViewHolder childViewHolder;
    private GroupViewHolder groupViewHolder;

    private String groupText;
    private String childText;

    public PrinterAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<Category>> listChildData,
                          HashMap<String, Boolean> listDataChildStatus,
                          HashMap<String, boolean[]> mChildCheckStates) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this._listDataChildStatus = listDataChildStatus;
        this.mChildCheckStates = mChildCheckStates;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final int mGroupPosition = groupPosition;
        final int mChildPosition = childPosition;
        final String group = _listDataHeader.get(mGroupPosition);
        childText = ((Category) getChild(groupPosition, childPosition)).getName();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.printer_element_item, null);
            childViewHolder = new ChildViewHolder();
            childViewHolder.mChildText = convertView.findViewById(R.id.lblListItem);
            childViewHolder.mSwitch = convertView.findViewById(R.id.swCategory);
            convertView.setTag(R.layout.printer_element_item, childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag(R.layout.printer_element_item);
        }
        childViewHolder.mChildText.setText(childText);
        childViewHolder.mSwitch.setOnCheckedChangeListener(null);

        if (getmChildCheckStates().containsKey(group)) {
            boolean getChecked[] = getmChildCheckStates().get(group);
            childViewHolder.mSwitch.setChecked(getChecked[mChildPosition]);
        } else {
            boolean getChecked[] = new boolean[getChildrenCount(mGroupPosition)];
            getmChildCheckStates().put(group, getChecked);
            childViewHolder.mSwitch.setChecked(false);
        }

        childViewHolder.mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    boolean getChecked[] = getmChildCheckStates().get(group);
                    getChecked[mChildPosition] = isChecked;
                    getmChildCheckStates().put(group, getChecked);
                } else {
                    boolean getChecked[] = getmChildCheckStates().get(group);
                    getChecked[mChildPosition] = isChecked;
                    getmChildCheckStates().put(group, getChecked);
                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        //View grid = null;
        groupText = (String) getGroup(groupPosition);
        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.printer_header_item, null);
            groupViewHolder = new GroupViewHolder();

            groupViewHolder.mGroupText = convertView.findViewById(R.id.lblListHeader);
            groupViewHolder.icon = convertView.findViewById(R.id.imgIndicator);

            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }
        groupViewHolder.mGroupText.setText(groupText);
        if(_listDataChildStatus.get(groupText)) {
            groupViewHolder.mGroupText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.printer_online, 0);

        } else {
            groupViewHolder.mGroupText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.printer_offline, 0);
        }
        if(expandableListView.isGroupExpanded(groupPosition)) {
            groupViewHolder.icon.setBackgroundResource(R.drawable.arrow_up);
        } else {
            groupViewHolder.icon.setBackgroundResource(R.drawable.arrow_down);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void setExpandableListView(ExpandableListView expandableListView) {
        this.expandableListView = expandableListView;
    }

    public HashMap<String, boolean[]> getmChildCheckStates() {
        return mChildCheckStates;
    }

    public final class GroupViewHolder {
        TextView mGroupText;
        ImageView icon;
    }

    public final class ChildViewHolder {
        TextView mChildText;
        Switch mSwitch;
    }

}

