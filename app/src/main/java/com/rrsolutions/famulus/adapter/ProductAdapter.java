package com.rrsolutions.famulus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.MultiProductsActivity;
import com.rrsolutions.famulus.ProductsActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.datawrapper.ProductData;
import com.rrsolutions.famulus.keyboard.UIUtil;
import com.rrsolutions.famulus.modeljson.Product;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 11/12/2017.
 */

public class ProductAdapter extends BaseAdapter {

    private List<Product> products = null;
    private Context mContext = null;

    public ProductAdapter(Context context, List<Product> p) {
        mContext = context;
        products = p;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return products.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid = null;
        if (convertView == null) {
            grid = new View(mContext);
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            grid = inflater.inflate(R.layout.product_item, parent, false);
        } else {
            grid = (View) convertView;
        }

        TextView txtProduct = grid.findViewById(R.id.txtProduct);
        TextView txtProductExtraInfo = grid.findViewById(R.id.txtProductExtraInfo);
        TextView txtQuantity = grid.findViewById(R.id.txtQuantity);
        ElegantNumberButton ebtnQuantity = grid.findViewById(R.id.ebtnQuantity);
        RelativeLayout rlElement = grid.findViewById(R.id.rlElement);
        LinearLayout llText = grid.findViewById(R.id.llText);
        LinearLayout llQuantity = grid.findViewById(R.id.llQuantity);
        TextView textView = ebtnQuantity.findViewById(com.cepheuen.elegantnumberbutton.R.id.number_counter);

        Shared.changeButton(mContext, ebtnQuantity);

        txtProduct.setText(products.get(position).getName());
        txtProductExtraInfo.setText(products.get(position).getExtra_info());

        llText.setTag(products.get(position));
        ebtnQuantity.setTag(position);
        textView.setTag(ebtnQuantity);

        if(products.get(position).isSelected()) {
            rlElement.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected));
        } else {
            rlElement.setBackgroundColor(Color.TRANSPARENT);
        }

        if(products.get(position).getQuantity() > 0) {
            ebtnQuantity.setNumber(String.valueOf(products.get(position).getQuantity()));
            txtQuantity.setText(String.valueOf(products.get(position).getQuantity()));
        } else {
            ebtnQuantity.setNumber("0");
            txtQuantity.setText("0");
        }

        if(products.get(position).isShowQuantity()) {
            ebtnQuantity.setVisibility(View.VISIBLE);
            llQuantity.setVisibility(View.GONE);
        } else {
            ebtnQuantity.setVisibility(View.GONE);
            llQuantity.setVisibility(View.VISIBLE);
        }

        llText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = (Product) v.getTag();
                ProductData productData = new ProductData(
                        product.getId(), product.getCat_id(), ((ProductsActivity) mContext).getCatName(),
                        product.getName(), product.getExtra_info(),
                        product.getPrice());
                Bundle bundle = new Bundle();
                Intent intent = new Intent(mContext, MultiProductsActivity.class);
                intent.putExtra("product", productData);
                bundle.putInt("catid", product.getCat_id());
                bundle.putInt("proid", product.getId());
                bundle.putString("proname", product.getName());
                bundle.putString("catname", ((ProductsActivity) mContext).getCatName());
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        });

        ebtnQuantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Product product = products.get(Integer.parseInt(view.getTag().toString()));
                product.setQuantity(newValue);
                if(newValue > 0) {
                    product.setSelected(true);
                } else {
                    product.setSelected(false);
                    Storage.init(mContext);
                    Storage.remove( "pro" + product.getId());
                }
                notifyDataSetChanged();
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Product product = products.get(Integer.parseInt(((ElegantNumberButton) v.getTag()).getTag().toString()));
                int quantity = Integer.parseInt (((ElegantNumberButton) v.getTag()).getNumber());
                showQuantityDialog(mContext, ((ElegantNumberButton) v.getTag()), product.getName(), quantity, product);
            }
        });

        return grid;
    }


    private void showQuantityDialog(Context context, final ElegantNumberButton button, String title,
                                          int quantity, final Product product) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_quantity, null);
        final EditText edtQuantity = grid.findViewById(R.id.edtQuantity);

        if(quantity > 0) {
            edtQuantity.setText(String.valueOf(quantity));
            edtQuantity.post(new Runnable() {
                @Override
                public void run() {
                    edtQuantity.setSelection(edtQuantity.getText().length());
                }
            });
        }
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText(title)
                .setConfirmText(context.getString(R.string.ok))
                .setCancelText(context.getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        if(edtQuantity.getText().toString().trim().length() > 0) {
                            int q = Integer.parseInt(edtQuantity.getText().toString());
                            button.setNumber(String.valueOf(q));
                            product.setQuantity(q);
                            product.setSelected(q > 0);
                        } else {
                            button.setNumber("0");
                            product.setQuantity(0);
                            product.setSelected(false);
                        }
                        notifyDataSetChanged();
                        sweetAlertDialog.cancel();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
        edtQuantity.requestFocus();
        edtQuantity.post(new Runnable() {
            @Override
            public void run() {
                UIUtil.showKeyboard(mContext, edtQuantity);
            }
        });

        edtQuantity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_GO
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || (event.getAction() == KeyEvent.KEYCODE_ENTER)) {
                    if(edtQuantity.getText().toString().trim().length() > 0) {
                        int q = Integer.parseInt(edtQuantity.getText().toString());
                        button.setNumber(String.valueOf(q));
                        product.setQuantity(q);
                        product.setSelected(q > 0);
                    } else {
                        button.setNumber("0");
                        product.setQuantity(0);
                        product.setSelected(false);
                    }
                    notifyDataSetChanged();
                    sweetAlertDialog.cancel();
                    return true;
                }
                return false;
            }
        });


    }

}
