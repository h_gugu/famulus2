package com.rrsolutions.famulus.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.MultiProductsActivity;
import com.rrsolutions.famulus.OrderActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.datawrapper.ProductData;

import java.util.HashMap;
import java.util.List;

/**
 * Created by hassanmushtaq on 11/12/2017.
 */

public class PrintOrderAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<OrderProduct>> _listDataChild;
    private double totalAmount = 0.0;
    private double totalAmount2 = 0.0;
    private TextView txtRest = null;
    private TextView txtSplit = null;

    public PrintOrderAdapter(Context context, List<String> listDataHeader, HashMap<String, List<OrderProduct>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText =  ((OrderProduct) getChild(groupPosition, childPosition)).getName();
        final String childExtraText =  ((OrderProduct) getChild(groupPosition, childPosition)).getExtra_info();
        final int quantity = ((OrderProduct) getChild(groupPosition, childPosition)).getQuantity();
        final OrderProduct orderProduct = (OrderProduct) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.print_order_item, null);
        }
        final View _convertView = convertView;
        final TextView txtProduct = convertView.findViewById(R.id.txtProduct);
        TextView txtProductExtraInfo = convertView.findViewById(R.id.txtProductExtraInfo);
        TextView txtQuantity = convertView.findViewById(R.id.txtQuantity);
        ElegantNumberButton ebtnQuantity = convertView.findViewById(R.id.ebtnQuantity);
        ebtnQuantity.setTag(orderProduct);

        Shared.changeButton(_context, ebtnQuantity);

        ebtnQuantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                TextView txtQuan = _convertView.findViewById(R.id.txtQuantity);
                String text = txtQuan.getText().toString();
                txtQuan.setText(String.valueOf(Integer.parseInt(text) - (newValue - oldValue)));
                OrderProduct orderProduct = (OrderProduct) view.getTag();
                orderProduct.setQuantity(Integer.parseInt(txtQuan.getText().toString()));
                totalAmount2 = totalAmount2 - ((newValue - oldValue) * orderProduct.getPrice());
                txtRest.setText(_context.getString(R.string.print_order_rest) + " " + String.format("%.2f", totalAmount2));
                txtSplit.setText(_context.getString(R.string.print_order_split) + " " + String.format("%.2f", totalAmount - totalAmount2));
            }
        });

        txtProduct.setText(childText);
        txtProductExtraInfo.setText(childExtraText);
        ebtnQuantity.setNumber("0");
        ebtnQuantity.setRange(0, quantity);
        txtQuantity.setText(String.valueOf(quantity));

        ebtnQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                OrderProduct orderProduct = (OrderProduct) v.getTag();
//                int quantity = Integer.parseInt (((ElegantNumberButton) v).getNumber());
//                Shared.showQuantityDialog(_context, ((ElegantNumberButton) v), orderProduct.getName(), quantity, null, null);
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.order_header_item, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        ImageView imgIndicator = convertView.findViewById(R.id.imgIndicator);

        lblListHeader.setText(headerTitle);

        if(isExpanded) {
            imgIndicator.setBackgroundResource(R.drawable.arrow_up);
        } else {
            imgIndicator.setBackgroundResource(R.drawable.arrow_down);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
        this.totalAmount2 = totalAmount;
    }

    public void setRest(TextView txtRest) {
        this.txtRest = txtRest;
    }

    public void setSplit(TextView txtSplit) {
        this.txtSplit = txtSplit;
    }
}
