package com.rrsolutions.famulus.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.rrsolutions.famulus.HistoryActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.modeljson.HistoryItem;
import com.rrsolutions.famulus.enumeration.OrderStatus;
import com.rrsolutions.famulus.printer.PrintCategory;
import com.rrsolutions.famulus.printer.PrintProduct;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hassanmushtaq on 03/01/2018.
 */

public class HistoryAdapter extends SearchAdapter<HistoryItem> {

    private List<HistoryItem> historyItems = null;
    private boolean showAll = false;
    private Locale locale = null;
    SimpleDateFormat sdfHistory = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    class ViewHolder {
        @BindView(R.id.chkSelect) CheckBox chkSelect;
        @BindView(R.id.txtTable) TextView txtTable;
        @BindView(R.id.txtDate) TextView txtDate;
        @BindView(R.id.txtStatus) TextView txtStatus;
        @BindView(R.id.txtOrderId) TextView txtOrderId;
        @BindView(R.id.txtPrice) TextView txtPrice;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public HistoryAdapter(List<HistoryItem> historyItems, Context context) {
        super(historyItems, context);
        this.historyItems = historyItems;
        locale = Shared.getLocale(context);
        sdfHistory = new SimpleDateFormat(Shared.sDateTimeFormat_History, locale);
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null) {
            convertView = layoutInflater.inflate(R.layout.history_order_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)convertView.getTag();
        }
        if(filteredContainer.get(position).getTable().equalsIgnoreCase("")) {
            viewHolder.txtTable.setText(context.getString(R.string.cashier));
        } else {
            viewHolder.txtTable.setText(context.getString(R.string.table_title) + " " + filteredContainer.get(position).getTable());
        }
        try {
            viewHolder.txtDate.setText(sdfHistory.format(Shared.sdfUTCDateTime.parse(filteredContainer.get(position).getDate())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(filteredContainer.get(position).getStatus() == OrderStatus.PRINTED.ordinal()) {
            viewHolder.txtStatus.setText(context.getString(R.string.printed));
        } else {
            viewHolder.txtStatus.setText(context.getString(R.string.not_printed));
        }
        viewHolder.txtOrderId.setText(String.valueOf(filteredContainer.get(position).getOrderid()));
        viewHolder.txtPrice.setText(calculatePrice(getHistoryItem(filteredContainer.get(position).getOrderid())));

        if(((HistoryActivity) context).isShowAll()) {
            viewHolder.chkSelect.setVisibility(View.VISIBLE);
        } else {
            viewHolder.chkSelect.setVisibility(View.GONE);
        }

        return convertView;
    }

    private String calculatePrice(HistoryItem historyItem) {
        double price = 0.0;
        for (PrintCategory printCategory : historyItem.getCategoryList()) {
            for (PrintProduct printProduct : printCategory.getListProduct()) {
                price += printProduct.getPrice() * printProduct.getQuantity();
            }
        }
        return String.format(locale, "%.2f", price) + "€";
    }

    private HistoryItem getHistoryItem(long orderid) {
        for(HistoryItem historyItem : historyItems) {
            if(historyItem.getOrderid() == orderid) {
                return historyItem;
            }
        }
        return null;
    }



}
