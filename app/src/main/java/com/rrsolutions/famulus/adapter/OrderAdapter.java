package com.rrsolutions.famulus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.MultiProductsActivity;
import com.rrsolutions.famulus.OrderActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.datawrapper.ProductData;
import com.rrsolutions.famulus.keyboard.UIUtil;
import com.rrsolutions.famulus.modeljson.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 11/12/2017.
 */

public class OrderAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<OrderProduct>> _listDataChild;
    private TextView txtPrice = null;
    private Locale locale = null;

    public OrderAdapter(Context context, List<String> listDataHeader, HashMap<String, List<OrderProduct>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        locale = Shared.getLocale(this._context);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText =  ((OrderProduct) getChild(groupPosition, childPosition)).getName();
        final String childExtraText =  ((OrderProduct) getChild(groupPosition, childPosition)).getExtra_info();
        final int quantity = ((OrderProduct) getChild(groupPosition, childPosition)).getQuantity();
        final boolean showQuantity = ((OrderProduct) getChild(groupPosition, childPosition)).isShowquantity();
        final int grpPos = groupPosition;
        final int childPos = childPosition;

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.order_item, null);
        }

        TextView txtProduct = convertView.findViewById(R.id.txtProduct);
        TextView txtProductExtraInfo = convertView.findViewById(R.id.txtProductExtraInfo);
        TextView txtQuantity = convertView.findViewById(R.id.txtQuantity);
        ElegantNumberButton ebtnQuantity = convertView.findViewById(R.id.ebtnQuantity);
        LinearLayout llText = convertView.findViewById(R.id.llText);
        LinearLayout llQuantity = convertView.findViewById(R.id.llQuantity);
        TextView textView = ebtnQuantity.findViewById(com.cepheuen.elegantnumberbutton.R.id.number_counter);
        ebtnQuantity.setTag(grpPos + "-" + childPos);
        textView.setTag(ebtnQuantity);

        llText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDB sqLiteDB = SQLiteDB.getInstance(_context);
                OrderProduct orderProduct = _listDataChild.get(_listDataHeader.get(grpPos)).get(childPos);
                String catName = sqLiteDB.selectCategory(orderProduct.getCat_id()).getName();
                ProductData productData = new ProductData(
                        orderProduct.getId(), orderProduct.getCat_id(), catName,
                        orderProduct.getName(), orderProduct.getExtra_info(),
                        orderProduct.getPrice());
                Bundle bundle = new Bundle();
                Intent intent = new Intent(_context, MultiProductsActivity.class);
                bundle.putString("catname", catName);
                bundle.putParcelable("product", productData);
                intent.putExtras(bundle);
                _context.startActivity(intent);
            }
        });

        ebtnQuantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                ((OrderActivity) _context).setDataChanged(true);
                OrderProduct orderProduct1 = ((OrderProduct) view.getTag());
                orderProduct1.setQuantity(newValue);
                double total = 0.0;
                for(String header : _listDataHeader) {
                    List<OrderProduct> orderProductList = _listDataChild.get(header);
                    for(OrderProduct orderProduct : orderProductList) {
                        total += orderProduct.getPrice() * orderProduct.getQuantity();
                    }
                }
                txtPrice.setText(_context.getString(R.string.order_total).replace("[amount]", String.format(locale, "%.2f", total)));
            }
        });

        Shared.changeButton(_context, ebtnQuantity);

        txtProduct.setText(childText);
        txtProductExtraInfo.setText(childExtraText);
        ebtnQuantity.setNumber(String.valueOf(quantity));
        txtQuantity.setText(String.valueOf(quantity));

        if(showQuantity) {
            ebtnQuantity.setVisibility(View.VISIBLE);
            llQuantity.setVisibility(View.GONE);
            ebtnQuantity.setTag(getChild(groupPosition, childPosition));

        } else {
            ebtnQuantity.setVisibility(View.GONE);
            llQuantity.setVisibility(View.VISIBLE);
        }

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderProduct orderProduct = ((OrderProduct) ((ElegantNumberButton) v.getTag()).getTag());
                int quantity = Integer.parseInt (((ElegantNumberButton) v.getTag()).getNumber());
                showQuantityDialog(_context, ((ElegantNumberButton) v.getTag()), orderProduct.getName(), quantity, orderProduct);
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.order_header_item, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        ImageView imgIndicator = convertView.findViewById(R.id.imgIndicator);

        lblListHeader.setText(headerTitle);

        if(isExpanded) {
            imgIndicator.setBackgroundResource(R.drawable.arrow_up);
        } else {
            imgIndicator.setBackgroundResource(R.drawable.arrow_down);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void showQuantityDialog(final Context context, final ElegantNumberButton button, String title,
                                          int quantity, final OrderProduct orderProduct) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_quantity, null);
        final EditText edtQuantity = grid.findViewById(R.id.edtQuantity);
        if(quantity > 0) {
            edtQuantity.setText(String.valueOf(quantity));
            edtQuantity.post(new Runnable() {
                @Override
                public void run() {
                    edtQuantity.setSelection(edtQuantity.getText().length());
                }
            });
        }
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText(title)
                .setConfirmText(context.getString(R.string.ok))
                .setCancelText(context.getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        if(edtQuantity.getText().toString().trim().length() > 0) {
                            int q = Integer.parseInt(edtQuantity.getText().toString());
                            button.setNumber(String.valueOf(q));
                            orderProduct.setQuantity(q);
                            notifyDataSetChanged();
                        }
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
        edtQuantity.requestFocus();
        edtQuantity.post(new Runnable() {
            @Override
            public void run() {
                UIUtil.showKeyboard(context, edtQuantity);
            }
        });

    }

    public void setPrice(TextView txtPrice) {
        this.txtPrice = txtPrice;
    }
}
