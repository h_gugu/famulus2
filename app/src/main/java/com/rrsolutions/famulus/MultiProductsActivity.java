package com.rrsolutions.famulus;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.rrsolutions.famulus.businesslogic.MainMenu;
import com.rrsolutions.famulus.datawrapper.ProductData;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.PrinterOption;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.fragment.MultiProduct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MultiProductsActivity extends AppCompatActivity {

    private Context mContext = null;
    private String sCategoryName = "";
    private ProductData productData = null;
    private Bundle bundle = new Bundle();

    private Button btnBack = null;
    private Button btnForward = null;
    private Button btnAdd = null;
    private LinearLayout llContent = null;
    private List<MultiProduct> multiProductList = new ArrayList<>();
    private boolean isActivityChanged = false;
    private MainMenu mainMenu;

    @Override
    public void onBackPressed() {
        isActivityChanged = true;
        save();
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Intent intent = new Intent(MultiProductsActivity.this, ProductsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("catid", productData.getCat_id());
        bundle.putString("catname", sqLiteDB.selectCategory(productData.getCat_id()).getName());
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_products);
        mContext = this;
        btnBack = findViewById(R.id.btnBack);
        btnForward = findViewById(R.id.btnForward);
        btnAdd = findViewById(R.id.btnAdd);
        llContent = findViewById(R.id.llContent);

        btnAdd.setVisibility(View.VISIBLE);

        savedInstanceState = getIntent().getExtras();
        if(savedInstanceState != null) {
            sCategoryName = savedInstanceState.getString("catname");
            productData = getIntent().getParcelableExtra("product");
            bundle = savedInstanceState;
        }

        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(productData.getName());
            getSupportActionBar().setSubtitle(getsCategoryName());
        }

        mainMenu = new MainMenu(mContext);

        btnAdd.setOnClickListener(btnAdd_onClick);
        btnBack.setOnClickListener(btnBack_onClick);
        btnForward.setOnClickListener(btnForward_onClick);

        Set<String> proopt = new HashSet<>();
        Storage.init(mContext);
        proopt = Storage.get("pro" + productData.getId(), proopt);
        List<String> pro = new ArrayList<>(proopt);
        Collections.sort(pro, new Comparator<String>(){
            public int compare(String o1, String o2) {
                System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
                String[] o1d = o1.split("\\|");
                int package1 = Integer.parseInt(o1d[o1d.length - 1]);
                String[] o2d = o2.split("\\|");
                int package2 = Integer.parseInt(o2d[o2d.length - 1]);
                return package1 - package2;
            }
        });
        if(pro.size() > 0) {
            for(String string : pro) {
                addProduct(string);
            }
        } else {
            addProduct("");
        }
    }

    private View.OnClickListener btnBack_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isActivityChanged = true;
            save();
            SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
            Intent intent = new Intent(MultiProductsActivity.this, ProductsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putInt("catid", productData.getCat_id());
            bundle.putString("catname", sqLiteDB.selectCategory(productData.getCat_id()).getName());
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    private View.OnClickListener btnForward_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isActivityChanged = true;
            save();
            Intent intent = new Intent(mContext, OrderActivity.class);
            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener btnAdd_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            addProduct("");
        }
    };

    private void addProduct(String product) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MultiProduct multiProduct = new MultiProduct();
        multiProduct.setRestore_product(product);
        multiProduct.setArguments(bundle);
        fragmentTransaction.add(R.id.llContent, multiProduct, productData.getName() + (multiProductList.size() + 1));
        fragmentTransaction.commit();
        multiProductList.add(multiProduct);
    }

    public void save() {
        Set<String> proopt = new HashSet<>();
        int i = 1;
        for(MultiProduct multiProduct : multiProductList) {
            if(multiProduct.getData().length() > 0) {
                proopt.add(multiProduct.getData() + "|" + i); // Make every element distinct in set.
                i++;
            }
        }
        Storage.init(mContext);
        if(proopt.size() > 0) {
            Storage.save(productData.getId(), proopt, 1);
        } else {
            Storage.remove("pro" + productData.getId());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName());
        String json = productData == null ? null : new Gson().toJson(productData);
        Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName() , getsCategoryName() + "|" + json);
        if(!isActivityChanged()) {
            save();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        boolean isPrintingMode = Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal();
        if(isPrintingMode) {
            menu.findItem(R.id.action_printing_mode).setVisible(true);
            menu.findItem(R.id.action_normal_printing).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.CATEGORY.ordinal());
            menu.findItem(R.id.action_one_product_per_ticket).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.PRODUCT.ordinal());
        }
        mainMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }

    public boolean isActivityChanged() {
        return isActivityChanged;
    }

    public void setActivityChanged(boolean activityChanged) {
        isActivityChanged = activityChanged;
    }

    public String getsCategoryName() {
        return sCategoryName;
    }
}
