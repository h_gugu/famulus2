package com.rrsolutions.famulus.printer;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class PrintOption {
    private int id = 0;
    private String name = "";

    public PrintOption() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
