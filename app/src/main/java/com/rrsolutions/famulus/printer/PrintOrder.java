package com.rrsolutions.famulus.printer;

import android.content.Context;

import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.OrderType;
import com.rrsolutions.famulus.modeljson.Category;
import com.rrsolutions.famulus.modeljson.HistoryItem;
import com.rrsolutions.famulus.modeljson.Option;
import com.rrsolutions.famulus.modeljson.Printer;
import com.rrsolutions.famulus.modeljson.Product;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class PrintOrder {

    /* Code for printer object

    Align Center = 10
    Align Left = 11
    Align Right = 12

    Biggest font size = 20
    Big font size = 21
    Normal font size = 22

    Bold = 30
    Normal = 31
    Underline = 32

    Space 2 Lines = 41
    Add Cut = 42

     */

    /*

    Table_Id = -1  Print for Cashier
    Table_Id = -2  Print all products
    Table_Id = -3  Print order history

     */

    private StringBuilder receipt = new StringBuilder();
    private String table = "";
    private List<PrintCategory> listCategory = new ArrayList<>();
    private Context mContext = null;
    private long orderId = 0;
    private int printType = 0;
    private String tableid = "";

    private int count = 0;

    private int quantityCharSize = 4;
    private int productCharSize = 30;
    private int productTotalCharSize = 7;
    private int historyTotalSpace = 15;
    private List<Long> orderIdList = new ArrayList<>();

    public PrintOrder(Context context) {
        mContext = context;
    }

//    public String getMac() {
//        return mac;
//    }
//
//    public void setMac(String mac) {
//        this.mac = mac;
//    }

    public List<PrintCategory> getListCategory() {
        return listCategory;
    }

    public void setListCategory(List<PrintCategory> listCategory) {
        this.listCategory = listCategory;
    }

    public void add(PrintCategory printCategory) {
        this.listCategory.add(printCategory);
    }

//    public String getReceipt() {
//        if (OrderType.RECEIPT.ordinal() == getPrintType()) makeReceipt();
//        //if(OrderType.PRODUCT_PRICES.ordinal() == getPrintType()) makeProductsList();
//        //if(OrderType.HISTORY.ordinal() == getPrintType()) makeOrderHistoryList();
//        return receipt.toString();
//    }

    public String getTestPrint(Printer printer) {
        makeTestPrint(printer);
        return receipt.toString();
    }

    public List<PrintCategory> getPrintCategories() {
        return listCategory;
    }

    public String makeReceipt(PrintCategory printCategory) {
        if (receipt.length() > 0) receipt.delete(0, receipt.length());
        double gtotal = 0.0;
        String table = mContext.getString(R.string.order_table);
        Storage.init(mContext);
        if(this.table.equalsIgnoreCase("")) {
            table = mContext.getString(R.string.cashier);
        } else {
            table = table.replace("[no]", this.table).replace(":", "");
        }
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        String waiter = sqLiteDB.getWaiterName();
        Calendar calendar = new GregorianCalendar();
        receipt.append("10|");
        receipt.append(Shared.sdfReceiptDateTime.format(calendar.getTime()));
        receipt.append("\n|");
        receipt.append(mContext.getString(R.string.waiter));
        receipt.append(": ");
        receipt.append(waiter);
        receipt.append("\n\n|");
        receipt.append("11|");
        receipt.append("20|");
        receipt.append(table);
        receipt.append("\n\n|");
        receipt.append("31|");
        receipt.append("21|");
        receipt.append(printCategory.getCatname());
        receipt.append("\n\n|");
        for (PrintProduct printProduct : printCategory.getListProduct()) {
            receipt.append("31|");
            receipt.append("22|");
            String product = "";
            product += Shared.repeat(" ",
                    quantityCharSize - String.valueOf(printProduct.getQuantity()).length()) +
                    String.valueOf(printProduct.getQuantity());
            product += " x ";
            product += printProduct.getName() + Shared.repeat(" ",
                    productCharSize - printProduct.getName().length());
            product += "  ";
            String total = String.format("%.2f", printProduct.getQuantity() * printProduct.getPrice());
            gtotal += printProduct.getQuantity() * printProduct.getPrice();
            product += total + Shared.repeat(" ",
                    productTotalCharSize - total.length());
            receipt.append(product);
            receipt.append("\n|");
            boolean option = false;
            if (printProduct.getDescription().trim().length() > 0) {
                receipt.append("30|");
                receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                receipt.append(printProduct.getDescription());
                receipt.append("\n|");
                option = true;
            }
            if (printProduct.getListOption().size() > 0) {
                receipt.append("30|");
                for (PrintOption printOption : printProduct.getListOption()) {
                    receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                    receipt.append(printOption.getName());
                    receipt.append("\n");
                }
                option = true;
            }
            if (option) receipt.append("|");
        }
        receipt.append("\n\n|");
        //receipt.append("\n|");
        receipt.append(Shared.repeat(" ", quantityCharSize + 2 + productCharSize));
        receipt.append(Shared.repeat("-", productTotalCharSize + 3));
        receipt.append("\n|");
        receipt.append(Shared.repeat(" ", quantityCharSize + 3));
        receipt.append(mContext.getString(R.string.total));
        receipt.append(Shared.repeat(" ",
                productCharSize - mContext.getString(R.string.total).length()));
        String gTotal = String.format("%.2f", gtotal);
        receipt.append(Shared.repeat(" ",
                productTotalCharSize - gTotal.length()));
        receipt.append(gTotal);
        receipt.append("\n\n|");
        receipt.append("10|");
        receipt.append(mContext.getString(R.string.receipt_footer));
        receipt.append("\n|");
        receipt.append(mContext.getString(R.string.receipt_footer2));
        receipt.append("\n|");
        receipt.append("41|");
        receipt.append("42|");
        return receipt.toString();
    }

    public void makeReceipt() {
        if (receipt.length() > 0) receipt.delete(0, receipt.length());
        if (listCategory.size() > 0) {
            for (PrintCategory printCategory : listCategory) {
                double gtotal = 0.0;
                String table = mContext.getString(R.string.order_table);
                if(this.table.equalsIgnoreCase("")) {
                    table = mContext.getString(R.string.cashier);
                } else {
                    table = table.replace("[no]", this.table).replace(":", "");
                }
                SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                String waiter = sqLiteDB.getWaiterName();
                Calendar calendar = new GregorianCalendar();
                receipt.append("10|");
                receipt.append(Shared.sdfReceiptDateTime.format(calendar.getTime()));
                receipt.append("\n|");
                receipt.append(mContext.getString(R.string.waiter));
                receipt.append(": ");
                receipt.append(waiter);
                receipt.append("\n\n|");
                receipt.append("11|");
                receipt.append("20|");
                receipt.append(table);
                receipt.append("\n\n|");
                receipt.append("31|");
                receipt.append("21|");
                receipt.append(printCategory.getCatname());
                receipt.append("\n\n|");
                for (PrintProduct printProduct : printCategory.getListProduct()) {
                    receipt.append("31|");
                    receipt.append("22|");
                    String product = "";
                    product += Shared.repeat(" ",
                            quantityCharSize - String.valueOf(printProduct.getQuantity()).length()) +
                            String.valueOf(printProduct.getQuantity());
                    product += " x ";
                    product += printProduct.getName() + Shared.repeat(" ",
                            productCharSize - printProduct.getName().length());
                    product += "  ";
                    String total = String.format("%.2f", printProduct.getQuantity() * printProduct.getPrice());
                    gtotal += printProduct.getQuantity() * printProduct.getPrice();
                    product += total + Shared.repeat(" ",
                            productTotalCharSize - total.length());
                    receipt.append(product);
                    receipt.append("\n|");
                    boolean option = false;
                    if (printProduct.getDescription().trim().length() > 0) {
                        receipt.append("30|");
                        receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                        receipt.append(printProduct.getDescription());
                        receipt.append("\n|");
                        option = true;
                    }
                    if (printProduct.getListOption().size() > 0) {
                        receipt.append("30|");
                        for (PrintOption printOption : printProduct.getListOption()) {
                            receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                            receipt.append(printOption.getName());
                            receipt.append("\n");
                        }
                        option = true;
                    }
                    if (option) receipt.append("|");
                }
                receipt.append("\n\n|");
                //receipt.append("\n|");
                receipt.append(Shared.repeat(" ", quantityCharSize + 2 + productCharSize));
                receipt.append(Shared.repeat("-", productTotalCharSize + 3));
                receipt.append("\n|");
                receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                receipt.append(mContext.getString(R.string.total));
                receipt.append(Shared.repeat(" ",
                        productCharSize - mContext.getString(R.string.total).length()));
                String gTotal = String.format("%.2f", gtotal);
                receipt.append(Shared.repeat(" ",
                        productTotalCharSize - gTotal.length()));
                receipt.append(gTotal);
                receipt.append("\n\n|");
                receipt.append("10|");
                receipt.append(mContext.getString(R.string.receipt_footer));
                receipt.append("\n|");
                receipt.append(mContext.getString(R.string.receipt_footer2));
                receipt.append("\n|");
                receipt.append("41|");
                receipt.append("42|");
            }

        }
    }

    public String makeProductsList() {
        if (receipt.length() > 0) receipt.delete(0, receipt.length());
        SQLiteDB sqLiteDB = new SQLiteDB(mContext);
        List<Category> categoryList = sqLiteDB.selectAllCategories();
        int totaltotal = 0;
        if (categoryList.size() > 0) {
            double gtotal = 0.0;
            String waiter = sqLiteDB.getWaiterName();
            Calendar calendar = new GregorianCalendar();
            receipt.append("10|");
            receipt.append(Shared.sdfReceiptDateTime.format(calendar.getTime()));
            receipt.append("\n|");
            receipt.append(mContext.getString(R.string.waiter));
            receipt.append(": ");
            receipt.append(waiter);
            receipt.append("\n\n|");
            receipt.append("11|");
            receipt.append("20|");
            receipt.append(mContext.getString(R.string.event_products));
            receipt.append("\n\n|");
            for (Category category : categoryList) {
                receipt.append("31|");
                receipt.append("21|");
                receipt.append(category.getName());
                receipt.append("\n\n|");
                List<Product> productList = sqLiteDB.selectAllProducts(category.getId());
                for (Product pro : productList) {
                    receipt.append("31|");
                    receipt.append("22|");
                    String product = "";
                    product += Shared.repeat(" ", quantityCharSize - 1) + "1";
                    product += " x ";
                    product += pro.getName() + Shared.repeat(" ",
                            productCharSize - pro.getName().length());
                    product += "  ";
                    totaltotal += pro.getPrice();
                    String total = String.format("%.2f", 1 * pro.getPrice());
                    gtotal += 1 * pro.getPrice();
                    product += total + Shared.repeat(" ",
                            productTotalCharSize - total.length());
                    receipt.append(product);
                    receipt.append("\n|");
                    boolean option = false;
                    List<Option> optionList = sqLiteDB.selectAllOptions(pro.getId());
                    if (optionList.size() > 0) {
                        receipt.append("30|");
                        for (Option opt : optionList) {
                            receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                            receipt.append(opt.getName());
                            receipt.append("\n");
                        }
                        option = true;
                    }
                    if (option) receipt.append("|");
                }
                receipt.append("\n\n|");
            }
            receipt.append("\n|");
            receipt.append("10|");
            receipt.append(mContext.getString(R.string.receipt_footer));
            receipt.append("\n|");
            receipt.append(mContext.getString(R.string.receipt_footer2));
            receipt.append("\n|");
            receipt.append("41|");
            receipt.append("42|");
        }
        return receipt.toString();
    }

    private PrintProduct getPrintProduct(PrintProduct printProduct, List<PrintProduct> list) {
        for (PrintProduct printProduct1 : list) {
            if (printProduct.getId() == printProduct1.getId()) {
                return printProduct1;
            }
        }
        return null;
    }

    private boolean hasOrderId(long orderId, List<HistoryItem> historyItems) {
        for (HistoryItem historyItem : historyItems) {
            if (historyItem.getOrderid() == orderId)
                return true;
        }
        return false;
    }

    public String makeOrderHistoryList() {
        if (receipt.length() > 0) receipt.delete(0, receipt.length());
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Storage.init(mContext);
        String waiter = sqLiteDB.getWaiterName();
        Hashtable<Integer, PrintCategory> hashtable = new Hashtable<>();
        List<HistoryItem> historyItems = sqLiteDB.getOrders(waiter, Storage.get(Shared.sUserTypeKey, 0), tableid);
        boolean printSelected = orderIdList.size() > 0;
        if (printSelected) {
            for (long oid : orderIdList) {
                if (historyItems.size() > 0) {
                    for (HistoryItem historyItem : historyItems) {
                        if (historyItem.getOrderid() == oid) {
                            if (historyItem.getCategoryList().size() > 0) {
                                PrintCategory printCategory1 = null;
                                PrintProduct printProduct1 = null;
                                for (PrintCategory printCategory : historyItem.getCategoryList()) {
                                    if (hashtable.containsKey(printCategory.getCatid())) {
                                        printCategory1 = hashtable.get(printCategory.getCatid());
                                    } else {
                                        printCategory1 = new PrintCategory();
                                        printCategory1.setCatid(printCategory.getCatid());
                                        printCategory1.setCatname(printCategory.getCatname());
                                    }
                                    if (printCategory.getListProduct().size() > 0) {
                                        for (PrintProduct printProduct : printCategory.getListProduct()) {
                                            printProduct1 = getPrintProduct(printProduct, printCategory1.getListProduct());
                                            if (printProduct1 == null) {
                                                printProduct1 = new PrintProduct();
                                                printProduct1.setId(printProduct.getId());
                                                printProduct1.setName(printProduct.getName());
                                                printProduct1.setPrice(printProduct.getPrice());
                                                printProduct1.setQuantity(printProduct1.getQuantity() + printProduct.getQuantity());
                                                printCategory1.add(printProduct1);
                                            } else {
                                                printProduct1.setQuantity(printProduct1.getQuantity() + printProduct.getQuantity());
                                            }
                                        }
                                    }
                                    hashtable.put(printCategory1.getCatid(), printCategory1);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        } else {
            if (historyItems.size() > 0) {
                for (HistoryItem historyItem : historyItems) {
                    if (historyItem.getCategoryList().size() > 0) {
                        PrintCategory printCategory1 = null;
                        PrintProduct printProduct1 = null;
                        for (PrintCategory printCategory : historyItem.getCategoryList()) {
                            if (hashtable.containsKey(printCategory.getCatid())) {
                                printCategory1 = hashtable.get(printCategory.getCatid());
                            } else {
                                printCategory1 = new PrintCategory();
                                printCategory1.setCatid(printCategory.getCatid());
                                printCategory1.setCatname(printCategory.getCatname());
                            }
                            if (printCategory.getListProduct().size() > 0) {
                                for (PrintProduct printProduct : printCategory.getListProduct()) {
                                    printProduct1 = getPrintProduct(printProduct, printCategory1.getListProduct());
                                    if (printProduct1 == null) {
                                        printProduct1 = new PrintProduct();
                                        printProduct1.setId(printProduct.getId());
                                        printProduct1.setName(printProduct.getName());
                                        printProduct1.setPrice(printProduct.getPrice());
                                        printProduct1.setQuantity(printProduct1.getQuantity() + printProduct.getQuantity());
                                        printCategory1.add(printProduct1);
                                    } else {
                                        printProduct1.setQuantity(printProduct1.getQuantity() + printProduct.getQuantity());
                                    }
                                }
                            }
                            hashtable.put(printCategory1.getCatid(), printCategory1);
                        }
                    }
                }
            }
        }
        if (hashtable.size() > 0) {
            double historytotal = 0.0;
            Calendar calendar = new GregorianCalendar();
            receipt.append("10|");
            receipt.append(Shared.sdfReceiptDateTime.format(calendar.getTime()));
            receipt.append("\n|");
            receipt.append(mContext.getString(R.string.waiter));
            receipt.append(": ");
            receipt.append(waiter);
            receipt.append("\n\n|");
            receipt.append("11|");
            receipt.append("20|");
            receipt.append(mContext.getString(R.string.order_history));
            receipt.append("\n\n|");
            for (PrintCategory printCategory : hashtable.values()) {
                double gtotal = 0;
                receipt.append("31|");
                receipt.append("21|");
                receipt.append(printCategory.getCatname());
                receipt.append("\n\n|");
                if (printCategory.getListProduct().size() > 0) {
                    for (PrintProduct printProduct : printCategory.getListProduct()) {
                        receipt.append("31|");
                        receipt.append("22|");
                        String product = "";
                        product += Shared.repeat(" ", quantityCharSize - String.valueOf(printProduct.getQuantity()).length())
                                + String.valueOf(printProduct.getQuantity());
                        product += " x ";
                        product += printProduct.getName() + Shared.repeat(" ",
                                productCharSize - printProduct.getName().length());
                        product += "  ";
                        String total = String.format("%.2f", printProduct.getQuantity() * printProduct.getPrice());
                        historytotal += printProduct.getQuantity() * printProduct.getPrice();
                        gtotal += printProduct.getQuantity() * printProduct.getPrice();
                        product += total + Shared.repeat(" ",
                                productTotalCharSize - total.length());
                        receipt.append(product);
                        receipt.append("\n|");
                    }
                    receipt.append("\n|");
                    receipt.append(Shared.repeat(" ", quantityCharSize + 2 + productCharSize));
                    receipt.append(Shared.repeat("-", productTotalCharSize + 3));
                    receipt.append("\n|");
                    receipt.append(Shared.repeat(" ", quantityCharSize + 3));
                    receipt.append(mContext.getString(R.string.total));
                    receipt.append(Shared.repeat(" ",
                            productCharSize - mContext.getString(R.string.total).length()));
                    String gTotal = String.format("%.2f", gtotal);
                    receipt.append(Shared.repeat(" ",
                            productTotalCharSize - gTotal.length()));
                    receipt.append(gTotal);
                    receipt.append("\n\n|");
                }
            }
            receipt.append("\n|");
            receipt.append("31|");
            receipt.append("21|");
            receipt.append(mContext.getString(R.string.total));
            receipt.append(Shared.repeat(" ",
                    historyTotalSpace - mContext.getString(R.string.total).length()));
            String gTotal = String.format("%.2f", historytotal);
            receipt.append(Shared.repeat(" ",
                    productTotalCharSize - gTotal.length()));
            receipt.append(gTotal);
            receipt.append("\n\n|");
            receipt.append("10|");
            receipt.append("31|");
            receipt.append("22|");
            receipt.append(mContext.getString(R.string.receipt_footer));
            receipt.append("\n|");
            receipt.append(mContext.getString(R.string.receipt_footer2));
            receipt.append("\n|");
            receipt.append("41|");
            receipt.append("42|");
        }
        return receipt.toString();
    }

    public void makeTestPrint(Printer printer) {
        if (receipt.length() > 0) receipt.delete(0, receipt.length());
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        String waiter = sqLiteDB.getWaiterName();
        Calendar calendar = new GregorianCalendar();
        List<String> categories = sqLiteDB.selectPrinterCategoriesName(printer.getName());
        if (sqLiteDB.hasOtherCategory(printer.getId())) {
            categories.add(mContext.getString(R.string.cat_others));
        }
        receipt.append("10|");
        receipt.append(Shared.sdfReceiptDateTime.format(calendar.getTime()));
        receipt.append("\n|");
        receipt.append(mContext.getString(R.string.waiter));
        receipt.append(": ");
        receipt.append(waiter);
        receipt.append("\n\n|");
        receipt.append("11|");
        receipt.append("31|");
        receipt.append("21|");
        receipt.append(printer.getName());
        receipt.append("\n|");
        if (categories.size() > 0) {
            for (String category : categories) {
                receipt.append("22|");
                receipt.append(category);
                receipt.append("\n|");
            }
        }
        receipt.append("\n\n|");
        receipt.append("10|");
        receipt.append("31|");
        receipt.append("22|");
        receipt.append(mContext.getString(R.string.receipt_footer));
        receipt.append("\n|");
        receipt.append(mContext.getString(R.string.receipt_footer2));
        receipt.append("\n|");
        receipt.append("41|");
        receipt.append("42|");
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public int getPrintType() {
        return printType;
    }

    public void setPrintType(int printType) {
        this.printType = printType;
    }

    public String getTableid() {
        return tableid;
    }

    public void setTableid(String tableid) {
        this.tableid = tableid;
    }

    public List<Long> getOrderIdList() {
        return orderIdList;
    }

    public void setOrderIdList(List<Long> orderIdList) {
        this.orderIdList = orderIdList;
    }
}
