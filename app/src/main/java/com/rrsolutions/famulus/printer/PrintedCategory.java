package com.rrsolutions.famulus.printer;

/**
 * Created by hassanmushtaq on 31/12/2017.
 */

public class PrintedCategory {
    private long orderid = 0;
    private int catid = 0;
    private String mac = "";

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
}
