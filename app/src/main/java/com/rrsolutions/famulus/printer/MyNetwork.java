package com.rrsolutions.famulus.printer;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;

/**
 * Created by hassanmushtaq on 24/12/2017.
 */

public class MyNetwork {

    private Context mContext = null;
    private boolean connected = false;
    private String name = "";
    private String ip = "";
    private int bandwidth = 0;

    public MyNetwork(Context context) {
        mContext = context;
    }

    public void fetchNetworkInfo() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null) {
            connected = false;
            return;
        }

        if (networkInfo.isConnected()) {
            connected = true;
            final WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                name = connectionInfo.getSSID();
                bandwidth = connectionInfo.getLinkSpeed();
            }
        }
    }

    public boolean isWifiConnected() {
        connected = false;
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
//        if (networkInfo == null) {
//            connected = false;
//            return false;
//        }
        if (mWifi.isConnected()) {
            connected = true;
            final WifiManager wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                name = connectionInfo.getSSID();
                bandwidth = connectionInfo.getLinkSpeed();
            }
        }
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(int bandwidth) {
        this.bandwidth = bandwidth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
