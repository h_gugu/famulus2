package com.rrsolutions.famulus.printer;

import android.content.Context;

import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.schedular.SchedulerManagement;

import java.sql.SQLClientInfoException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class PrintManagement {

    private Context mContext = null;
    private Hashtable<Integer, PrintCategory> hashCategory = new Hashtable<>();
    private Hashtable<String, PrintOrder> hashPrintOrder = new Hashtable<>();
    private long orderid = 0;

    public PrintManagement(Context context) {
        mContext = context;
    }

    public void printSession() {
        getSessionOrder();
        assignCategoriestoPrinter();
        if(hashPrintOrder.size() > 0) {
            for (String mac : hashPrintOrder.keySet()) {
                if(Shared.isPrinterAvailable(mac)) {
                    PrintOrder printOrder = hashPrintOrder.get(mac);
                    printOrder.setOrderId(getOrderid());
                    printOrder.setCount(1);
                    printOrder.setTable(Storage.get(Shared.sTableKey, ""));
                    printOrder.setOrderId(orderid);
                    MyPrinter myPrinter = new MyPrinter(mContext, mac);
                    SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                    myPrinter.setPrinterData(sqLiteDB.getPrinter(mac));
                    myPrinter.addinQueue(printOrder);
                    synchronized (Shared.hashtablePrinting) {
                        Shared.hashtablePrinting.put(mac, myPrinter);
                    }
                    new Thread(myPrinter).start();
                }
            }
            SchedulerManagement schedulerManagement = new SchedulerManagement(mContext);
            schedulerManagement.createJob();
        }
    }



    private void getSessionOrder() {
        Storage.init(mContext);
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Map<String, Set<String>> map = Storage.getAllProducts();
        for(String key : map.keySet()) {
            PrintCategory printCategory = null;
            List<PrintProduct> list2 = null;
            Set<String> pros = map.get(key);
            List<String> products = new ArrayList<>(pros);
            Collections.sort(products, new Comparator<String>(){
                public int compare(String o1, String o2) {
                    System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
                    String[] o1d = o1.split("\\|");
                    int package1 = Integer.parseInt(o1d[o1d.length - 1]);
                    String[] o2d = o2.split("\\|");
                    int package2 = Integer.parseInt(o2d[o2d.length - 1]);
                    return package1 - package2;
                }
            });
            for(String product : products) {
                String[] data = product.substring(0, product.lastIndexOf("|")).split("\\|");
                if(hashCategory.containsKey(Integer.parseInt(data[0]))) {
                    printCategory = hashCategory.get(Integer.parseInt(data[0]));
                    list2 = printCategory.getListProduct();
                }
                else {
                    printCategory = new PrintCategory();
                    printCategory.setCatid(Integer.parseInt(data[0]));
                    printCategory.setCatname(data[1]);
                    hashCategory.put(Integer.parseInt(data[0]), printCategory);
                    list2 = new ArrayList<>();
                }
                PrintProduct printProduct = new PrintProduct();
                printProduct.setId(Integer.parseInt(data[2]));
                printProduct.setName(data[3]);
                printProduct.setPrice(Double.parseDouble(data[5]));
                printProduct.setQuantity(Integer.parseInt(data[6]));
                List<PrintOption> list = new ArrayList<>();
                if(data.length > 7) {
                    if(data[7].startsWith("d")) {
                        printProduct.setDescription(data[7].substring(1));
                    } else if(data[7].startsWith("o")){
                        data[7] = data[7].substring(1);
                        String[] options = data[7].split(",");
                        for(String option : options) {
                            PrintOption printOption = new PrintOption();
                            printOption.setId(Integer.parseInt(option));
                            printOption.setName(sqLiteDB.getOptionName(Integer.parseInt(option)));
                            list.add(printOption);
                        }
                    }
                }
                if(data.length > 8) {
                    data[8] = data[8].substring(1);
                    String[] options = data[8].split(",");
                    for(String option : options) {
                        PrintOption printOption = new PrintOption();
                        printOption.setId(Integer.parseInt(option));
                        printOption.setName(sqLiteDB.getOptionName(Integer.parseInt(option)));
                        list.add(printOption);
                    }
                }
                printProduct.setListOption(list);
                list2.add(printProduct);
                printCategory.setListProduct(list2);
            }
        }
    }

    private void assignCategoriestoPrinter() {
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        if(hashCategory.size() > 0) {
            PrintOrder printOrder = null;
            List<PrintCategory> printCategories = null;
            for(int key : hashCategory.keySet()) {
                String[] macs = sqLiteDB.getPrinterMac(key);
                if(macs != null && macs.length > 0) {
                    for(String mac : macs) {
                        //if(Shared.isPrinterOnline(mac)) {
                        if (hashPrintOrder.containsKey(mac)) {
                            printOrder = hashPrintOrder.get(mac);
                            printCategories = printOrder.getListCategory();
                        } else {
                            printOrder = new PrintOrder(mContext);
                            printCategories = new ArrayList<>();
                            printOrder.setListCategory(printCategories);
                        }
                        printCategories.add(hashCategory.get(key));
                        hashPrintOrder.put(mac, printOrder);
                        //}
                    }
                }
            }
        }
    }

    public boolean updatePendingOrders() {
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        List<Long> listOrderIds = new ArrayList<>();
        Queue<PrintOrder> queue = null;
        Hashtable<String, Queue<PrintOrder>> hashtable = new Hashtable<>();
        Hashtable<String, PrintOrder> hashtable1 = new Hashtable<>();
        listOrderIds = sqLiteDB.getPendingOrder();
        if(listOrderIds.size() > 0) {
            for(long orderid : listOrderIds) {
                PrintOrder printOrder = null;
                String table_id = sqLiteDB.getOrderTable(orderid);
                //if(table_id > -1) {
                    List<Integer> listCatids = sqLiteDB.getPendingCategory(orderid);
                    hashtable1.clear();
                    if(listCatids.size() > 0) {
                        for(int catid : listCatids) {
                            PrintCategory printCategory = sqLiteDB.getPendingCategory(orderid, catid);
                            String[] macs = sqLiteDB.getPrinterMac(printCategory.getCatid());
                            if(macs != null && macs.length > 0) {
                                for(String mac : macs) {
                                    if(hashtable1.containsKey(mac)) {
                                        printOrder = hashtable1.get(mac);
                                    } else {
                                        printOrder = new PrintOrder(mContext);
                                        printOrder.setCount(1);
                                        printOrder.setOrderId(orderid);
                                        printOrder.setTable(String.valueOf(table_id));
                                    }
                                    printOrder.add(printCategory);
                                    hashtable1.put(mac, printOrder);
                                }
                            }
                        }
                        if(hashtable1.size() > 0) {
                            for(String mac : hashtable1.keySet()) {
                                if(hashtable.containsKey(mac)) {
                                    queue = hashtable.get(mac);
                                } else {
                                    queue = new LinkedList<>();
                                }
                                queue.add(hashtable1.get(mac));
                                hashtable.put(mac, queue);
                            }
                        }
                    } else {
                        sqLiteDB.updateOrder(orderid);
                    }
                //}
//                else if(table_id == -2 || table_id == -3) {
//                    String[] macs = sqLiteDB.getPrinterMac(-1);
//                    hashtable1.clear();
//                    if(macs != null && macs.length > 0) {
//                        for(String mac : macs) {
//                            printOrder = new PrintOrder(mContext);
//                            printOrder.setCount(1);
//                            if(table_id == -2) printOrder.setPrintType(OrderType.PRODUCT_PRICES.ordinal());
//                            if(table_id == -3) printOrder.setPrintType(OrderType.HISTORY.ordinal());
//                            printOrder.setOrderId(orderid);
//                            printOrder.setTable(String.valueOf(table_id));
//                            hashtable1.put(mac, printOrder);
//                        }
//                        if(hashtable1.size() > 0) {
//                            for(String mac : hashtable1.keySet()) {
//                                if(hashtable.containsKey(mac)) {
//                                    queue = hashtable.get(mac);
//                                } else {
//                                    queue = new LinkedList<>();
//                                }
//                                queue.add(hashtable1.get(mac));
//                                hashtable.put(mac, queue);
//                            }
//                        }
//                    }
//                }

            }
            if(hashtable.size() > 0) {
                for(String mac : hashtable.keySet()) {
                    if(Shared.isPrinterAvailable(mac)) {
                        MyPrinter myPrinter = new MyPrinter(mContext, mac);
                        myPrinter.setQueue(hashtable.get(mac));
                        myPrinter.setPrinterData(sqLiteDB.getPrinter(mac));
                        new Thread(myPrinter).start();
                        Shared.hashtablePrinting.put(mac, myPrinter);
                    }
                }
            }
        } else {
            Storage.init(mContext);
            Storage.remove(Shared.sBackgroundWorkerKey);
            return false;
        }
        return true;
    }

    private void updatePrintedCategories() {
        List<Long> list = new ArrayList<>();
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        List<PrintedCategory> listCats = sqLiteDB.getPrintedCategories();
        if(listCats.size() > 0) {
            for(PrintedCategory printedCategory : listCats) {
                sqLiteDB.updatePrintedCategory(printedCategory.getOrderid(), printedCategory.getCatid());
                if(!list.contains(printedCategory.getOrderid())) list.add(printedCategory.getOrderid());
            }
            for(Long orderid : list) {
                List<Integer> list1 = sqLiteDB.getPendingCategory(orderid);
                if(list1.size() == 0) {
                    sqLiteDB.updateOrder(orderid);
                    sqLiteDB.deletePrintedCategory(orderid);
                }
            }
        }
    }


    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }
}
