package com.rrsolutions.famulus.printer;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.epson.epos2.Epos2CallbackCode;
import com.epson.epos2.Epos2Exception;
import com.epson.epos2.printer.Printer;
import com.epson.epos2.printer.PrinterStatusInfo;
import com.epson.epos2.printer.ReceiveListener;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.enumeration.OrderType;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by hassanmushtaq on 24/12/2017.
 */

public class MyPrinter implements ReceiveListener, Runnable {

    private Context mContext = null;
    private Printer mPrinter = null;
    private com.rrsolutions.famulus.modeljson.Printer printerData = new com.rrsolutions.famulus.modeljson.Printer();
    private Hashtable<String, Integer> printerModels = new Hashtable<>();
    private String sMac = "";
    private String message = "";
    private PrintOrder printOrder = null;
    private boolean pendingorder = false;
    private Queue<PrintOrder> queue = new LinkedList<>();
    private List<String> printedCat = new ArrayList<>();
    private boolean running = false;
    private int TIMEOUT = 25000;
    private int PRINT_TIMEOUT = 60000;
    private int WAIT_PRINT_STATUS = 4000;
    private int WAIT_NEXT_PRINT = 7000;

    public MyPrinter(Context context, String mac) {
        mContext = context;
        init();
        setMac(mac);
    }

    public void addinQueue(PrintOrder printOrder1) {
        queue.add(printOrder1);
    }

    private void init() {
        printerModels.put("TM-M10", Printer.TM_M10);
        printerModels.put("TM-M30", Printer.TM_M30);
        printerModels.put("TM-M30 (122)", Printer.TM_M30);
        printerModels.put("TM-P20", Printer.TM_P20);
        printerModels.put("TM-P60", Printer.TM_P60);
        printerModels.put("TM-P60II", Printer.TM_P60II);
        printerModels.put("TM-P80", Printer.TM_P80);
        printerModels.put("TM-T20", Printer.TM_T20);
        printerModels.put("TM-T20II", Printer.TM_T20);
        printerModels.put("TM-T60", Printer.TM_T60);
        printerModels.put("TM-T70", Printer.TM_T70);
        printerModels.put("TM-T81", Printer.TM_T81);
        printerModels.put("TM-T82", Printer.TM_T82);
        printerModels.put("TM-T83", Printer.TM_T83);
        printerModels.put("TM-T88", Printer.TM_T88);
        printerModels.put("TM-T90", Printer.TM_T90);
        printerModels.put("TM-T90KP", Printer.TM_T90KP);
        printerModels.put("TM-U220", Printer.TM_U220);
        printerModels.put("TM-U330", Printer.TM_U330);
        printerModels.put("TM-L90", Printer.TM_L90);
        printerModels.put("TM-H6000", Printer.TM_H6000);
    }

    // region Print Process

    private boolean initializeObject() {
        try {
            //mPrinter = new Printer(printerModels.get(connectedPrinter.getName()), Printer.MODEL_ANK, mContext);
            int series = 6;
            try {
                series = printerModels.get(printerData.getModel().toUpperCase());
            } catch (Exception ex) {
                series = 6;
            }
            mPrinter = new Printer(series, Printer.MODEL_ANK, mContext);
        } catch (Exception e) {
            return false;
        }
        mPrinter.setReceiveEventListener(this);
        return true;
    }

    private boolean connectPrinter() {
        boolean isBeginTransaction = false;
        if (mPrinter == null) {
            return false;
        }

        try {
            //mPrinter.connect("TCP:" + getMac(), Printer.PARAM_DEFAULT);
            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mContext, "Trying to Connect at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
                }
            });
            mPrinter.connect("TCP:" + getMac(), TIMEOUT);
            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mContext, "Connected at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Epos2Exception e) {
            error = "Error status = " + e.getErrorStatus();
            ConnectionError = e.getErrorStatus();
            new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(mContext, error + " at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
                }
            });
            return false;
        }
        try {
            mPrinter.beginTransaction();
            isBeginTransaction = true;
        } catch (Exception e) {

        }

        if (!isBeginTransaction) {
            try {
                mPrinter.disconnect();
            } catch (Epos2Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        return true;
    }

    private boolean isPrintable(PrinterStatusInfo status) {
        if (status == null) {
            return false;
        }
        if (status.getConnection() == Printer.FALSE) {
            return false;
        } else if (status.getOnline() == Printer.FALSE) {
            return false;
        } else {
            //print available
        }
        return true;
    }

    private boolean printData() {
        if (mPrinter == null) {
            return false;
        }
        try {
            mPrinter.sendData(PRINT_TIMEOUT);
        } catch (Exception e) {
            try {
                //disconnectPrinter();
                mPrinter.disconnect();
                return false;
            } catch (Exception ex) {
                // Do nothing
            }
            return false;
        }
        return true;
    }

    // endregion

    @Override
    public void onPtrReceive(Printer printer, int code, PrinterStatusInfo printerStatusInfo, String s) {
        if (Epos2CallbackCode.CODE_SUCCESS == code) {
            if(printOrder.getPrintType() == OrderType.RECEIPT.ordinal()) {
                SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                sqLiteDB.updatePrintedCategory(printOrder.getOrderId(), printedCategory.getCatid());
                Log.e("Printed job", getMac() + " - " + printOrder.getOrderId() + " - " + printedCategory.getCatid());
                synchronized (this) {
                    notify();
                }
            } else {
                synchronized (this) {
                    notify();
                }
            }
        } else {
            //disconnectPrinter();
            synchronized (this) {
                notifyAll();
           }
        }
    }

    private PrintCategory printedCategory = null;
    private String error = "";
    private int ConnectionError = 0;


    @Override
    public void run() {
        running = true;
        printOrder = null;

        boolean canPrint = false;
        if (initializeObject()) {
            if (connectPrinter()) {
                PrinterStatusInfo status = mPrinter.getStatus();
                if (isPrintable(status)) {
                    canPrint = true;
                }
            }
        }
        if (canPrint) {
            while (!queue.isEmpty()) {
                try {
                    if (!running) break;
                    printOrder = queue.poll();
                    if (printOrder != null) {
                        if (printOrder.getPrintType() == OrderType.TEST_PRINT.ordinal()) {
                            message = printOrder.getTestPrint(printerData);
                            parseReceiptData();
                            printData();
                            synchronized (this) {
                                wait(WAIT_NEXT_PRINT);
                            }
                        } else if (printOrder.getPrintType() == OrderType.HISTORY.ordinal()) {
                            message = printOrder.makeOrderHistoryList();
                            parseReceiptData();
                            printData();
                            synchronized (this) {
                                wait(WAIT_NEXT_PRINT);
                            }
                        } else if (printOrder.getPrintType() == OrderType.PRODUCT_PRICES.ordinal()) {
                            message = printOrder.makeProductsList();
                            parseReceiptData();
                            printData();
                            synchronized (this) {
                                wait(WAIT_NEXT_PRINT);
                            }
                        } else {
                            Queue<PrintCategory> printCategories = new LinkedList<>(printOrder.getListCategory());
                            while (!printCategories.isEmpty()) {
                                printedCategory = printCategories.poll();
                                message = printOrder.makeReceipt(printedCategory);
                                mPrinter.clearCommandBuffer();
                                parseReceiptData();
                                boolean isPrinted = printData();
                                synchronized (this) {
                                    wait(WAIT_NEXT_PRINT);
                                }
                                if(!isPrinted) {
                                    running = false;
                                    break;
                                }

//                                if(isPrinted) {
//                                    SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
//                                    sqLiteDB.updatePrintedCategory(printOrder.getOrderId(), printedCategory.getCatid());
//                                } else {
//                                    running = false;
//                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    running = false;
                }
            }
        }
        disconnectPrinter();
        running = false;
        synchronized (Shared.hashtablePrinting) {
            Shared.hashtablePrinting.remove(getMac());
        }
    }

    private boolean parseReceiptData() {
        try {
            String[] data = message.split("\\|");
            if (data.length > 0) {
                for (String d : data) {
                    if (d.trim().length() > 0) {
                        if (Shared.isInteger(d)) {
                            switch (Integer.parseInt(d)) {
                                case 10:
                                    mPrinter.addTextAlign(Printer.ALIGN_CENTER);
                                    break;
                                case 11:
                                    mPrinter.addTextAlign(Printer.ALIGN_LEFT);
                                    break;
                                case 12:
                                    mPrinter.addTextAlign(Printer.ALIGN_RIGHT);
                                    break;
                                case 20:
                                    mPrinter.addTextSize(3, 3);
                                    break;
                                case 21:
                                    mPrinter.addTextSize(2, 2);
                                    break;
                                case 22:
                                    mPrinter.addTextSize(1, 1);
                                    break;
                                case 30:
                                    mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.TRUE, Printer.COLOR_1);
                                    break;
                                case 31:
                                    mPrinter.addTextStyle(Printer.FALSE, Printer.FALSE, Printer.FALSE, Printer.COLOR_1);
                                    break;
                                case 32:
                                    mPrinter.addTextStyle(Printer.FALSE, Printer.TRUE, Printer.FALSE, Printer.COLOR_1);
                                    break;
                                case 41:
                                    mPrinter.addFeedLine(2);
                                    break;
                                case 42:
                                    mPrinter.addCut(Printer.CUT_FEED);
                                    break;
                            }
                        } else {
                            mPrinter.addText(d);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    private void finalizeObject() {
        if (mPrinter == null) {
            return;
        }
        mPrinter.clearCommandBuffer();
        mPrinter.setReceiveEventListener(null);
        mPrinter = null;
    }

    private void disconnectPrinter() {
        if (mPrinter == null) {
            return;
        }
        if (ConnectionError == 2) {
//            try {
//                mPrinter.disconnect();
//            } catch (final Epos2Exception e) {
//                error = "Disconnected " + e.getErrorStatus();
//                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
//
//                    @Override
//                    public void run() {
//                        Toast.makeText(mContext, error + " at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
//                    }
//                });
//                e.printStackTrace();
//            }
        }
        else if(ConnectionError == 3) {

        }  else {

            try {
                mPrinter.endTransaction();
            } catch (final Epos2Exception e) {
                error = "End transaction " + e.getErrorStatus();
                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, error + " at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }
            try {
                mPrinter.disconnect();
                error = "Disconnected ";
                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, error + " at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
                    }
                });
            } catch (final Epos2Exception e) {
                error = "Disconnected " + e.getErrorStatus();
                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(mContext, error + " at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
                    }
                });
                e.printStackTrace();
            }
        }

//        new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
//
//            @Override
//            public void run() {
//                Toast.makeText(mContext, "Disconnect at " + Shared.sdfUTCDateTime.format(new Date()), Toast.LENGTH_LONG).show();
//            }
//        });
        synchronized (Shared.hashtablePrinting) {
            Shared.hashtablePrinting.remove(getMac());
        }
        finalizeObject();
    }

    public String getMac() {
        return sMac;
    }

    public void setMac(String sMac) {
        this.sMac = sMac;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isPendingorder() {
        return pendingorder;
    }

    public void setPendingorder(boolean pendingorder) {
        this.pendingorder = pendingorder;
    }

    public void setQueue(Queue<PrintOrder> queue) {
        this.queue = queue;
    }

    public boolean isRunning() {
        return running;
    }

    public void setPrinterData(com.rrsolutions.famulus.modeljson.Printer printerData) {
        this.printerData = printerData;
    }
}
