package com.rrsolutions.famulus.printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class PrintProduct {
    private int id = 0;
    private String name = "";
    private int quantity = 0;
    private double price = 0.0;
    private String extrainfo = "";
    private String description = "";
    private List<PrintOption> listOption = new ArrayList<>();

    public PrintProduct() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PrintOption> getListOption() {
        return listOption;
    }

    public void setListOption(List<PrintOption> listOption) {
        this.listOption = listOption;
    }

    public String getExtrainfo() {
        return extrainfo;
    }

    public void setExtrainfo(String extrainfo) {
        this.extrainfo = extrainfo;
    }
}
