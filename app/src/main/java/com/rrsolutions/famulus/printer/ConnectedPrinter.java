package com.rrsolutions.famulus.printer;

/**
 * Created by hassanmushtaq on 24/12/2017.
 */

public class ConnectedPrinter {

    private String mac = "";
    private String name = "";
    private String printer = "";
    private String ip = "";
    private String network = "";
    private int bandwidth = 0;

    public ConnectedPrinter() {}

    public ConnectedPrinter(String mac, String printer, String name, String ip, String network, int bandwidth) {
        this.mac = mac;
        this.printer = printer;
        this.name = name;
        this.ip = ip;
        this.network = network;
        this.bandwidth = bandwidth;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public int getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(int bandwidth) {
        this.bandwidth = bandwidth;
    }

    public String getPrinter() {
        return printer;
    }

    public void setPrinter(String printer) {
        this.printer = printer;
    }
}
