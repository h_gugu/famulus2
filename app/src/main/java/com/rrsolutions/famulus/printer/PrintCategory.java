package com.rrsolutions.famulus.printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class PrintCategory {

    private int catid = 0;
    private int status = 0;
    private int printer_option = 0;
    private String catname = "";
    private List<PrintProduct> listProduct = new ArrayList<>();

    public PrintCategory() {}

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public List<PrintProduct> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<PrintProduct> listProduct) {
        this.listProduct = listProduct;
    }

    public void add(PrintProduct printProduct) {
        listProduct.add(printProduct);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPrinter_option() {
        return printer_option;
    }

    public void setPrinter_option(int printer_option) {
        this.printer_option = printer_option;
    }
}
