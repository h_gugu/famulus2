package com.rrsolutions.famulus;

import android.app.Application;

import com.evernote.android.job.JobManager;
import com.rrsolutions.famulus.schedular.MyJobCreator;

/**
 * Created by hassanmushtaq on 25/12/2017.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JobManager.create(this).addJobCreator(new MyJobCreator());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        //Log.i(TAG, "onLowMemory()");
    }

}
