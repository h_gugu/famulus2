package com.rrsolutions.famulus;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.TextView;

import com.rrsolutions.famulus.businesslogic.HistoryController;
import com.rrsolutions.famulus.businesslogic.MainMenu;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.UserType;

public class HistoryActivity extends AppCompatActivity {

    private GridView gvHistory = null;
    private ExpandableListView elstProducts = null;
    private TextView txtWaiter = null;
    private TextView txtTotalPrice = null;
    private TextView txtPrice = null;
    private Button btnBack = null;
    private Button btnForward = null;
    private EditText edtSearch = null;
    private Context mContext = null;
    private HistoryController historyController = null;

    private boolean isOrderGridVisible = true;
    private MainMenu mainMenu;
    private boolean bShowAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        mContext = this;

        gvHistory = findViewById(R.id.gvHistory);
        elstProducts = findViewById(R.id.elstProducts);
        txtWaiter = findViewById(R.id.txtWaiter);
        txtTotalPrice = findViewById(R.id.txtTotalPrice);
        txtPrice = findViewById(R.id.txtPrice);
        btnBack = findViewById(R.id.btnBack);
        btnForward = findViewById(R.id.btnForward);
        edtSearch = findViewById(R.id.edtSearch);

        TextView emptyView = ((HistoryActivity) mContext).findViewById(R.id.txtEmpty);
        emptyView.setText(mContext.getString(R.string.grid_no_item));
        gvHistory.setEmptyView(emptyView);

        mainMenu = new MainMenu(mContext);
        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mContext.getString(R.string.order_history));
        }

        historyController = new HistoryController();
        historyController.setListener(mContext);
        new Thread(historyController).start();
    }

    public GridView getHistory() {
        return gvHistory;
    }

    public TextView getWaiter() {
        return txtWaiter;
    }

    public TextView getTotalPrice() {
        return txtTotalPrice;
    }

    public TextView getPrice() {
        return txtPrice;
    }

    public ExpandableListView getProducts() {
        return elstProducts;
    }

    public Button getBack() {
        return btnBack;
    }

    public Button getForward() {
        return btnForward;
    }

    public EditText getSearch() {
        return edtSearch;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if(isOrderGridVisible()) {
            historyController.stop();
            super.onBackPressed();
        } else {
            gvHistory.setVisibility(View.VISIBLE);
            elstProducts.setVisibility(View.GONE);
            txtTotalPrice.setVisibility(View.VISIBLE);
            txtPrice.setVisibility(View.GONE);
            btnBack.setVisibility(View.VISIBLE);
            btnForward.setVisibility(View.VISIBLE);
            isOrderGridVisible = true;
        }
    }

    @Override
    protected void onDestroy() {
        historyController.stop();
        super.onDestroy();
    }

    public boolean isOrderGridVisible() {
        return isOrderGridVisible;
    }

    public void setOrderGridVisible(boolean orderGridVisible) {
        isOrderGridVisible = orderGridVisible;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_select_all).setVisible(true);
        menu.findItem(R.id.action_printing_mode).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(Storage.get(Shared.sUserTypeKey, 0) == UserType.WAITER.ordinal());
        mainMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }

    public void showSearch() {
        edtSearch.setVisibility(View.VISIBLE);
    }

    public void showSelectCheckBox() {
        if(isShowAll()) {
            setShowAll(false);
        } else {
            setShowAll(true);
        }
        historyController.showCheckBox();
    }

    public boolean isShowAll() {
        return bShowAll;
    }

    public void setShowAll(boolean bShowAll) {
        this.bShowAll = bShowAll;
    }
}
