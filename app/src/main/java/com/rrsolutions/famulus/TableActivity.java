package com.rrsolutions.famulus;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.evernote.android.job.JobRequest;
import com.rrsolutions.famulus.businesslogic.DiscoverPrinters;
import com.rrsolutions.famulus.businesslogic.MainMenu;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.PrinterOption;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.notification.NotificationManager;
import com.rrsolutions.famulus.printer.MyNetwork;
import com.rrsolutions.famulus.schedular.OrderJob;

import java.util.HashSet;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class TableActivity extends AppCompatActivity {

    private Button btnOne = null;
    private Button btnTwo = null;
    private Button btnThree = null;
    private Button btnFour = null;
    private Button btnFive = null;
    private Button btnSix = null;
    private Button btnSeven = null;
    private Button btnEight = null;
    private Button btnNine = null;
    private Button btnZero = null;
    private RelativeLayout btnBackspace = null;
    private TextView txtTable = null;
    private ImageView imgForward = null;
    private String sTable = "";

    private boolean isActivityChanged = false;
    private Context mContext = null;
    private MainMenu mainMenu = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        mContext = this;

        btnOne = findViewById(R.id.btnOne);
        btnTwo = findViewById(R.id.btnTwo);
        btnThree = findViewById(R.id.btnThree);
        btnFour = findViewById(R.id.btnFour);
        btnFive = findViewById(R.id.btnFive);
        btnSix = findViewById(R.id.btnSix);
        btnSeven = findViewById(R.id.btnSeven);
        btnEight = findViewById(R.id.btnEight);
        btnNine = findViewById(R.id.btnNine);
        btnZero = findViewById(R.id.btnZero);
        btnBackspace = findViewById(R.id.rlBackspace);
        txtTable = findViewById(R.id.txtTableNo);
        imgForward = findViewById(R.id.imgForward);
        imgForward.setOnClickListener(imgForward_onClick);

        NotificationManager notificationManager = new NotificationManager(mContext);
        notificationManager.checkCategoriesPrinters();

        txtTable.setFilters(new InputFilter[] {new InputFilter.LengthFilter(Shared.iMaxTablesDigits)});

        Storage.init(mContext);
        sTable = Storage.get(Shared.sTableKey, "");
        SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        Shared.sWaiter = sqLiteDB.getWaiterName();

        txtTable.setText(sTable);
        mainMenu = new MainMenu(mContext);

//        final TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "A Snackbar is a lightweight material design method for providing feedback to a user, while optionally providing an action to the user.", TSnackbar.LENGTH_INDEFINITE);
//        snackbar.setActionTextColor(Color.WHITE);
//        View snackbarView = snackbar.getView();
//        snackbarView.setBackgroundColor(Color.parseColor("#CC00CC"));
//        TextView textView = snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
//        textView.setTextColor(Color.YELLOW);
//        snackbar.setAction("Undo", new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                snackbar.dismiss();
//            }
//        });
//        snackbar.show();

//        int jobId = new JobRequest.Builder(OrderJob.TAG)
//                .startNow()
//                .build()
//                .schedule();
    }

    public void Numeric_onClick(View v) {
        String text = txtTable.getText().toString();
        Button btn = null;
        if(v instanceof Button) {
            btn = (Button) v;
        }
        if(btn != null) {
            if(!btn.getText().toString().toLowerCase().equals(getString(R.string.table_btn_delete).toLowerCase())) {
//                if(btn.getText().equals("0")) {
//                    if(text.trim().length() > 0) {
//                        text += btn.getText().toString();
//                    }
//                } else {
//                    text += btn.getText().toString();
//                }
                text += btn.getText().toString();
            }
        } else {
            if(txtTable.getText().toString().length() > 0) {
                text = text.substring(0, txtTable.getText().toString().length() - 1);
            }
        }
        txtTable.setText(text);

    }

    private View.OnClickListener imgForward_onClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String text = txtTable.getText().toString().trim();
            if(text.length() > 0) {
                isActivityChanged = true;
                Storage.init(mContext);
                Storage.save(Shared.sTableKey, text);
                Intent intent = new Intent(TableActivity.this, CategoryActivity.class);
                startActivity(intent);
            } else {
                Shared.showAlert(mContext, mContext.getString(R.string.table_title),
                        getString(R.string.table_err_invalid), SweetAlertDialog.ERROR_TYPE);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName());
        if(!isActivityChanged) {
            Storage.init(mContext);
            Storage.save(Shared.sTableKey, txtTable.getText().toString().trim());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        Storage.init(mContext);
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        boolean isPrintingMode = Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal();
        if(isPrintingMode) {
            menu.findItem(R.id.action_printing_mode).setVisible(true);
            menu.findItem(R.id.action_normal_printing).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.CATEGORY.ordinal());
            menu.findItem(R.id.action_one_product_per_ticket).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.PRODUCT.ordinal());
        }
        mainMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

    }
}
