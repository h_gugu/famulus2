package com.rrsolutions.famulus;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.rrsolutions.famulus.businesslogic.LoginController;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.network.WebManagement;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {

    private Context mContext = null;
    private EditText edtUsername = null;
    private EditText edtPassword = null;
    private Button btnLogin = null;
    private TextView txtMarker = null;
    private SwitchCompat swhSaveCredentials = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;

        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        swhSaveCredentials = findViewById(R.id.swhSaveCredentials);
        txtMarker = findViewById(R.id.txtMarker);

        savedInstanceState = getIntent().getExtras();
        if(savedInstanceState != null) {
            if(savedInstanceState.getBoolean("logout")) {
                WebManagement webManagement = new WebManagement(mContext);
                webManagement.logout();
            }
        }

        LoginController loginController = new LoginController();
        loginController.setListeners(mContext);
        //Shared.determineScreenDensity(mContext);
    }

    public EditText getUsername() {
        return edtUsername;
    }

    public EditText getPassword() {
        return edtPassword;
    }

    public Button getLogin() {
        return btnLogin;
    }

    public TextView getMarker() {
        return txtMarker;
    }

    public SwitchCompat getSaveCredentials() {
        return swhSaveCredentials;
    }
}
