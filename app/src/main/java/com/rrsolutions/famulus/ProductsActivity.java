package com.rrsolutions.famulus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.GridView;

import com.rrsolutions.famulus.businesslogic.MainMenu;
import com.rrsolutions.famulus.businesslogic.ProductController;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.PrinterOption;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.modeljson.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsActivity extends AppCompatActivity {

    private Bundle bundle = new Bundle();
    private Context mContext = null;

    private int iCateId = 0;
    private String sCatName = "";

    private GridView gvProducts = null;
    private Button btnBack = null;
    private Button btnForward = null;

    private List<Product> products = new ArrayList<>();

    private ProductController productController = new ProductController();
    private boolean isActivityChanged = false;
    private MainMenu mainMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        mContext = this;
        gvProducts = findViewById(R.id.gvProducts);
        btnBack = findViewById(R.id.btnBack);
        btnForward = findViewById(R.id.btnForward);

        if(getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
        }

        mainMenu = new MainMenu(mContext);

        iCateId = bundle.getInt("catid");
        sCatName = bundle.getString("catname");
        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(sCatName);
        }
        productController.setListeners(mContext);
    }

    public int getCatId() {
        return iCateId;
    }

    public String getCatName() {
        return sCatName;
    }

    public GridView getProductGrid() {
        return gvProducts;
    }

    public Button getBack() {
        return btnBack;
    }

    public Button getForward() {
        return btnForward;
    }

    public List<Product> getProducts() {
        return products;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName());
        Shared.saveLastOpenedActivity(mContext, getClass().getCanonicalName() , iCateId + "|" + sCatName);
        if(!isActivityChanged()) {
            productController.saveSession();
        }
    }

    @Override
    public void onBackPressed() {
        setActivityChanged(true);
        productController.saveSession();
        Intent intent = new Intent(mContext, CategoryActivity.class);
        mContext.startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        menu.findItem(R.id.action_refresh).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);
        boolean isPrintingMode = Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal();
        if(isPrintingMode) {
            menu.findItem(R.id.action_printing_mode).setVisible(true);
            menu.findItem(R.id.action_normal_printing).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.CATEGORY.ordinal());
            menu.findItem(R.id.action_one_product_per_ticket).setChecked(Storage.get(Shared.sUserTypeKey, 0) == PrinterOption.PRODUCT.ordinal());
        }
        mainMenu.setMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        mainMenu.handleMainMenu(item);
        return super.onOptionsItemSelected(item);
    }

    public boolean isActivityChanged() {
        return isActivityChanged;
    }

    public void setActivityChanged(boolean activityChanged) {
        isActivityChanged = activityChanged;
    }



}
