package com.rrsolutions.famulus.enumeration;

/**
 * Created by hassanmushtaq on 18/01/2018.
 */

public enum SyncStatus {
    NOT_SYNC,
    PENDING,
    SYNC
}
