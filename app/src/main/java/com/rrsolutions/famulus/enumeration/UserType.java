package com.rrsolutions.famulus.enumeration;

/**
 * Created by hassanmushtaq on 03/01/2018.
 */

public enum UserType {
    WAITER,
    CASHIER
}
