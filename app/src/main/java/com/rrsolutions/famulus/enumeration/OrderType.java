package com.rrsolutions.famulus.enumeration;

/**
 * Created by hassanmushtaq on 03/01/2018.
 */

public enum OrderType {
    RECEIPT,
    HISTORY,
    PRODUCT_PRICES,
    TEST_PRINT
}
