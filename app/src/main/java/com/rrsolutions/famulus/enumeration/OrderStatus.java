package com.rrsolutions.famulus.enumeration;

/**
 * Created by hassanmushtaq on 03/01/2018.
 */

public enum OrderStatus {
    NON_PRINTED,
    PENDING,
    PRINTED
}
