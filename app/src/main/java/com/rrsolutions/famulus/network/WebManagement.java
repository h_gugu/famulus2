package com.rrsolutions.famulus.network;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.rrsolutions.famulus.CategoryActivity;
import com.rrsolutions.famulus.LoginActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.TableActivity;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.modeljson.Category;
import com.rrsolutions.famulus.modeljson.Device;
import com.rrsolutions.famulus.modeljson.EventInfo;
import com.rrsolutions.famulus.modeljson.Printer;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 05/01/2018.
 */

public class WebManagement {

    private Context mContext = null;
    private SweetAlertDialog pDialog = null;
    private String sessionKey = "";

    public  WebManagement(Context context) {
        mContext = context;
    }

    public void login(final String username, final String password) {
        pDialog = new SweetAlertDialog(mContext, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        pDialog.setTitleText(mContext.getString(R.string.login_auth));
        pDialog.setCancelable(false);
        ((LoginActivity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pDialog.show();
            }
        });
        String url = Shared.sLoginURL;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            if (jsonResponse.has(Shared.sSessionKey)) {
                                Storage.init(mContext);
                                sessionKey = jsonResponse.getString(Shared.sSessionKey);
                                Storage.save(Shared.sSessionKey, sessionKey);
                                pDialog.setTitleText(mContext.getString(R.string.login_fetch_data));
                                System.out.println("Site: " + sessionKey);
                                fetchAllCategoriesProducts();
                            } else {
                                if (jsonResponse.has("reason")) {
                                    String reason = jsonResponse.getString("reason");
                                    Shared.showAlert(mContext, mContext.getString(R.string.login_title), reason, SweetAlertDialog.ERROR_TYPE);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismissWithAnimation();
                        Shared.showAlert(mContext, mContext.getString(R.string.login_auth_failed), mContext.getString(R.string.login_err_wrong_user_pass), SweetAlertDialog.ERROR_TYPE);
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        Volley.newRequestQueue(mContext).add(postRequest);
    }

    private void fetchAllCategoriesProducts() {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        String url = Shared.sCategoriesProductURL;
        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        try {
                            Storage.init(mContext);
                            SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
                            sqLiteDB.deleteLogin();
                            sqLiteDB.deleteCategory();
                            sqLiteDB.deleteDevice();
                            sqLiteDB.deleteEventInfo();
                            sqLiteDB.deletePrinter();
                            JsonObject o = new JsonParser().parse(response).getAsJsonObject();
                            JsonObject jsonObject1 = (JsonObject) o.get("device");
                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<Device>(){}.getType();
                            Device device = gson.fromJson(jsonObject1.toString(), collectionType);
                            Storage.save(Shared.sUserTypeKey, device.getOperation_mode());
                            sqLiteDB.insert(device);
                            JsonArray jsonObject2 = (JsonArray) o.get("printers");
                            Type collectionType2 = new TypeToken<List<Printer>>(){}.getType();
                            List<Printer> printers = gson.fromJson(jsonObject2.toString(), collectionType2);
                            sqLiteDB.insert(printers);
                            JsonObject jsonObject3 = (JsonObject) o.get("event_info");
                            Type collectionType3 = new TypeToken<EventInfo>(){}.getType();
                            EventInfo eventInfo = gson.fromJson(jsonObject3.toString(), collectionType3);
                            sqLiteDB.insert(eventInfo);
                            JsonObject jsonObject4 = (JsonObject) o.get("event_data");
                            JsonArray jsonArray = jsonObject4.getAsJsonArray("data");
                            Type collectionType4 = new TypeToken<List<Category>>(){}.getType();
                            List<Category> categories = gson.fromJson(jsonArray.toString(), collectionType4);
                            sqLiteDB.insertCats(categories);
                            pDialog.dismissWithAnimation();
                            if(device.getOperation_mode() == UserType.CASHIER.ordinal()) {
                                Storage.save(Shared.sTableKey, "");
                                Intent intent = new Intent(mContext, CategoryActivity.class);
                                mContext.startActivity(intent);
                            } else {
                                Intent intent = new Intent(mContext, TableActivity.class);
                                mContext.startActivity(intent);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.d("ERROR","error => "+error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("X-Access-Token", sessionKey);
                return params;
            }
        };
        queue.add(postRequest);

    }

    public void logout() {
        RequestQueue queue = Volley.newRequestQueue(mContext);
        Storage.init(mContext);
        final SQLiteDB sqLiteDB = SQLiteDB.getInstance(mContext);
        final String sessionKey = Storage.get(Shared.sSessionKey, "");
        String url = Shared.sLogoutURL;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub
                        Log.d("ERROR","error => "+error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("X-Access-Token", sessionKey);
                return params;
            }
        };
        queue.add(postRequest);

    }


}
