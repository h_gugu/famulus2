package com.rrsolutions.famulus.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.MultiProductsActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.datawrapper.ProductData;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.keyboard.UIUtil;
import com.rrsolutions.famulus.modeljson.Option;
import com.rrsolutions.famulus.modeljson.Product;

import java.util.Hashtable;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 07/12/2017.
 */

public class MultiProduct extends Fragment {

    private TextView txtProduct = null;
    private TextView txtExtraInfo = null;
    private TextView txtMoreOption = null;
    private LinearLayout llOption = null;
    private ElegantNumberButton ebtnQuantity = null;
    private MultiProductsActivity objMultiProductsActivity = null;
    private ProductData productData = null;

    private Hashtable<Integer, String> hashtable = new Hashtable<>();
    private List<Option> options= null;

    private String product_data = "";
    private String restore_product = "";

    // catid | catname | productid | productname | extrainfo | price | quantity | desc | optids (separated by comma)

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.multi_product_item, container, false);
        setRetainInstance(true);

        txtProduct = view.findViewById(R.id.txtProduct);
        txtExtraInfo = view.findViewById(R.id.txtProductExtraInfo);
        txtMoreOption = view.findViewById(R.id.txtMoreOption);
        ebtnQuantity = view.findViewById(R.id.ebtnQuantity);
        llOption = view.findViewById(R.id.llOptions);
        TextView textView = ebtnQuantity.findViewById(com.cepheuen.elegantnumberbutton.R.id.number_counter);
        textView.setTag(ebtnQuantity);
        savedInstanceState = getArguments();
        if(savedInstanceState != null) {
            productData = savedInstanceState.getParcelable("product");
            txtProduct.setText(productData.getName());
            txtExtraInfo.setText(productData.getExtra_info());
        }

        SQLiteDB sqLiteDB = SQLiteDB.getInstance(getActivity());
        options = sqLiteDB.selectAllOptions(productData.getId());
        restoreSession();
        txtMoreOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionDialog();
            }
        });

        llOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionDialog();
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int quantity = Integer.parseInt(((ElegantNumberButton) v.getTag()).getNumber());
                showQuantityDialog(getContext(), ((ElegantNumberButton) v.getTag()), productData.getName(), quantity);
            }
        });

        Shared.changeButton(getActivity(), ebtnQuantity);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof MultiProductsActivity) objMultiProductsActivity = (MultiProductsActivity) context;
    }

    public String getData() {
        if(Integer.parseInt(ebtnQuantity.getNumber()) > 0) {
            product_data = productData.getCat_id() + "|" + productData.getCat_name() + "|" + productData.getId()
                    + "|" + productData.getName() + "|" + productData.getExtra_info() + "|" + productData.getPrice()
                    + "|" + ebtnQuantity.getNumber();
            //product_data = ebtnQuantity.getNumber();
            if(hashtable.size() > 0) {
                if(hashtable.containsKey(0)) {
                    product_data += "|d" + hashtable.get(0);
                }
                String opt = "";
                for(Integer key : hashtable.keySet()) {
                    if(key != 0) {
                        opt += "," + key;
                    }
                }
                if(opt.length() > 0) {
                    opt = opt.substring(1);
                    product_data += "|o" + opt;
                }
            }
            return product_data;
        }
        return "";
    }

    public void restoreSession() {
        if(restore_product.length() > 0) {
            String string = restore_product.substring(0, restore_product.lastIndexOf("|"));
            String[] data = string.split("\\|");
            if(data.length > 0) {
                ebtnQuantity.setNumber(data[6]);
            }
            if(data.length > 7) {
                for(int i = 7; i < data.length; i++) {
                    if(data[i].startsWith("d")) {
                        hashtable.put(0, data[i].substring(1));
                    }
                    else if(data[i].startsWith("o")) {
                        saveOptions(data[i].substring(1));
                    }
                }
                saveOptions();
            }
        }
    }

    public void saveOptions(String opt) {
        if(opt.length() > 0) {
            String[] data = opt.split(",");
            if(data.length > 0) {
                for(String s : data) {
                    String name = getOptionName(Integer.parseInt(s.trim()));
                    hashtable.put(Integer.parseInt(s.trim()), name);
                }

            }
        }
    }

    public String getOptionName(int id) {
        for(Option option : options) {
            if(option.getId() == id) return option.getName();
        }
        return "";
    }

    public void saveOptions() {
        if(hashtable.size() > 0) {
            txtMoreOption.setVisibility(View.GONE);
            llOption.setVisibility(View.VISIBLE);
            if(hashtable.containsKey(0)) {
                String string = hashtable.get(0);
                final TextView txtOption = new TextView(getActivity());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                txtOption.setSingleLine();
                txtOption.setClickable(false);
                txtOption.setFocusable(false);
                txtOption.setTextColor(ContextCompat.getColor(getActivity(), R.color.defaultColor));
                txtOption.setText( " - " + string);
                txtOption.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.default_small_font_size));
                //params.setMargins(0, (int) getResources().getDimension(R.dimen.layout_margin), 0, (int) getResources().getDimension(R.dimen.layout_margin));
                llOption.addView(txtOption, params);
            }
            for(Integer key : hashtable.keySet()) {
                if(key != 0) {
                    String string = hashtable.get(key);
                    final TextView txtOption = new TextView(getActivity());
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    txtOption.setSingleLine();
                    txtOption.setClickable(false);
                    txtOption.setFocusable(false);
                    txtOption.setTextColor(ContextCompat.getColor(getActivity(), R.color.defaultColor));
                    txtOption.setText( " - " + string);
                    txtOption.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.default_small_font_size));
                    //params.setMargins(0, (int) getResources().getDimension(R.dimen.layout_margin), 0, (int) getResources().getDimension(R.dimen.layout_margin));
                    llOption.addView(txtOption, params);
                }
            }
        }
    }

    public void showOptionDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View grid = inflater.inflate(R.layout.option_item, null);
        final LinearLayout llOptions = grid.findViewById(R.id.llOptions);

        final EditText edtDesc = grid.findViewById(R.id.edtDesc);
        edtDesc.setHorizontallyScrolling(false);
        edtDesc.setMaxLines(Integer.MAX_VALUE);
        edtDesc.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edtDesc.setRawInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
        if(hashtable.containsKey(0)) {
            edtDesc.setGravity(Gravity.TOP);
            edtDesc.setText(hashtable.get(0));
            if(edtDesc.getText().length() > 0) {
                edtDesc.post(new Runnable() {
                    @Override
                    public void run() {
                        edtDesc.setSelection(edtDesc.getText().length());
                    }
                });
            }
        }
        edtDesc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    edtDesc.setGravity(Gravity.TOP);
                } else {
                    edtDesc.setGravity(Gravity.CENTER);
                }
            }
        });
        if(options.size() > 0) {
            for(Option option : options) {
                final TextView txtOption = new TextView(getActivity());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                txtOption.setSingleLine();
                txtOption.setTextColor(ContextCompat.getColor(getActivity(), R.color.defaultColor));
                txtOption.setText(option.getName());
                if(hashtable.contains(option.getName())) {
                    txtOption.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check, 0);
                    option.setSelected(true);
                }
                txtOption.setTag(option);
                txtOption.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.default_font_size));
                params.setMargins(0, (int) getResources().getDimension(R.dimen.category_element_margin),
                        0, (int) getResources().getDimension(R.dimen.category_element_margin));
                txtOption.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Option option1 = (Option) v.getTag();
                        if(!option1.isSelected()) {
                            txtOption.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.check, 0);
                            option1.setSelected(true);
                        } else {
                            txtOption.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                            option1.setSelected(false);
                        }
                        txtOption.setTag(option1);
                    }
                });
                llOptions.addView(txtOption, params);
            }
        }

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setTitleText(productData.getName())
                .setConfirmText(getString(R.string.ok))
                .setCancelText(getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        hashtable.clear();
                        if(edtDesc.getText().toString().trim().length() > 0) {
                            hashtable.put(0, edtDesc.getText().toString().trim());
                        }
                        for(int i = 0; i < llOptions.getChildCount(); i++) {
                            TextView txt = (TextView) llOptions.getChildAt(i);
                            Option option = (Option) txt.getTag();
                            if(option.isSelected()) {
                                hashtable.put(option.getId(), option.getName());
                            }
                        }
                        llOption.removeAllViews();
                        if(hashtable.size() == 0) {
                            llOption.setVisibility(View.GONE);
                            txtMoreOption.setVisibility(View.VISIBLE);
                        }
                        saveOptions();
                        if(hashtable.size() > 0) {
                            int q = Integer.parseInt(ebtnQuantity.getNumber());
                            if(q == 0) {
                                ebtnQuantity.setNumber("1");
                            }
                        }
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
        //Button btn = sweetAlertDialog.findViewById(R.id.confirm_button);
        //btn.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        edtDesc.requestFocus();
        edtDesc.post(new Runnable() {
            @Override
            public void run() {
                UIUtil.showKeyboard(getContext(), edtDesc);
            }
        });
    }

    public String getRestore_product() {
        return restore_product;
    }

    public void setRestore_product(String restore_product) {
        this.restore_product = restore_product;
    }

    public void showQuantityDialog(final Context context, final ElegantNumberButton button, String title, int quantity) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_quantity, null);
        final EditText edtQuantity = grid.findViewById(R.id.edtQuantity);
        if(quantity > 0) {
            edtQuantity.setText(String.valueOf(quantity));
            edtQuantity.post(new Runnable() {
                @Override
                public void run() {
                    edtQuantity.setSelection(edtQuantity.getText().length());
                }
            });
        }
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText(title)
                .setConfirmText(context.getString(R.string.ok))
                .setCancelText(context.getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        if(edtQuantity.getText().toString().trim().length() > 0) {
                            int q = Integer.parseInt(edtQuantity.getText().toString());
                            button.setNumber(String.valueOf(q));
                        }
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
        edtQuantity.requestFocus();
        edtQuantity.post(new Runnable() {
            @Override
            public void run() {
                UIUtil.showKeyboard(context, edtQuantity);
            }
        });

    }

}
