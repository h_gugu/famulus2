package com.rrsolutions.famulus.datawrapper;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by hassanmushtaq on 13/12/2017.
 */

public class ProductData implements Parcelable {

    private int id = 0;
    private int cat_id = 0;
    private String cat_name = "";
    private String name = "";
    private String extra_info = "";
    private double price = 0.0;
    //private boolean enabled = false;
    //private boolean deleted = false;
    //private int quantity = 0;
    //private boolean selected = false;

    //constructor used for parcel
    public ProductData(Parcel parcel){
        //read and set saved values from parcel
        id = parcel.readInt();
        cat_id = parcel.readInt();
        cat_name = parcel.readString();
        name = parcel.readString();
        extra_info = parcel.readString();
        price = parcel.readDouble();
//        enabled = parcel.readInt() == 1;
//        deleted = parcel.readInt() == 1;
//        quantity = parcel.readInt();
//        selected = parcel.readInt() == 1;
    }



    public ProductData(int id, int cat_id, String cat_name, String name, String extra_info, double price) {
        this.id = id;
        this.cat_id = cat_id;
        this.cat_name = cat_name;
        this.name = name;
        this.extra_info = extra_info;
        this.price = price;
//        this.enabled = enabled;
//        this.deleted = deleted;
//        this.quantity = quantity;
//        this.selected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(cat_id);
        dest.writeString(cat_name);
        dest.writeString(name);
        dest.writeString(extra_info);
        dest.writeDouble(price);
//        dest.writeInt(enabled ? 1 : 0);
//        dest.writeInt(deleted ? 1 : 0);
//        dest.writeInt(quantity);
//        dest.writeInt(selected ? 1 : 0);
    }

    //creator - used when un-parceling our parcle (creating the object)
    public static final Parcelable.Creator<ProductData> CREATOR = new Parcelable.Creator<ProductData>(){

        @Override
        public ProductData createFromParcel(Parcel parcel) {
            return new ProductData(parcel);
        }

        @Override
        public ProductData[] newArray(int size) {
            return new ProductData[0];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

//    public boolean isEnabled() {
//        return enabled;
//    }
//
//    public void setEnabled(boolean enabled) {
//        this.enabled = enabled;
//    }
//
//    public boolean isDeleted() {
//        return deleted;
//    }
//
//    public void setDeleted(boolean deleted) {
//        this.deleted = deleted;
//    }
//
//    public int getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }
//
//    public boolean isSelected() {
//        return selected;
//    }
//
//    public void setSelected(boolean selected) {
//        this.selected = selected;
//    }
}
