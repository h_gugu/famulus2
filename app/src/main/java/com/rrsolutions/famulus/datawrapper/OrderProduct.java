package com.rrsolutions.famulus.datawrapper;

import java.util.ArrayList;

/**
 * Created by hassanmushtaq on 14/12/2017.
 */

public class OrderProduct {

    private int id = 0;
    private int type = 0;
    private int cat_id = 0;
    private String name = "";
    private String extra_info = "";
    private double price = 0.0;
    private int quantity = 0;
    private boolean showquantity = true;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isShowquantity() {
        return showquantity;
    }

    public void setShowquantity(boolean showquantity) {
        this.showquantity = showquantity;
    }
}
