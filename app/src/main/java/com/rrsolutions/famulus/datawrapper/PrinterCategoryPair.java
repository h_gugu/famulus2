package com.rrsolutions.famulus.datawrapper;

/**
 * Created by hassanmushtaq on 16/12/2017.
 */

public class PrinterCategoryPair {

    private int printid = 0;
    private int categoryid = 0;

    public PrinterCategoryPair(int printid, int categoryid) {
        this.printid = printid;
        this.setCategoryid(categoryid);
    }

    public int getPrintid() {
        return printid;
    }

    public void setPrintid(int printid) {
        this.printid = printid;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }
}
