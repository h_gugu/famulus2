package com.rrsolutions.famulus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.datawrapper.ProductData;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.enumeration.UserType;
import com.rrsolutions.famulus.modeljson.Category;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Shared.setDefaultLocale(this);

        startService(new Intent(getBaseContext(), AppClosedService.class));
        //List<String> list = Storage.getAll();
        //Shared.copyDbToExternalStorage(this, "famulus.db");
        //SQLiteDB sqLiteDB = SQLiteDB.getInstance(this);
        //sqLiteDB.updateOrder(0);
        //sqLiteDB.updatePrintedCategory();

        try {
            if(isAlreadyLogin()) {
                String activity = Storage.get(Shared.sLastOpenedActivityKey, "");
                Class c = null;
                c = Class.forName(activity);
                if (activity.contains("TableActivity")) {
                    MovetoActivity(c, null);
                } else if (activity.contains("CategoryActivity")) {
                    MovetoActivity(c, null);
                } else if (activity.contains("OrderActivity")) {
                    MovetoActivity(c, null);
                } else if (activity.contains("MultiProductsActivity")) {
                    String data = Storage.get(Shared.sLastOpenedActivityDataKey, "");
                    Bundle bundle = new Bundle();
                    if(data.trim().length() > 0) {
                        String[] activityData = data.split("\\|");
                        if(activityData.length > 0) {
                            bundle.putString("catname", activityData[0]);
                            ProductData productData = new Gson().fromJson(activityData[1], ProductData.class);
                            bundle.putParcelable("product", productData);
                        }
                    }
                    MovetoActivity(c, bundle);
                } else if (activity.contains("ProductsActivity")) {
                    String data = Storage.get(Shared.sLastOpenedActivityDataKey, "");
                    Bundle bundle = new Bundle();
                    if(data.trim().length() > 0) {
                        String[] activityData = data.split("\\|");
                        if(activityData.length > 0) {
                            bundle.putInt("catid", Integer.parseInt(activityData[0]));
                            bundle.putString("catname", activityData[1]);
                        }
                    }
                    MovetoActivity(c, bundle);
                } else {
                    MovetoActivity(TableActivity.class, null);
                }
            } else {
                MovetoActivity(LoginActivity.class, null);
                //MovetoActivity(TableActivity.class, null);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            MovetoActivity(TableActivity.class, null);
        }
    }

    private boolean isAlreadyLogin() {
        Storage.init(this);
        String sessionKey = Storage.get("session_key", "");
        return (sessionKey.length() > 1);
    }

    private void MovetoActivity(Class c, Bundle bundle) {
        Storage.init(this);
        if(Storage.get(Shared.sUserTypeKey, 0) == UserType.CASHIER.ordinal()) {
            if(c == TableActivity.class) c = CategoryActivity.class;
        }
        Intent intent = new Intent(this, c);
        if(bundle != null) intent.putExtras(bundle);
        startActivity(intent);
    }

}
