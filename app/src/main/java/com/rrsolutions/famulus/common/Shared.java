package com.rrsolutions.famulus.common;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.service.voice.VoiceInteractionService;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.InputType;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.rrsolutions.famulus.LoginActivity;
import com.rrsolutions.famulus.R;
import com.rrsolutions.famulus.database.SQLiteDB;
import com.rrsolutions.famulus.database.Storage;
import com.rrsolutions.famulus.datawrapper.OrderProduct;
import com.rrsolutions.famulus.modeljson.Option;
import com.rrsolutions.famulus.modeljson.Product;
import com.rrsolutions.famulus.network.WebManagement;
import com.rrsolutions.famulus.printer.ConnectedPrinter;
import com.rrsolutions.famulus.printer.MyPrinter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.DoubleSummaryStatistics;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by hassanmushtaq on 05/12/2017.
 */

public class Shared {

    public static String sLoginURL = "http://37.187.42.116:2995/api/app/device/login";
    public static String sLogoutURL = "http://37.187.42.116:2995/api/app/device/logout";
    public static String sCategoriesProductURL = "http://37.187.42.116:2995/api/app/activity/event";

    public static final int iMaxTablesDigits = 10;

    public static final String sStorageName = "famulus";
    public static final String sLastOpenedActivityKey = "activityKey";
    public static final String sLastOpenedActivityDataKey = "activityDataKey";
    public static final String sTableKey = "table";
    public static final String sPendingOrderKey = "pending_order";
    public static final String sBackgroundWorkerKey = "background_worker";
    public static final String sBackgroundWorkerRunatKey = "background_worker_time";
    public static final String sLanguageKey = "app_lang";
    public static final String sSessionKey = "session_key";
    public static final String sUserTypeKey = "user_type_key";
    public static final String sPrintingOptionKey = "printing_option_key";
    public static final String sJobIDKey = "jobid_key";

    public static SimpleDateFormat sdfUTCDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat sdfReceiptDateTime = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");

    public static String sDateTimeFormat_History = "EEE d MMM HH:mm";

    public static List<String> connectedPrinters = new ArrayList<>();
    public static Hashtable<String, ConnectedPrinter> connectedPrintersDetails = new Hashtable<>();
    public static Hashtable<String, MyPrinter> hashtablePrinting = new Hashtable<>();

    public static String sWaiter = "";
    public static String sDefaultLangLocale = "en_GB";
    public static boolean isAppClose = false;

    private static long g_BeginDateMilliSecs2 = 1373846400000L;


//    public static String[] Languages = {"English (US)", "Deutsch", "Italiano", "日本人", "Русский", "Español", "Português",
//            "简体中文", "Romania", "Ukraine", "Bahasa Indonesia", "Svenska", "Polski", "Français", "Norsk", "Dansk", "Dutch"};

//    public static int Flags[] = {com.cry.cryptobase.R.drawable.united_states, com.cry.cryptobase.R.drawable.germany, com.cry.cryptobase.R.drawable.italy, com.cry.cryptobase.R.drawable.japan, com.cry.cryptobase.R.drawable.russia, com.cry.cryptobase.R.drawable.spain,
//            com.cry.cryptobase.R.drawable.portugal, com.cry.cryptobase.R.drawable.china, com.cry.cryptobase.R.drawable.romania, com.cry.cryptobase.R.drawable.ukraine, com.cry.cryptobase.R.drawable.indonesia, com.cry.cryptobase.R.drawable.sweden,
//            com.cry.cryptobase.R.drawable.poland, com.cry.cryptobase.R.drawable.france, com.cry.cryptobase.R.drawable.norway, com.cry.cryptobase.R.drawable.denmark, R.drawable.netherlands};
//
//    public static String[] Locales = {"en-US", "de-DE", "it-IT", "ja-JP", "ru-RU", "es-ES", "pt-PT", "zh-CN", "ro-RO", "uk-UA", "in-ID",
//            "sv-SE", "pl-PL", "fr-FR", "no-NO", "da-DK", "nl-NL"};
//
//
//    public static String[] sMonth = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
//    public static String[] sShortMonth = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
//    public static String[] sWeek = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
//

    public static String[] Languages = { "Deutsch", "Italiano", "English" };

    public static String[] Locales = {"de-DE", "it-IT", "en-GB"};
    public static boolean isKeyboardShowing = false;

//    public static int Flags[] = {com.cry.cryptobase.R.drawable.united_states, com.cry.cryptobase.R.drawable.germany, com.cry.cryptobase.R.drawable.italy, com.cry.cryptobase.R.drawable.japan, com.cry.cryptobase.R.drawable.russia, com.cry.cryptobase.R.drawable.spain,
//            com.cry.cryptobase.R.drawable.portugal, com.cry.cryptobase.R.drawable.china, com.cry.cryptobase.R.drawable.romania, com.cry.cryptobase.R.drawable.ukraine, com.cry.cryptobase.R.drawable.indonesia, com.cry.cryptobase.R.drawable.sweden,
//            com.cry.cryptobase.R.drawable.poland, com.cry.cryptobase.R.drawable.france, com.cry.cryptobase.R.drawable.norway, com.cry.cryptobase.R.drawable.denmark, R.drawable.netherlands};



    public static void TogglePasswordField(EditText editText, boolean status) {
        if(status) {
            editText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            //editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            editText.setInputType(129);
        }
    }

    public static Spanned fromHtml(String text) {
        if(Build.VERSION.SDK_INT > 23) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }

    public static void showAlert(Context mContext, String title, String message, int type) {
        final SweetAlertDialog alertDialog = new SweetAlertDialog(mContext, type);
        alertDialog.setTitleText(title);
        alertDialog.setContentText(message);
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                TextView text = alertDialog.findViewById(R.id.content_text);
                text.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                text.setMinLines(4);
            }
        });
        alertDialog.show();
        //Button btn = alertDialog.findViewById(R.id.confirm_button);
        //btn.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
    }

    public static void showSnackBar(final Activity activity, String text, int duration, String button, final Class _class) {
        /*
        duration = -2 -> TSnackbar.LENGTH_INDEFINITE
        duration = -1 -> TSnackbar.LENGTH_SHORT
        duration = 0 -> TSnackbar.LENGTH_LONG
        or
        duration in secs
         */
        final TSnackbar snackbar = TSnackbar.make(activity.findViewById(android.R.id.content), text, duration);
        snackbar.setActionTextColor(Color.WHITE);
        View snackbarView = snackbar.getView();
        //snackbarView.setBackgroundColor(Color.parseColor("#CC00CC"));
        TextView textView = snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setMaxLines(5);
        //textView.setTextColor(Color.YELLOW);
        if(button.trim().length() > 0) {
            snackbar.setAction(button, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
        }
        if(_class != null) {
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, _class);
                    activity.startActivity(intent);
                }
            });
        }

        snackbar.show();
    }

    public static void saveLastOpenedActivity(Context context, String activityName) {
        Storage.init(context);
        Storage.save(Shared.sLastOpenedActivityKey, activityName);
    }

    public static void saveLastOpenedActivity(Context context, String activityName, String data) {
        Storage.init(context);
        Storage.save(Shared.sLastOpenedActivityKey, activityName);
        Storage.save(Shared.sLastOpenedActivityDataKey, data);
    }

    public static boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
            return true;
        } catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
        return false;
    }

    public static void clearSession(Context context) {
        Storage.init(context);
        List<String> list = Storage.getAll();
        for(String key : list) {
            if(key.startsWith("cat")) Storage.remove(key);
            if(key.startsWith("pro")) Storage.remove(key);
            if(key.startsWith("proopt")) Storage.remove(key);
            if(key.startsWith("printorder")) Storage.remove(key);
            if(key.startsWith("printing")) Storage.remove(key);
        }
        Storage.remove(sTableKey);
        Storage.remove(sLastOpenedActivityKey);
        Storage.remove(sLastOpenedActivityDataKey);
    }

    public static boolean hasProduct(Context context) {
        Storage.init(context);
        Map<String, Set<String>> list = Storage.getAllProducts();
        if(list.size() > 0) return true;
//        Map<String, String> list2 = Storage.getAll(3);
//        if(list2.size() > 0) return true;
        return false;
    }

    public static boolean isPrinterAvailable(String mac) {
        MyPrinter myPrinter = Shared.hashtablePrinting.get(mac);
        if(myPrinter != null) {
            if(myPrinter.isRunning()) {
                return false;
            }
        }
        return true;
    }

    public static void determineScreenDensity(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int density = metrics.densityDpi;

        if (density==DisplayMetrics.DENSITY_XXXHIGH) {
            Toast.makeText(context, "DENSITY_XXXHIGH: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
        else if (density==DisplayMetrics.DENSITY_XXHIGH) {
            Toast.makeText(context, "DENSITY_XXHIGH: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
        else if (density==DisplayMetrics.DENSITY_XHIGH) {
            Toast.makeText(context, "DENSITY_XHIGH: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
        else if (density==DisplayMetrics.DENSITY_HIGH) {
            Toast.makeText(context, "DENSITY_HIGH: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
        else if (density==DisplayMetrics.DENSITY_MEDIUM) {
            Toast.makeText(context, "DENSITY_MEDIUM: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
        else if (density==DisplayMetrics.DENSITY_LOW) {
            Toast.makeText(context, "DENSITY_LOW:" + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, "Density is neither HIGH, MEDIUM OR LOW: " + String.valueOf(density),  Toast.LENGTH_LONG).show();
        }
    }

    public static int getScreenDimension(Context context, boolean getWidth) {
        int dimension = 0;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if(wm != null) {
            Display display = wm.getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            dimension = height;
            if(getWidth) dimension = width;
        }
        return dimension;
    }

    public static long makeOrderId() {
        Calendar calendar2 = new GregorianCalendar();
//		int zoneOffset = calendar2.get(java.util.Calendar.ZONE_OFFSET);
//		int dstOffset = calendar2.get(java.util.Calendar.DST_OFFSET);
//		calendar2.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        return (calendar2.getTime().getTime() - g_BeginDateMilliSecs2) / 100;
    }

    public static void showPINDialog(final Context context, String title, final Class c, final boolean isPin1) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View grid = inflater.inflate(R.layout.dialog_quantity, null);
        final TextView txtPIN = grid.findViewById(R.id.txtQuantity);
        final EditText edtPIN = grid.findViewById(R.id.edtQuantity);
        edtPIN.setHint("");
        if(isPin1) txtPIN.setText(context.getString(R.string.pin_level1_dialog_enter));
        else txtPIN.setText(context.getString(R.string.pin_level2_dialog_enter));
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setTitleText(title)
                .setConfirmText(context.getString(R.string.ok))
                .setCancelText(context.getString(R.string.cancel))
                .setCustomView(grid)
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        if(edtPIN.getText().toString().trim().length() > 0) {
                            SQLiteDB sqLiteDB = SQLiteDB.getInstance(context);
                            if(sqLiteDB.isPINValid(edtPIN.getText().toString(), isPin1 ? 1: 2)) {
                                sweetAlertDialog.dismissWithAnimation();
                                if(c == null) {
                                    try {
                                        sqLiteDB.deleteLogin();
                                        sqLiteDB.deleteCategory();
                                        sqLiteDB.deleteProduct();
                                        sqLiteDB.deleteOption();
                                        sqLiteDB.deleteDevice();
                                        sqLiteDB.deleteEventInfo();
                                        sqLiteDB.deletePrinter();
                                        Shared.clearSession(context);
                                        Storage.init(context);
                                        Storage.remove(Shared.sSessionKey);
                                        Intent intent = new Intent(context, LoginActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean("logout", true);
                                        intent.putExtras(bundle);
                                        context.startActivity(intent);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    Intent intent = new Intent(context, c);
                                    context.startActivity(intent);
                                }
                            }
                        } else {
                            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Enter PIN")
                                    .show();
                        }
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();

    }

    /**
     * Copy the local DB file of an application to the root of external storage directory
     * @param context the Context of application
     * @param dbName The name of the DB
     */
    public static void copyDbToExternalStorage(Context context , String dbName){

        try {
            File name = context.getDatabasePath(dbName);
            File dir = new File(Environment.getExternalStorageDirectory() + "/Download/Famulus/");
            dir.mkdirs();
            File sdcardFile = new File(Environment.getExternalStorageDirectory() , "/Download/Famulus/famulus.db");//The name of output file
            sdcardFile.createNewFile();
            InputStream inputStream = null;
            OutputStream outputStream = null;
            inputStream = new FileInputStream(name);
            outputStream = new FileOutputStream(sdcardFile);
            byte[] buffer = new byte[1024];
            int read;
            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            inputStream.close();
            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e) {
            Log.e("Exception" , e.toString());
        }
    }

    public static boolean isPrinterOnline(String mac) {
        return connectedPrinters.contains(mac);
    }

    public static String repeat(String str, int count) {
        if(str == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder(str.length() * count);
        for(int i = 0; i < count; i++) {
            sb.append(str);
        }
        return sb.toString();
    }

    public static void changeButton(Context context, ElegantNumberButton ebtnQuantity) {
        Button button1 = ebtnQuantity.findViewById(com.cepheuen.elegantnumberbutton.R.id.subtract_btn);
        Button button2 = ebtnQuantity.findViewById(com.cepheuen.elegantnumberbutton.R.id.add_btn);
        TextView textView = ebtnQuantity.findViewById(com.cepheuen.elegantnumberbutton.R.id.number_counter);
        button1.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.default_font_size));
        button2.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.default_font_size));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.default_font_size));
    }

    public static void setDefaultLocale(Context context) {
        Storage.init(context);
        String language = Storage.get(Shared.sLanguageKey, "");
        if(language.trim().equalsIgnoreCase("")) {
            Locale locale = Locale.getDefault();
            boolean isSave = false;
            for(String l : Shared.Locales) {
                if(l.startsWith(locale.getLanguage())) {
                    Storage.save(Shared.sLanguageKey, l);
                    isSave = true;
                    break;
                }
            }
            if(!isSave) {
                Storage.save(Shared.sLanguageKey, sDefaultLangLocale);
            }
        }
    }

    public static Locale getLocale(Context context) {
        Storage.init(context);
        String language_code = Storage.get(Shared.sLanguageKey, "");
        if(language_code.equalsIgnoreCase("")) {
            setDefaultLocale(context);
        }
        String[] codes = language_code.split("-");
        return new Locale(codes[0].toLowerCase(), codes[1].toLowerCase());
    }

}
