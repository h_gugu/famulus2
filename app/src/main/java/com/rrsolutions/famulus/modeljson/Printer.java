package com.rrsolutions.famulus.modeljson;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class Printer {
    private int id = 0;
    @SerializedName("name")
    private String name = "";
    @SerializedName("manufacturer")
    private String manufacturer = "";
    @SerializedName("mac")
    private String mac = "";
    @SerializedName("serial")
    private String serial = "";
    @SerializedName("model")
    private String model = "";

    private boolean connected = false;

    public Printer(String name, String manufacturer, String mac, String serial, String model) {
        this.setName(name);
        this.setManufacturer(manufacturer);
        this.setMac(mac);
        this.setSerial(serial);
        this.setModel(model);
    }

    public Printer() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
