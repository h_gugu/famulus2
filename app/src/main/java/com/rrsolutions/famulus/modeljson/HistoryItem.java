package com.rrsolutions.famulus.modeljson;

import com.rrsolutions.famulus.printer.PrintCategory;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * Created by hassanmushtaq on 03/01/2018.
 */

public class HistoryItem {

    private boolean checked = false;
    private long orderid = 0;
    private String table = "";
    private String searchtable = "";
    private int printingmode = 0;
    private String date = "";
    private String currency = "";
    private String waiter = "";
    private int status = -1;
    private List<PrintCategory> categoryList = new ArrayList<>();

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPrintingmode() {
        return printingmode;
    }

    public void setPrintingmode(int printingmode) {
        this.printingmode = printingmode;
    }

    public String getWaiter() {
        return waiter;
    }

    public void setWaiter(String waiter) {
        this.waiter = waiter;
    }

    public List<PrintCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<PrintCategory> categoryList) {
        this.categoryList = categoryList;
    }

    public String getSearchtable() {
        return searchtable;
    }

    public void setSearchtable(String searchtable) {
        this.searchtable = searchtable;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
