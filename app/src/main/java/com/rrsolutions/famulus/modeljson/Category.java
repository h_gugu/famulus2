package com.rrsolutions.famulus.modeljson;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class Category {

    @SerializedName("id")
    private int id = 0;
    @SerializedName("name")
    private String name = "";
    @SerializedName("extra_info")
    private String extra_info = "";
    @SerializedName("enabled")
    private boolean enabled = false;
    @SerializedName("deleted")
    private boolean deleted = false;
    @SerializedName("products")
    private List<Product> products = new ArrayList<>();

    private boolean selected = false;

    public Category() {}

    public Category(int id, String name, String extra_info, boolean enabled, boolean deleted, List<Product> products) {
        this.id = id;
        this.name = name;
        this.extra_info = extra_info;
        this.enabled = enabled;
        this.deleted = deleted;
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
