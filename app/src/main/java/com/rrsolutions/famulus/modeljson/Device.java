package com.rrsolutions.famulus.modeljson;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class Device {

    private int id = 0;
    @SerializedName("waiter_name")
    private String waiter_name = "";
    @SerializedName("operation_mode")
    private int operation_mode = 0;
    @SerializedName("allow_oh_viewing")
    private boolean allow_oh_viewing = false;
    @SerializedName("enable_payment_screen")
    private boolean enable_payment_screen = false;
    @SerializedName("oh_update_interval")
    private int oh_update_interval = 0;
    @SerializedName("level_1_pin")
    private String level_1_pin = "";
    @SerializedName("level_2_pin")
    private String level_2_pin = "";

    public Device(String waiter_name, int operation_mode, boolean allow_oh_viewing, boolean enable_payment_screen,
                  int oh_update_interval, String level_1_pin, String level_2_pin) {
        this.waiter_name = waiter_name;
        this.operation_mode = operation_mode;
        this.allow_oh_viewing = allow_oh_viewing;
        this.enable_payment_screen = enable_payment_screen;
        this.oh_update_interval = oh_update_interval;
        this.level_1_pin = level_1_pin;
        this.level_2_pin = level_2_pin;
    }

    public String getWaiter_name() {
        return waiter_name;
    }

    public void setWaiter_name(String waiter_name) {
        this.waiter_name = waiter_name;
    }

    public int getOperation_mode() {
        return operation_mode;
    }

    public void setOperation_mode(int operation_mode) {
        this.operation_mode = operation_mode;
    }

    public boolean isAllow_oh_viewing() {
        return allow_oh_viewing;
    }

    public void setAllow_oh_viewing(boolean allow_oh_viewing) {
        this.allow_oh_viewing = allow_oh_viewing;
    }

    public boolean isEnable_payment_screen() {
        return enable_payment_screen;
    }

    public void setEnable_payment_screen(boolean enable_payment_screen) {
        this.enable_payment_screen = enable_payment_screen;
    }

    public int getOh_update_interval() {
        return oh_update_interval;
    }

    public void setOh_update_interval(int oh_update_interval) {
        this.oh_update_interval = oh_update_interval;
    }

    public String getLevel_1_pin() {
        return level_1_pin;
    }

    public void setLevel_1_pin(String level_1_pin) {
        this.level_1_pin = level_1_pin;
    }

    public String getLevel_2_pin() {
        return level_2_pin;
    }

    public void setLevel_2_pin(String level_2_pin) {
        this.level_2_pin = level_2_pin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
