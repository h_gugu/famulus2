package com.rrsolutions.famulus.modeljson;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class EventInfo {

    private int id = 0;
    @SerializedName("name")
    private String name = "";
    @SerializedName("start_date")
    private String start_date = "";
    @SerializedName("end_date")
    private String end_date = "";

    public EventInfo(String name, String start_date, String end_date) {
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }
}
