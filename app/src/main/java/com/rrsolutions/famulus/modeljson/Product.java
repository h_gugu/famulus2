package com.rrsolutions.famulus.modeljson;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class Product {

    @SerializedName("id")
    private int id = 0;
    @SerializedName("cat_id")
    private int cat_id = 0;
    @SerializedName("name")
    private String name = "";
    @SerializedName("extra_info")
    private String extra_info = "";
    @SerializedName("price")
    private double price = 0.0;
    @SerializedName("enabled")
    private boolean enabled = false;
    @SerializedName("deleted")
    private boolean deleted = false;
    @SerializedName("options")
    private List<Option> options = new ArrayList<>();

    private int quantity = 0;
    private boolean selected = false;
    private boolean showquantity = true;

    public Product() {}

    public Product(int id, int cat_id, String name, String extra_info, double price, boolean enabled, boolean deleted, List<Option> options) {
        this.id = id;
        this.cat_id = cat_id;
        this.name = name;
        this.extra_info = extra_info;
        this.price = price;
        this.enabled = enabled;
        this.deleted = deleted;
        this.options = options;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isShowQuantity() {
        return showquantity;
    }

    public void setShowQuantity(boolean showquantity) {
        this.showquantity = showquantity;
    }
}
