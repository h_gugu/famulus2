package com.rrsolutions.famulus.modeljson;

import com.google.gson.annotations.SerializedName;

/**
 * Created by hassanmushtaq on 06/12/2017.
 */

public class Option {

    @SerializedName("id")
    private int id = 0;
    @SerializedName("prod_id")
    private int prod_id = 0;
    @SerializedName("orderIndex")
    private int orderIndex = 0;
    @SerializedName("name")
    private String name = "";
    @SerializedName("extra_info")
    private String extra_info = "";
    @SerializedName("enabled")
    private boolean enabled = false;
    @SerializedName("deleted")
    private boolean deleted = false;

    private boolean selected = false;
    private int quantity = 0;
    private String description = "";

    public Option() {}

    public Option (int id, int prod_id, int orderIndex, String name, String extra_info, boolean enabled, boolean deleted) {
        this.id = id;
        this.prod_id = prod_id;
        this.orderIndex = orderIndex;
        this.name = name;
        this.extra_info = extra_info;
        this.enabled = enabled;
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProd_id() {
        return prod_id;
    }

    public void setProd_id(int prod_id) {
        this.prod_id = prod_id;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
