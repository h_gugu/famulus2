package com.rrsolutions.famulus;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.rrsolutions.famulus.businesslogic.PrintOrderController;
import com.rrsolutions.famulus.common.MyContextWrapper;
import com.rrsolutions.famulus.common.Shared;
import com.rrsolutions.famulus.database.Storage;

public class PrintOrderActivity extends AppCompatActivity {

    private Context mContext = null;
    private ExpandableListView elstOrder = null;
    private Button btnBack = null;
    private Button btnForward = null;
    private TextView txtSplit = null;
    private TextView txtRest = null;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(MyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_order);
        mContext = this;
        elstOrder = findViewById(R.id.elstOrder);
        btnBack = findViewById(R.id.btnBack);
        btnForward = findViewById(R.id.btnForward);
        txtSplit = findViewById(R.id.txtSplit);
        txtRest = findViewById(R.id.txtRest);

        btnBack.setBackgroundResource(R.drawable.file_multiple);
        btnForward.setBackgroundResource(R.drawable.file);

        if (getSupportActionBar() != null) {
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mContext.getString(R.string.print_order_title));
        }

        PrintOrderController printOrderController = new PrintOrderController();
        printOrderController.setListener(mContext);
    }

    public ExpandableListView getOrder() {
        return elstOrder;
    }

    public Button getBack() {
        return btnBack;
    }

    public Button getForward() {
        return btnForward;
    }

    public TextView getSplit() {
        return txtSplit;
    }

    public TextView getRest() {
        return txtRest;
    }

    @Override
    public void onBackPressed() {

    }
}
